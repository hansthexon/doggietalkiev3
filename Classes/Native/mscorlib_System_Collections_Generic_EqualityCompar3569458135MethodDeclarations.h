﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/TrackableResultData>
struct DefaultComparer_t3569458135;

#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1947527974.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor()
extern "C"  void DefaultComparer__ctor_m1077471590_gshared (DefaultComparer_t3569458135 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1077471590(__this, method) ((  void (*) (DefaultComparer_t3569458135 *, const MethodInfo*))DefaultComparer__ctor_m1077471590_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/TrackableResultData>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3327698493_gshared (DefaultComparer_t3569458135 * __this, TrackableResultData_t1947527974  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3327698493(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3569458135 *, TrackableResultData_t1947527974 , const MethodInfo*))DefaultComparer_GetHashCode_m3327698493_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/TrackableResultData>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2025958937_gshared (DefaultComparer_t3569458135 * __this, TrackableResultData_t1947527974  ___x0, TrackableResultData_t1947527974  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2025958937(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3569458135 *, TrackableResultData_t1947527974 , TrackableResultData_t1947527974 , const MethodInfo*))DefaultComparer_Equals_m2025958937_gshared)(__this, ___x0, ___y1, method)
