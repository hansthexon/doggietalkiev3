﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.AppleReceipt
struct AppleReceipt_t3991411794;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Void UnityEngine.Purchasing.Security.AppleReceipt::.ctor()
extern "C"  void AppleReceipt__ctor_m4128629250 (AppleReceipt_t3991411794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.Security.AppleReceipt::get_bundleID()
extern "C"  String_t* AppleReceipt_get_bundleID_m1725040983 (AppleReceipt_t3991411794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.AppleReceipt::set_bundleID(System.String)
extern "C"  void AppleReceipt_set_bundleID_m1577937306 (AppleReceipt_t3991411794 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.AppleReceipt::set_appVersion(System.String)
extern "C"  void AppleReceipt_set_appVersion_m3571446326 (AppleReceipt_t3991411794 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.AppleReceipt::set_opaque(System.Byte[])
extern "C"  void AppleReceipt_set_opaque_m2408428135 (AppleReceipt_t3991411794 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.AppleReceipt::set_hash(System.Byte[])
extern "C"  void AppleReceipt_set_hash_m1608210220 (AppleReceipt_t3991411794 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.AppleReceipt::set_originalApplicationVersion(System.String)
extern "C"  void AppleReceipt_set_originalApplicationVersion_m3318704410 (AppleReceipt_t3991411794 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime UnityEngine.Purchasing.Security.AppleReceipt::get_receiptCreationDate()
extern "C"  DateTime_t693205669  AppleReceipt_get_receiptCreationDate_m4060965251 (AppleReceipt_t3991411794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.AppleReceipt::set_receiptCreationDate(System.DateTime)
extern "C"  void AppleReceipt_set_receiptCreationDate_m3678109238 (AppleReceipt_t3991411794 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
