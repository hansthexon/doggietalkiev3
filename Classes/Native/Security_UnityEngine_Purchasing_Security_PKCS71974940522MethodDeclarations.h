﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.PKCS7
struct PKCS7_t1974940522;
// LipingShare.LCLib.Asn1Processor.Asn1Node
struct Asn1Node_t1770761751;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.Security.SignerInfo>
struct List_1_t3491469936;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.Security.X509Cert>
struct List_1_t4145897706;
// UnityEngine.Purchasing.Security.X509Cert
struct X509Cert_t481809278;

#include "codegen/il2cpp-codegen.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Node1770761751.h"
#include "Security_UnityEngine_Purchasing_Security_X509Cert481809278.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Void UnityEngine.Purchasing.Security.PKCS7::.ctor(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  void PKCS7__ctor_m1019290992 (PKCS7_t1974940522 * __this, Asn1Node_t1770761751 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LipingShare.LCLib.Asn1Processor.Asn1Node UnityEngine.Purchasing.Security.PKCS7::get_data()
extern "C"  Asn1Node_t1770761751 * PKCS7_get_data_m139975642 (PKCS7_t1974940522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.PKCS7::set_data(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  void PKCS7_set_data_m2145946707 (PKCS7_t1974940522 * __this, Asn1Node_t1770761751 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.Security.SignerInfo> UnityEngine.Purchasing.Security.PKCS7::get_sinfos()
extern "C"  List_1_t3491469936 * PKCS7_get_sinfos_m3813593105 (PKCS7_t1974940522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.PKCS7::set_sinfos(System.Collections.Generic.List`1<UnityEngine.Purchasing.Security.SignerInfo>)
extern "C"  void PKCS7_set_sinfos_m921812246 (PKCS7_t1974940522 * __this, List_1_t3491469936 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.Security.X509Cert> UnityEngine.Purchasing.Security.PKCS7::get_certChain()
extern "C"  List_1_t4145897706 * PKCS7_get_certChain_m1660994762 (PKCS7_t1974940522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.PKCS7::set_certChain(System.Collections.Generic.List`1<UnityEngine.Purchasing.Security.X509Cert>)
extern "C"  void PKCS7_set_certChain_m766545607 (PKCS7_t1974940522 * __this, List_1_t4145897706 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Purchasing.Security.PKCS7::Verify(UnityEngine.Purchasing.Security.X509Cert,System.DateTime)
extern "C"  bool PKCS7_Verify_m738735600 (PKCS7_t1974940522 * __this, X509Cert_t481809278 * ___cert0, DateTime_t693205669  ___certificateCreationTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Purchasing.Security.PKCS7::ValidateChain(UnityEngine.Purchasing.Security.X509Cert,UnityEngine.Purchasing.Security.X509Cert,System.DateTime)
extern "C"  bool PKCS7_ValidateChain_m2979125067 (PKCS7_t1974940522 * __this, X509Cert_t481809278 * ___root0, X509Cert_t481809278 * ___cert1, DateTime_t693205669  ___certificateCreationTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.PKCS7::CheckStructure()
extern "C"  void PKCS7_CheckStructure_m676548793 (PKCS7_t1974940522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
