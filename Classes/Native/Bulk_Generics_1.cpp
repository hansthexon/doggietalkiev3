﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Comparer`1/DefaultComparer<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct DefaultComparer_t3458397517;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Byte>
struct DefaultComparer_t2172374669;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct DefaultComparer_t2205520327;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct DefaultComparer_t2823092174;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>
struct DefaultComparer_t3477443198;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>
struct DefaultComparer_t4147226435;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>
struct DefaultComparer_t1022871826;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>
struct DefaultComparer_t561147681;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>
struct DefaultComparer_t1178719528;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>
struct DefaultComparer_t2878395072;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>
struct DefaultComparer_t4282435443;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>
struct DefaultComparer_t1919529182;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Color32>
struct DefaultComparer_t3658755047;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
struct DefaultComparer_t2805423905;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Experimental.Director.Playable>
struct DefaultComparer_t2156815781;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.RuntimePlatform>
struct DefaultComparer_t358855200;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct DefaultComparer_t1545907033;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>
struct DefaultComparer_t2110548107;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>
struct DefaultComparer_t3988496347;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector2>
struct DefaultComparer_t732977812;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector3>
struct DefaultComparer_t732977813;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector4>
struct DefaultComparer_t732977814;
// System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>
struct DefaultComparer_t1499800277;
// System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>
struct DefaultComparer_t447996739;
// System.Collections.Generic.Comparer`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct Comparer_1_t3859136403;
// System.Collections.Generic.Comparer`1<System.Byte>
struct Comparer_1_t2573113555;
// System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct Comparer_1_t2606259213;
// System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Comparer_1_t3223831060;
// System.Collections.Generic.Comparer`1<System.DateTime>
struct Comparer_1_t3878182084;
// System.Collections.Generic.Comparer`1<System.DateTimeOffset>
struct Comparer_1_t252998025;
// System.Collections.Generic.Comparer`1<System.Guid>
struct Comparer_1_t1423610712;
// System.Collections.Generic.Comparer`1<System.Int32>
struct Comparer_1_t961886567;
// System.Collections.Generic.Comparer`1<System.Object>
struct Comparer_1_t1579458414;
// System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeNamedArgument>
struct Comparer_1_t3279133958;
// System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeTypedArgument>
struct Comparer_1_t388207033;
// System.Collections.Generic.Comparer`1<System.TimeSpan>
struct Comparer_1_t2320268068;
// System.Collections.Generic.Comparer`1<UnityEngine.Color32>
struct Comparer_1_t4059493933;
// System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.RaycastResult>
struct Comparer_1_t3206162791;
// System.Collections.Generic.Comparer`1<UnityEngine.Experimental.Director.Playable>
struct Comparer_1_t2557554667;
// System.Collections.Generic.Comparer`1<UnityEngine.RuntimePlatform>
struct Comparer_1_t759594086;
// System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>
struct Comparer_1_t1946645919;
// System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>
struct Comparer_1_t2511286993;
// System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>
struct Comparer_1_t94267937;
// System.Collections.Generic.Comparer`1<UnityEngine.Vector2>
struct Comparer_1_t1133716698;
// System.Collections.Generic.Comparer`1<UnityEngine.Vector3>
struct Comparer_1_t1133716699;
// System.Collections.Generic.Comparer`1<UnityEngine.Vector4>
struct Comparer_1_t1133716700;
// System.Collections.Generic.Comparer`1<Vuforia.Image/PIXEL_FORMAT>
struct Comparer_1_t1900539163;
// System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>
struct Comparer_1_t848735625;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1697274930;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t955353609;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct Dictionary_2_t231711286;
// System.Collections.Generic.Dictionary`2<System.Object,LitJson.ArrayMetadata>
struct Dictionary_2_t1600894590;
// System.Collections.Generic.Dictionary`2<System.Object,LitJson.ObjectMetadata>
struct Dictionary_2_t3587982526;
// System.Collections.Generic.Dictionary`2<System.Object,LitJson.PropertyMetadata>
struct Dictionary_2_t3285886264;
// System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>
struct Dictionary_2_t440094893;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t3417634846;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t3925882069;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1663937576;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2281509423;
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t578942739;
// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t1316726616;
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct Dictionary_2_t88735534;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2879144337.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2879144337MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1733269780.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1733269780MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2235177892.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2235177892MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint1376425630.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen223115942.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen223115942MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint2D3659330976.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen879938638.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen879938638MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen231330514.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen231330514MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3667545548.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4271104839.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4271104839MethodDeclarations.h"
#include "UnityEngine_UnityEngine_jvalue3412352577.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2308223602.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2308223602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen945932582.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen945932582MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen627693740.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen627693740MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2728337229.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2728337229MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2620119317.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2620119317MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo1761367055.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2613618411.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2613618411MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1754866149.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen240568204.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen240568204MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3676783238.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1266026145.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1266026145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch407273883.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1887381311.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1887381311MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentTy1028629049.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3915389062.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3915389062MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UICharInfo3056636800.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen185062840.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen185062840MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UILineInfo3621277874.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2063011080.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2063011080MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3102459841.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3102459841MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3102459842.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3102459842MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3102459843.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3102459843MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen547656355.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen547656355MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WebCamDevice3983871389.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2380003853.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2380003853MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearDevice_Eyew1521251591.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3869282306.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3869282306MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT3010530044.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1793284669.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1793284669MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_RectangleData934532407.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2817478768.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2817478768MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Targe1958726506.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen597704739.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen597704739MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl4033919773.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2312124631.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2312124631MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1453372369.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3827361251.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3827361251MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl2968608989.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2806280236.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2806280236MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1947527974.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2082637913.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2082637913MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1223885651.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2132716346.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2132716346MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1273964084.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen306021597.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen306021597MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3742236631.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2583418750.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2583418750MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof1724666488.h"
#include "mscorlib_System_Collections_Generic_CollectionDebug517719049.h"
#include "mscorlib_System_Collections_Generic_CollectionDebug517719049MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_CollectionDebu2230541861.h"
#include "mscorlib_System_Collections_Generic_CollectionDebu2230541861MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3458397517.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3458397517MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3859136403MethodDeclarations.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I674159988.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2172374669.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2172374669MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2573113555MethodDeclarations.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2205520327.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2205520327MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2606259213MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2823092174.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2823092174MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3223831060MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3477443198.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3477443198MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3878182084MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def4147226435.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def4147226435MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen252998025MethodDeclarations.h"
#include "mscorlib_System_DateTimeOffset1362988906.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1022871826.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1022871826MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1423610712MethodDeclarations.h"
#include "mscorlib_System_Guid2533601593.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa561147681.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa561147681MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen961886567MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1178719528.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1178719528MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1579458414MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2878395072.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2878395072MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3279133958MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def4282435443.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def4282435443MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen388207033MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1919529182.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1919529182MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2320268068MethodDeclarations.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3658755047.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3658755047MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen4059493933MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2805423905.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2805423905MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3206162791MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2156815781.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2156815781MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2557554667MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa358855200.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa358855200MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen759594086MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1545907033.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1545907033MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1946645919MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2110548107.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2110548107MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2511286993MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3988496347.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3988496347MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen94267937MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977812.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977812MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716698MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977813.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977813MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716699MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977814.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977814MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716700MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1499800277.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1499800277MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1900539163MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa447996739.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa447996739MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen848735625MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3859136403.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Activator1850728717MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2573113555.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2606259213.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3223831060.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3878182084.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen252998025.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1423610712.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen961886567.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1579458414.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3279133958.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen388207033.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2320268068.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen4059493933.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3206162791.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2557554667.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen759594086.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1946645919.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2511286993.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen94267937.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716698.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716699.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716700.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1900539163.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen848735625.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1697274930.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Link2723257478.h"
#include "mscorlib_System_ObjectDisposedException2695136451MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException2695136451.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2275378311.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2275378311MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge955353609.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23007666127.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23007666127MethodDeclarations.h"
#include "Vuforia.UnityExtensions_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1551735988.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1551735988MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge231711286.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22284023804.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22284023804MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2920919292.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2920919292MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1600894590.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23653207108.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23653207108MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_ArrayMetadata2008834462.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En613039932.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En613039932MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3587982526.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21345327748.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21345327748MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_ObjectMetadata3995922398.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En310943670.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En310943670MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3285886264.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21043231486.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21043231486MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata3693826136.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1760119595.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1760119595MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge440094893.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22492407411.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22492407411MethodDeclarations.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I848034765.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En442692252.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En442692252MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3417634846.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En950939475.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En950939475MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3925882069.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21683227291.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21683227291MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2983962278.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2983962278MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1663937576.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1898967441.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1898967441MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge578942739.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22631255257.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22631255257MethodDeclarations.h"
#include "mscorlib_System_UInt16986882611.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2636751318.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2636751318MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1316726616.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23369039134.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23369039134MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1408760236.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1408760236MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen88735534.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22141048052.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22141048052MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key91811072.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key91811072MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1697274930MethodDeclarations.h"

// !!0 System.Array::InternalArray__get_Item<UnityEngine.Color>(System.Int32)
extern "C"  Color_t2020392075  Array_InternalArray__get_Item_TisColor_t2020392075_m996560062_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor_t2020392075_m996560062(__this, p0, method) ((  Color_t2020392075  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor_t2020392075_m996560062_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Color32>(System.Int32)
extern "C"  Color32_t874517518  Array_InternalArray__get_Item_TisColor32_t874517518_m1877643687_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor32_t874517518_m1877643687(__this, p0, method) ((  Color32_t874517518  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor32_t874517518_m1877643687_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.ContactPoint>(System.Int32)
extern "C"  ContactPoint_t1376425630  Array_InternalArray__get_Item_TisContactPoint_t1376425630_m3234597783_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContactPoint_t1376425630_m3234597783(__this, p0, method) ((  ContactPoint_t1376425630  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContactPoint_t1376425630_m3234597783_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.ContactPoint2D>(System.Int32)
extern "C"  ContactPoint2D_t3659330976  Array_InternalArray__get_Item_TisContactPoint2D_t3659330976_m825151777_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContactPoint2D_t3659330976_m825151777(__this, p0, method) ((  ContactPoint2D_t3659330976  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContactPoint2D_t3659330976_m825151777_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.RaycastResult>(System.Int32)
extern "C"  RaycastResult_t21186376  Array_InternalArray__get_Item_TisRaycastResult_t21186376_m4125877765_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastResult_t21186376_m4125877765(__this, p0, method) ((  RaycastResult_t21186376  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastResult_t21186376_m4125877765_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Experimental.Director.Playable>(System.Int32)
extern "C"  Playable_t3667545548  Array_InternalArray__get_Item_TisPlayable_t3667545548_m1976366877_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPlayable_t3667545548_m1976366877(__this, p0, method) ((  Playable_t3667545548  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPlayable_t3667545548_m1976366877_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.jvalue>(System.Int32)
extern "C"  jvalue_t3412352577  Array_InternalArray__get_Item_Tisjvalue_t3412352577_m4292019368_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_Tisjvalue_t3412352577_m4292019368(__this, p0, method) ((  jvalue_t3412352577  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_Tisjvalue_t3412352577_m4292019368_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
extern "C"  Keyframe_t1449471340  Array_InternalArray__get_Item_TisKeyframe_t1449471340_m1003508933_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyframe_t1449471340_m1003508933(__this, p0, method) ((  Keyframe_t1449471340  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyframe_t1449471340_m1003508933_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.RaycastHit>(System.Int32)
extern "C"  RaycastHit_t87180320  Array_InternalArray__get_Item_TisRaycastHit_t87180320_m3529622569_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastHit_t87180320_m3529622569(__this, p0, method) ((  RaycastHit_t87180320  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastHit_t87180320_m3529622569_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.RaycastHit2D>(System.Int32)
extern "C"  RaycastHit2D_t4063908774  Array_InternalArray__get_Item_TisRaycastHit2D_t4063908774_m3592947655_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastHit2D_t4063908774_m3592947655(__this, p0, method) ((  RaycastHit2D_t4063908774  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastHit2D_t4063908774_m3592947655_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.RuntimePlatform>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisRuntimePlatform_t1869584967_m2132902450_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRuntimePlatform_t1869584967_m2132902450(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRuntimePlatform_t1869584967_m2132902450_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
extern "C"  HitInfo_t1761367055  Array_InternalArray__get_Item_TisHitInfo_t1761367055_m2443000901_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisHitInfo_t1761367055_m2443000901(__this, p0, method) ((  HitInfo_t1761367055  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisHitInfo_t1761367055_m2443000901_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
extern "C"  GcAchievementData_t1754866149  Array_InternalArray__get_Item_TisGcAchievementData_t1754866149_m2980277810_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcAchievementData_t1754866149_m2980277810(__this, p0, method) ((  GcAchievementData_t1754866149  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcAchievementData_t1754866149_m2980277810_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
extern "C"  GcScoreData_t3676783238  Array_InternalArray__get_Item_TisGcScoreData_t3676783238_m733932313_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcScoreData_t3676783238_m733932313(__this, p0, method) ((  GcScoreData_t3676783238  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcScoreData_t3676783238_m733932313_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Touch>(System.Int32)
extern "C"  Touch_t407273883  Array_InternalArray__get_Item_TisTouch_t407273883_m3836865510_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTouch_t407273883_m3836865510(__this, p0, method) ((  Touch_t407273883  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTouch_t407273883_m3836865510_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/ContentType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisContentType_t1028629049_m2406619723_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContentType_t1028629049_m2406619723(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContentType_t1028629049_m2406619723_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UICharInfo>(System.Int32)
extern "C"  UICharInfo_t3056636800  Array_InternalArray__get_Item_TisUICharInfo_t3056636800_m3872982785_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUICharInfo_t3056636800_m3872982785(__this, p0, method) ((  UICharInfo_t3056636800  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUICharInfo_t3056636800_m3872982785_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UILineInfo>(System.Int32)
extern "C"  UILineInfo_t3621277874  Array_InternalArray__get_Item_TisUILineInfo_t3621277874_m1432166059_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUILineInfo_t3621277874_m1432166059(__this, p0, method) ((  UILineInfo_t3621277874  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUILineInfo_t3621277874_m1432166059_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UIVertex>(System.Int32)
extern "C"  UIVertex_t1204258818  Array_InternalArray__get_Item_TisUIVertex_t1204258818_m3450355955_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUIVertex_t1204258818_m3450355955(__this, p0, method) ((  UIVertex_t1204258818  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUIVertex_t1204258818_m3450355955_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
extern "C"  Vector2_t2243707579  Array_InternalArray__get_Item_TisVector2_t2243707579_m2394947294_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector2_t2243707579_m2394947294(__this, p0, method) ((  Vector2_t2243707579  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector2_t2243707579_m2394947294_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Vector3>(System.Int32)
extern "C"  Vector3_t2243707580  Array_InternalArray__get_Item_TisVector3_t2243707580_m2841870745_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector3_t2243707580_m2841870745(__this, p0, method) ((  Vector3_t2243707580  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector3_t2243707580_m2841870745_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Vector4>(System.Int32)
extern "C"  Vector4_t2243707581  Array_InternalArray__get_Item_TisVector4_t2243707581_m3866288892_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector4_t2243707581_m3866288892(__this, p0, method) ((  Vector4_t2243707581  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector4_t2243707581_m3866288892_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.WebCamDevice>(System.Int32)
extern "C"  WebCamDevice_t3983871389  Array_InternalArray__get_Item_TisWebCamDevice_t3983871389_m919931200_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisWebCamDevice_t3983871389_m919931200(__this, p0, method) ((  WebCamDevice_t3983871389  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisWebCamDevice_t3983871389_m919931200_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.EyewearDevice/EyewearCalibrationReading>(System.Int32)
extern "C"  EyewearCalibrationReading_t1521251591  Array_InternalArray__get_Item_TisEyewearCalibrationReading_t1521251591_m3136691741_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisEyewearCalibrationReading_t1521251591_m3136691741(__this, p0, method) ((  EyewearCalibrationReading_t1521251591  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisEyewearCalibrationReading_t1521251591_m3136691741_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.Image/PIXEL_FORMAT>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisPIXEL_FORMAT_t3010530044_m1937237900_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPIXEL_FORMAT_t3010530044_m1937237900(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPIXEL_FORMAT_t3010530044_m1937237900_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.RectangleData>(System.Int32)
extern "C"  RectangleData_t934532407  Array_InternalArray__get_Item_TisRectangleData_t934532407_m3279689927_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRectangleData_t934532407_m3279689927(__this, p0, method) ((  RectangleData_t934532407  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRectangleData_t934532407_m3279689927_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.TargetFinder/TargetSearchResult>(System.Int32)
extern "C"  TargetSearchResult_t1958726506  Array_InternalArray__get_Item_TisTargetSearchResult_t1958726506_m3304182054_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTargetSearchResult_t1958726506_m3304182054(__this, p0, method) ((  TargetSearchResult_t1958726506  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTargetSearchResult_t1958726506_m3304182054_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/PropData>(System.Int32)
extern "C"  PropData_t4033919773  Array_InternalArray__get_Item_TisPropData_t4033919773_m2730530575_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPropData_t4033919773_m2730530575(__this, p0, method) ((  PropData_t4033919773  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPropData_t4033919773_m2730530575_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>(System.Int32)
extern "C"  SmartTerrainRevisionData_t1453372369  Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t1453372369_m3179446059_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t1453372369_m3179446059(__this, p0, method) ((  SmartTerrainRevisionData_t1453372369  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t1453372369_m3179446059_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/SurfaceData>(System.Int32)
extern "C"  SurfaceData_t2968608989  Array_InternalArray__get_Item_TisSurfaceData_t2968608989_m2184537775_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSurfaceData_t2968608989_m2184537775(__this, p0, method) ((  SurfaceData_t2968608989  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSurfaceData_t2968608989_m2184537775_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/TrackableResultData>(System.Int32)
extern "C"  TrackableResultData_t1947527974  Array_InternalArray__get_Item_TisTrackableResultData_t1947527974_m2187852578_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTrackableResultData_t1947527974_m2187852578(__this, p0, method) ((  TrackableResultData_t1947527974  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTrackableResultData_t1947527974_m2187852578_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/VirtualButtonData>(System.Int32)
extern "C"  VirtualButtonData_t1223885651  Array_InternalArray__get_Item_TisVirtualButtonData_t1223885651_m600888069_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVirtualButtonData_t1223885651_m600888069(__this, p0, method) ((  VirtualButtonData_t1223885651  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVirtualButtonData_t1223885651_m600888069_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/WordData>(System.Int32)
extern "C"  WordData_t1273964084  Array_InternalArray__get_Item_TisWordData_t1273964084_m181199676_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisWordData_t1273964084_m181199676(__this, p0, method) ((  WordData_t1273964084  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisWordData_t1273964084_m181199676_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/WordResultData>(System.Int32)
extern "C"  WordResultData_t3742236631  Array_InternalArray__get_Item_TisWordResultData_t3742236631_m3796221901_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisWordResultData_t3742236631_m3796221901(__this, p0, method) ((  WordResultData_t3742236631  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisWordResultData_t3742236631_m3796221901_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.WebCamProfile/ProfileData>(System.Int32)
extern "C"  ProfileData_t1724666488  Array_InternalArray__get_Item_TisProfileData_t1724666488_m2551998872_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisProfileData_t1724666488_m2551998872(__this, p0, method) ((  ProfileData_t1724666488  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisProfileData_t1724666488_m2551998872_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m838009241_gshared (InternalEnumerator_1_t2879144337 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m838009241_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2879144337 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2879144337 *>(__this + 1);
	InternalEnumerator_1__ctor_m838009241(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4103553221_gshared (InternalEnumerator_1_t2879144337 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4103553221_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2879144337 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2879144337 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4103553221(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3843687121_gshared (InternalEnumerator_1_t2879144337 * __this, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = InternalEnumerator_1_get_Current_m4290564662((InternalEnumerator_1_t2879144337 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Color_t2020392075  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3843687121_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2879144337 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2879144337 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3843687121(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3984359374_gshared (InternalEnumerator_1_t2879144337 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3984359374_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2879144337 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2879144337 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3984359374(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3103009949_gshared (InternalEnumerator_1_t2879144337 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3103009949_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2879144337 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2879144337 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3103009949(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Color>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4290564662_MetadataUsageId;
extern "C"  Color_t2020392075  InternalEnumerator_1_get_Current_m4290564662_gshared (InternalEnumerator_1_t2879144337 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4290564662_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Color_t2020392075  L_8 = ((  Color_t2020392075  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Color_t2020392075  InternalEnumerator_1_get_Current_m4290564662_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2879144337 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2879144337 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4290564662(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m96919148_gshared (InternalEnumerator_1_t1733269780 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m96919148_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1733269780 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1733269780 *>(__this + 1);
	InternalEnumerator_1__ctor_m96919148(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2275167408_gshared (InternalEnumerator_1_t1733269780 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2275167408_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1733269780 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1733269780 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2275167408(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30488070_gshared (InternalEnumerator_1_t1733269780 * __this, const MethodInfo* method)
{
	{
		Color32_t874517518  L_0 = InternalEnumerator_1_get_Current_m3143558721((InternalEnumerator_1_t1733269780 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Color32_t874517518  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30488070_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1733269780 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1733269780 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30488070(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m876833153_gshared (InternalEnumerator_1_t1733269780 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m876833153_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1733269780 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1733269780 *>(__this + 1);
	InternalEnumerator_1_Dispose_m876833153(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4068681772_gshared (InternalEnumerator_1_t1733269780 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4068681772_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1733269780 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1733269780 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4068681772(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Color32>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3143558721_MetadataUsageId;
extern "C"  Color32_t874517518  InternalEnumerator_1_get_Current_m3143558721_gshared (InternalEnumerator_1_t1733269780 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3143558721_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Color32_t874517518  L_8 = ((  Color32_t874517518  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Color32_t874517518  InternalEnumerator_1_get_Current_m3143558721_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1733269780 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1733269780 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3143558721(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3210262878_gshared (InternalEnumerator_1_t2235177892 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3210262878_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2235177892 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2235177892 *>(__this + 1);
	InternalEnumerator_1__ctor_m3210262878(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2564106794_gshared (InternalEnumerator_1_t2235177892 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2564106794_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2235177892 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2235177892 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2564106794(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1497708066_gshared (InternalEnumerator_1_t2235177892 * __this, const MethodInfo* method)
{
	{
		ContactPoint_t1376425630  L_0 = InternalEnumerator_1_get_Current_m3035290781((InternalEnumerator_1_t2235177892 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ContactPoint_t1376425630  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1497708066_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2235177892 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2235177892 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1497708066(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3715403693_gshared (InternalEnumerator_1_t2235177892 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3715403693_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2235177892 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2235177892 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3715403693(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3299881374_gshared (InternalEnumerator_1_t2235177892 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3299881374_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2235177892 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2235177892 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3299881374(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3035290781_MetadataUsageId;
extern "C"  ContactPoint_t1376425630  InternalEnumerator_1_get_Current_m3035290781_gshared (InternalEnumerator_1_t2235177892 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3035290781_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ContactPoint_t1376425630  L_8 = ((  ContactPoint_t1376425630  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ContactPoint_t1376425630  InternalEnumerator_1_get_Current_m3035290781_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2235177892 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2235177892 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3035290781(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3623160640_gshared (InternalEnumerator_1_t223115942 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3623160640_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t223115942 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t223115942 *>(__this + 1);
	InternalEnumerator_1__ctor_m3623160640(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2619213736_gshared (InternalEnumerator_1_t223115942 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2619213736_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t223115942 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t223115942 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2619213736(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2061144652_gshared (InternalEnumerator_1_t223115942 * __this, const MethodInfo* method)
{
	{
		ContactPoint2D_t3659330976  L_0 = InternalEnumerator_1_get_Current_m4045489063((InternalEnumerator_1_t223115942 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ContactPoint2D_t3659330976  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2061144652_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t223115942 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t223115942 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2061144652(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3885764311_gshared (InternalEnumerator_1_t223115942 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3885764311_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t223115942 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t223115942 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3885764311(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2906956792_gshared (InternalEnumerator_1_t223115942 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2906956792_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t223115942 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t223115942 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2906956792(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4045489063_MetadataUsageId;
extern "C"  ContactPoint2D_t3659330976  InternalEnumerator_1_get_Current_m4045489063_gshared (InternalEnumerator_1_t223115942 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4045489063_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ContactPoint2D_t3659330976  L_8 = ((  ContactPoint2D_t3659330976  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ContactPoint2D_t3659330976  InternalEnumerator_1_get_Current_m4045489063_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t223115942 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t223115942 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4045489063(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m994739194_gshared (InternalEnumerator_1_t879938638 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m994739194_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t879938638 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t879938638 *>(__this + 1);
	InternalEnumerator_1__ctor_m994739194(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2046302786_gshared (InternalEnumerator_1_t879938638 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2046302786_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t879938638 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t879938638 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2046302786(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2900144990_gshared (InternalEnumerator_1_t879938638 * __this, const MethodInfo* method)
{
	{
		RaycastResult_t21186376  L_0 = InternalEnumerator_1_get_Current_m319833891((InternalEnumerator_1_t879938638 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RaycastResult_t21186376  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2900144990_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t879938638 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t879938638 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2900144990(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3805775699_gshared (InternalEnumerator_1_t879938638 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3805775699_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t879938638 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t879938638 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3805775699(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m572812642_gshared (InternalEnumerator_1_t879938638 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m572812642_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t879938638 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t879938638 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m572812642(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m319833891_MetadataUsageId;
extern "C"  RaycastResult_t21186376  InternalEnumerator_1_get_Current_m319833891_gshared (InternalEnumerator_1_t879938638 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m319833891_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RaycastResult_t21186376  L_8 = ((  RaycastResult_t21186376  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  RaycastResult_t21186376  InternalEnumerator_1_get_Current_m319833891_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t879938638 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t879938638 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m319833891(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Experimental.Director.Playable>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1561877944_gshared (InternalEnumerator_1_t231330514 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1561877944_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t231330514 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t231330514 *>(__this + 1);
	InternalEnumerator_1__ctor_m1561877944(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Experimental.Director.Playable>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m196087328_gshared (InternalEnumerator_1_t231330514 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m196087328_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t231330514 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t231330514 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m196087328(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Experimental.Director.Playable>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1810411564_gshared (InternalEnumerator_1_t231330514 * __this, const MethodInfo* method)
{
	{
		Playable_t3667545548  L_0 = InternalEnumerator_1_get_Current_m1443393095((InternalEnumerator_1_t231330514 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Playable_t3667545548  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1810411564_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t231330514 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t231330514 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1810411564(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Experimental.Director.Playable>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2135741487_gshared (InternalEnumerator_1_t231330514 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2135741487_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t231330514 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t231330514 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2135741487(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Experimental.Director.Playable>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m725174512_gshared (InternalEnumerator_1_t231330514 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m725174512_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t231330514 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t231330514 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m725174512(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Experimental.Director.Playable>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1443393095_MetadataUsageId;
extern "C"  Playable_t3667545548  InternalEnumerator_1_get_Current_m1443393095_gshared (InternalEnumerator_1_t231330514 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1443393095_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Playable_t3667545548  L_8 = ((  Playable_t3667545548  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Playable_t3667545548  InternalEnumerator_1_get_Current_m1443393095_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t231330514 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t231330514 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1443393095(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.jvalue>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4236653209_gshared (InternalEnumerator_1_t4271104839 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4236653209_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4271104839 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4271104839 *>(__this + 1);
	InternalEnumerator_1__ctor_m4236653209(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.jvalue>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2041264341_gshared (InternalEnumerator_1_t4271104839 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2041264341_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4271104839 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4271104839 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2041264341(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.jvalue>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1109962925_gshared (InternalEnumerator_1_t4271104839 * __this, const MethodInfo* method)
{
	{
		jvalue_t3412352577  L_0 = InternalEnumerator_1_get_Current_m2256132082((InternalEnumerator_1_t4271104839 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		jvalue_t3412352577  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1109962925_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4271104839 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4271104839 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1109962925(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.jvalue>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3337758128_gshared (InternalEnumerator_1_t4271104839 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3337758128_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4271104839 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4271104839 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3337758128(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.jvalue>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1893450973_gshared (InternalEnumerator_1_t4271104839 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1893450973_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4271104839 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4271104839 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1893450973(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.jvalue>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2256132082_MetadataUsageId;
extern "C"  jvalue_t3412352577  InternalEnumerator_1_get_Current_m2256132082_gshared (InternalEnumerator_1_t4271104839 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2256132082_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		jvalue_t3412352577  L_8 = ((  jvalue_t3412352577  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  jvalue_t3412352577  InternalEnumerator_1_get_Current_m2256132082_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4271104839 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4271104839 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2256132082(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2007859216_gshared (InternalEnumerator_1_t2308223602 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2007859216_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2308223602 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2308223602 *>(__this + 1);
	InternalEnumerator_1__ctor_m2007859216(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715220344_gshared (InternalEnumerator_1_t2308223602 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715220344_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2308223602 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2308223602 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715220344(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m790514740_gshared (InternalEnumerator_1_t2308223602 * __this, const MethodInfo* method)
{
	{
		Keyframe_t1449471340  L_0 = InternalEnumerator_1_get_Current_m3959023023((InternalEnumerator_1_t2308223602 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Keyframe_t1449471340  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m790514740_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2308223602 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2308223602 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m790514740(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3766393335_gshared (InternalEnumerator_1_t2308223602 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3766393335_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2308223602 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2308223602 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3766393335(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2289229080_gshared (InternalEnumerator_1_t2308223602 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2289229080_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2308223602 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2308223602 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2289229080(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3959023023_MetadataUsageId;
extern "C"  Keyframe_t1449471340  InternalEnumerator_1_get_Current_m3959023023_gshared (InternalEnumerator_1_t2308223602 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3959023023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Keyframe_t1449471340  L_8 = ((  Keyframe_t1449471340  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Keyframe_t1449471340  InternalEnumerator_1_get_Current_m3959023023_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2308223602 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2308223602 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3959023023(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3664249240_gshared (InternalEnumerator_1_t945932582 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3664249240_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t945932582 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t945932582 *>(__this + 1);
	InternalEnumerator_1__ctor_m3664249240(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m192344320_gshared (InternalEnumerator_1_t945932582 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m192344320_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t945932582 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t945932582 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m192344320(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3043347404_gshared (InternalEnumerator_1_t945932582 * __this, const MethodInfo* method)
{
	{
		RaycastHit_t87180320  L_0 = InternalEnumerator_1_get_Current_m1715820327((InternalEnumerator_1_t945932582 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RaycastHit_t87180320  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3043347404_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t945932582 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t945932582 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3043347404(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3464626239_gshared (InternalEnumerator_1_t945932582 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3464626239_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t945932582 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t945932582 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3464626239(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3332669936_gshared (InternalEnumerator_1_t945932582 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3332669936_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t945932582 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t945932582 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3332669936(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1715820327_MetadataUsageId;
extern "C"  RaycastHit_t87180320  InternalEnumerator_1_get_Current_m1715820327_gshared (InternalEnumerator_1_t945932582 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1715820327_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RaycastHit_t87180320  L_8 = ((  RaycastHit_t87180320  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  RaycastHit_t87180320  InternalEnumerator_1_get_Current_m1715820327_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t945932582 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t945932582 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1715820327(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m32322958_gshared (InternalEnumerator_1_t627693740 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m32322958_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t627693740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t627693740 *>(__this + 1);
	InternalEnumerator_1__ctor_m32322958(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1777467498_gshared (InternalEnumerator_1_t627693740 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1777467498_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t627693740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t627693740 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1777467498(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1533037706_gshared (InternalEnumerator_1_t627693740 * __this, const MethodInfo* method)
{
	{
		RaycastHit2D_t4063908774  L_0 = InternalEnumerator_1_get_Current_m1025321669((InternalEnumerator_1_t627693740 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RaycastHit2D_t4063908774  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1533037706_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t627693740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t627693740 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1533037706(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4040890621_gshared (InternalEnumerator_1_t627693740 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4040890621_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t627693740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t627693740 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4040890621(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1799288398_gshared (InternalEnumerator_1_t627693740 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1799288398_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t627693740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t627693740 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1799288398(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1025321669_MetadataUsageId;
extern "C"  RaycastHit2D_t4063908774  InternalEnumerator_1_get_Current_m1025321669_gshared (InternalEnumerator_1_t627693740 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1025321669_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RaycastHit2D_t4063908774  L_8 = ((  RaycastHit2D_t4063908774  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  RaycastHit2D_t4063908774  InternalEnumerator_1_get_Current_m1025321669_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t627693740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t627693740 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1025321669(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RuntimePlatform>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1632604343_gshared (InternalEnumerator_1_t2728337229 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1632604343_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2728337229 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2728337229 *>(__this + 1);
	InternalEnumerator_1__ctor_m1632604343(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RuntimePlatform>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3290730223_gshared (InternalEnumerator_1_t2728337229 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3290730223_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2728337229 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2728337229 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3290730223(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RuntimePlatform>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2046459407_gshared (InternalEnumerator_1_t2728337229 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m763487222((InternalEnumerator_1_t2728337229 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2046459407_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2728337229 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2728337229 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2046459407(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RuntimePlatform>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m357168826_gshared (InternalEnumerator_1_t2728337229 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m357168826_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2728337229 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2728337229 *>(__this + 1);
	InternalEnumerator_1_Dispose_m357168826(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RuntimePlatform>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2684098683_gshared (InternalEnumerator_1_t2728337229 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2684098683_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2728337229 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2728337229 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2684098683(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.RuntimePlatform>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m763487222_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m763487222_gshared (InternalEnumerator_1_t2728337229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m763487222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m763487222_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2728337229 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2728337229 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m763487222(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3220229132_gshared (InternalEnumerator_1_t2620119317 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3220229132_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2620119317 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2620119317 *>(__this + 1);
	InternalEnumerator_1__ctor_m3220229132(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m574988908_gshared (InternalEnumerator_1_t2620119317 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m574988908_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2620119317 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2620119317 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m574988908(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1933635818_gshared (InternalEnumerator_1_t2620119317 * __this, const MethodInfo* method)
{
	{
		HitInfo_t1761367055  L_0 = InternalEnumerator_1_get_Current_m2826780083((InternalEnumerator_1_t2620119317 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		HitInfo_t1761367055  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1933635818_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2620119317 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2620119317 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1933635818(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m282312359_gshared (InternalEnumerator_1_t2620119317 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m282312359_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2620119317 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2620119317 *>(__this + 1);
	InternalEnumerator_1_Dispose_m282312359(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m886855812_gshared (InternalEnumerator_1_t2620119317 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m886855812_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2620119317 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2620119317 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m886855812(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2826780083_MetadataUsageId;
extern "C"  HitInfo_t1761367055  InternalEnumerator_1_get_Current_m2826780083_gshared (InternalEnumerator_1_t2620119317 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2826780083_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		HitInfo_t1761367055  L_8 = ((  HitInfo_t1761367055  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  HitInfo_t1761367055  InternalEnumerator_1_get_Current_m2826780083_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2620119317 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2620119317 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2826780083(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3474059021_gshared (InternalEnumerator_1_t2613618411 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3474059021_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2613618411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2613618411 *>(__this + 1);
	InternalEnumerator_1__ctor_m3474059021(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3946824409_gshared (InternalEnumerator_1_t2613618411 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3946824409_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2613618411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2613618411 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3946824409(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2685895857_gshared (InternalEnumerator_1_t2613618411 * __this, const MethodInfo* method)
{
	{
		GcAchievementData_t1754866149  L_0 = InternalEnumerator_1_get_Current_m406393356((InternalEnumerator_1_t2613618411 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		GcAchievementData_t1754866149  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2685895857_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2613618411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2613618411 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2685895857(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m250541766_gshared (InternalEnumerator_1_t2613618411 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m250541766_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2613618411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2613618411 *>(__this + 1);
	InternalEnumerator_1_Dispose_m250541766(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2520133033_gshared (InternalEnumerator_1_t2613618411 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2520133033_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2613618411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2613618411 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2520133033(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m406393356_MetadataUsageId;
extern "C"  GcAchievementData_t1754866149  InternalEnumerator_1_get_Current_m406393356_gshared (InternalEnumerator_1_t2613618411 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m406393356_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		GcAchievementData_t1754866149  L_8 = ((  GcAchievementData_t1754866149  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  GcAchievementData_t1754866149  InternalEnumerator_1_get_Current_m406393356_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2613618411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2613618411 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m406393356(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2891033852_gshared (InternalEnumerator_1_t240568204 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2891033852_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t240568204 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t240568204 *>(__this + 1);
	InternalEnumerator_1__ctor_m2891033852(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1055858572_gshared (InternalEnumerator_1_t240568204 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1055858572_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t240568204 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t240568204 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1055858572(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1201713088_gshared (InternalEnumerator_1_t240568204 * __this, const MethodInfo* method)
{
	{
		GcScoreData_t3676783238  L_0 = InternalEnumerator_1_get_Current_m75828603((InternalEnumerator_1_t240568204 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		GcScoreData_t3676783238  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1201713088_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t240568204 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t240568204 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1201713088(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1982788747_gshared (InternalEnumerator_1_t240568204 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1982788747_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t240568204 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t240568204 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1982788747(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4065131604_gshared (InternalEnumerator_1_t240568204 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4065131604_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t240568204 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t240568204 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4065131604(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m75828603_MetadataUsageId;
extern "C"  GcScoreData_t3676783238  InternalEnumerator_1_get_Current_m75828603_gshared (InternalEnumerator_1_t240568204 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m75828603_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		GcScoreData_t3676783238  L_8 = ((  GcScoreData_t3676783238  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  GcScoreData_t3676783238  InternalEnumerator_1_get_Current_m75828603_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t240568204 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t240568204 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m75828603(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Touch>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1894376481_gshared (InternalEnumerator_1_t1266026145 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1894376481_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1266026145 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1266026145 *>(__this + 1);
	InternalEnumerator_1__ctor_m1894376481(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Touch>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2698301021_gshared (InternalEnumerator_1_t1266026145 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2698301021_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1266026145 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1266026145 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2698301021(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Touch>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m297674753_gshared (InternalEnumerator_1_t1266026145 * __this, const MethodInfo* method)
{
	{
		Touch_t407273883  L_0 = InternalEnumerator_1_get_Current_m3033039414((InternalEnumerator_1_t1266026145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Touch_t407273883  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m297674753_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1266026145 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1266026145 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m297674753(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Touch>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2713184934_gshared (InternalEnumerator_1_t1266026145 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2713184934_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1266026145 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1266026145 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2713184934(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Touch>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1629124821_gshared (InternalEnumerator_1_t1266026145 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1629124821_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1266026145 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1266026145 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1629124821(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Touch>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3033039414_MetadataUsageId;
extern "C"  Touch_t407273883  InternalEnumerator_1_get_Current_m3033039414_gshared (InternalEnumerator_1_t1266026145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3033039414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Touch_t407273883  L_8 = ((  Touch_t407273883  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Touch_t407273883  InternalEnumerator_1_get_Current_m3033039414_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1266026145 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1266026145 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3033039414(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2458691472_gshared (InternalEnumerator_1_t1887381311 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2458691472_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1887381311 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1887381311 *>(__this + 1);
	InternalEnumerator_1__ctor_m2458691472(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m86252988_gshared (InternalEnumerator_1_t1887381311 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m86252988_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1887381311 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1887381311 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m86252988(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2389982234_gshared (InternalEnumerator_1_t1887381311 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3732458101((InternalEnumerator_1_t1887381311 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2389982234_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1887381311 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1887381311 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2389982234(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3291666845_gshared (InternalEnumerator_1_t1887381311 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3291666845_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1887381311 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1887381311 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3291666845(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m252820768_gshared (InternalEnumerator_1_t1887381311 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m252820768_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1887381311 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1887381311 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m252820768(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3732458101_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3732458101_gshared (InternalEnumerator_1_t1887381311 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3732458101_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3732458101_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1887381311 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1887381311 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3732458101(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1815261138_gshared (InternalEnumerator_1_t3915389062 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1815261138_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3915389062 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3915389062 *>(__this + 1);
	InternalEnumerator_1__ctor_m1815261138(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2208002250_gshared (InternalEnumerator_1_t3915389062 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2208002250_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3915389062 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3915389062 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2208002250(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m160972190_gshared (InternalEnumerator_1_t3915389062 * __this, const MethodInfo* method)
{
	{
		UICharInfo_t3056636800  L_0 = InternalEnumerator_1_get_Current_m889125315((InternalEnumerator_1_t3915389062 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UICharInfo_t3056636800  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m160972190_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3915389062 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3915389062 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m160972190(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1399397099_gshared (InternalEnumerator_1_t3915389062 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1399397099_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3915389062 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3915389062 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1399397099(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3850699098_gshared (InternalEnumerator_1_t3915389062 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3850699098_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3915389062 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3915389062 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3850699098(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m889125315_MetadataUsageId;
extern "C"  UICharInfo_t3056636800  InternalEnumerator_1_get_Current_m889125315_gshared (InternalEnumerator_1_t3915389062 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m889125315_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UICharInfo_t3056636800  L_8 = ((  UICharInfo_t3056636800  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  UICharInfo_t3056636800  InternalEnumerator_1_get_Current_m889125315_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3915389062 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3915389062 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m889125315(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m681761736_gshared (InternalEnumerator_1_t185062840 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m681761736_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t185062840 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t185062840 *>(__this + 1);
	InternalEnumerator_1__ctor_m681761736(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3775211636_gshared (InternalEnumerator_1_t185062840 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3775211636_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t185062840 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t185062840 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3775211636(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2821735692_gshared (InternalEnumerator_1_t185062840 * __this, const MethodInfo* method)
{
	{
		UILineInfo_t3621277874  L_0 = InternalEnumerator_1_get_Current_m2105085649((InternalEnumerator_1_t185062840 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UILineInfo_t3621277874  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2821735692_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t185062840 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t185062840 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2821735692(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2045737049_gshared (InternalEnumerator_1_t185062840 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2045737049_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t185062840 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t185062840 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2045737049(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2410670600_gshared (InternalEnumerator_1_t185062840 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2410670600_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t185062840 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t185062840 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2410670600(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2105085649_MetadataUsageId;
extern "C"  UILineInfo_t3621277874  InternalEnumerator_1_get_Current_m2105085649_gshared (InternalEnumerator_1_t185062840 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2105085649_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UILineInfo_t3621277874  L_8 = ((  UILineInfo_t3621277874  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  UILineInfo_t3621277874  InternalEnumerator_1_get_Current_m2105085649_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t185062840 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t185062840 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2105085649(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2956304256_gshared (InternalEnumerator_1_t2063011080 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2956304256_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2063011080 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2063011080 *>(__this + 1);
	InternalEnumerator_1__ctor_m2956304256(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2315964220_gshared (InternalEnumerator_1_t2063011080 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2315964220_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2063011080 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2063011080 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2315964220(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2764360876_gshared (InternalEnumerator_1_t2063011080 * __this, const MethodInfo* method)
{
	{
		UIVertex_t1204258818  L_0 = InternalEnumerator_1_get_Current_m1883328177((InternalEnumerator_1_t2063011080 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UIVertex_t1204258818  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2764360876_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2063011080 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2063011080 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2764360876(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4229866913_gshared (InternalEnumerator_1_t2063011080 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4229866913_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2063011080 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2063011080 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4229866913(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4061424048_gshared (InternalEnumerator_1_t2063011080 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4061424048_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2063011080 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2063011080 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4061424048(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1883328177_MetadataUsageId;
extern "C"  UIVertex_t1204258818  InternalEnumerator_1_get_Current_m1883328177_gshared (InternalEnumerator_1_t2063011080 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1883328177_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UIVertex_t1204258818  L_8 = ((  UIVertex_t1204258818  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  UIVertex_t1204258818  InternalEnumerator_1_get_Current_m1883328177_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2063011080 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2063011080 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1883328177(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2808001655_gshared (InternalEnumerator_1_t3102459841 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2808001655_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459841 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459841 *>(__this + 1);
	InternalEnumerator_1__ctor_m2808001655(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1018453615_gshared (InternalEnumerator_1_t3102459841 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1018453615_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459841 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459841 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1018453615(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m442726479_gshared (InternalEnumerator_1_t3102459841 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = InternalEnumerator_1_get_Current_m2986222582((InternalEnumerator_1_t3102459841 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Vector2_t2243707579  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m442726479_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459841 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459841 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m442726479(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2270401482_gshared (InternalEnumerator_1_t3102459841 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2270401482_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459841 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459841 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2270401482(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector2>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4175772187_gshared (InternalEnumerator_1_t3102459841 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4175772187_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459841 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459841 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4175772187(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Vector2>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2986222582_MetadataUsageId;
extern "C"  Vector2_t2243707579  InternalEnumerator_1_get_Current_m2986222582_gshared (InternalEnumerator_1_t3102459841 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2986222582_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Vector2_t2243707579  L_8 = ((  Vector2_t2243707579  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Vector2_t2243707579  InternalEnumerator_1_get_Current_m2986222582_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459841 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459841 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2986222582(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2782443954_gshared (InternalEnumerator_1_t3102459842 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2782443954_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459842 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459842 *>(__this + 1);
	InternalEnumerator_1__ctor_m2782443954(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2361456586_gshared (InternalEnumerator_1_t3102459842 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2361456586_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459842 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459842 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2361456586(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m762846484_gshared (InternalEnumerator_1_t3102459842 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = InternalEnumerator_1_get_Current_m747506907((InternalEnumerator_1_t3102459842 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Vector3_t2243707580  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m762846484_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459842 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459842 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m762846484(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m14398895_gshared (InternalEnumerator_1_t3102459842 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m14398895_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459842 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459842 *>(__this + 1);
	InternalEnumerator_1_Dispose_m14398895(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector3>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2953305370_gshared (InternalEnumerator_1_t3102459842 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2953305370_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459842 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459842 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2953305370(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Vector3>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m747506907_MetadataUsageId;
extern "C"  Vector3_t2243707580  InternalEnumerator_1_get_Current_m747506907_gshared (InternalEnumerator_1_t3102459842 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m747506907_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Vector3_t2243707580  L_8 = ((  Vector3_t2243707580  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Vector3_t2243707580  InternalEnumerator_1_get_Current_m747506907_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459842 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459842 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m747506907(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector4>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3901400705_gshared (InternalEnumerator_1_t3102459843 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3901400705_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459843 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459843 *>(__this + 1);
	InternalEnumerator_1__ctor_m3901400705(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector4>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3994416165_gshared (InternalEnumerator_1_t3102459843 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3994416165_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459843 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459843 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3994416165(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector4>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1699120817_gshared (InternalEnumerator_1_t3102459843 * __this, const MethodInfo* method)
{
	{
		Vector4_t2243707581  L_0 = InternalEnumerator_1_get_Current_m2687258796((InternalEnumerator_1_t3102459843 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Vector4_t2243707581  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1699120817_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459843 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459843 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1699120817(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector4>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1925604588_gshared (InternalEnumerator_1_t3102459843 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1925604588_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459843 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459843 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1925604588(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector4>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1441038493_gshared (InternalEnumerator_1_t3102459843 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1441038493_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459843 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459843 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1441038493(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Vector4>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2687258796_MetadataUsageId;
extern "C"  Vector4_t2243707581  InternalEnumerator_1_get_Current_m2687258796_gshared (InternalEnumerator_1_t3102459843 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2687258796_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Vector4_t2243707581  L_8 = ((  Vector4_t2243707581  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Vector4_t2243707581  InternalEnumerator_1_get_Current_m2687258796_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3102459843 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3102459843 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2687258796(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3533596819_gshared (InternalEnumerator_1_t547656355 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3533596819_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t547656355 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t547656355 *>(__this + 1);
	InternalEnumerator_1__ctor_m3533596819(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1312680659_gshared (InternalEnumerator_1_t547656355 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1312680659_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t547656355 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t547656355 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1312680659(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3362466879_gshared (InternalEnumerator_1_t547656355 * __this, const MethodInfo* method)
{
	{
		WebCamDevice_t3983871389  L_0 = InternalEnumerator_1_get_Current_m4173392858((InternalEnumerator_1_t547656355 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		WebCamDevice_t3983871389  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3362466879_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t547656355 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t547656355 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3362466879(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2180558740_gshared (InternalEnumerator_1_t547656355 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2180558740_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t547656355 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t547656355 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2180558740(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2261236503_gshared (InternalEnumerator_1_t547656355 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2261236503_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t547656355 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t547656355 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2261236503(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4173392858_MetadataUsageId;
extern "C"  WebCamDevice_t3983871389  InternalEnumerator_1_get_Current_m4173392858_gshared (InternalEnumerator_1_t547656355 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4173392858_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		WebCamDevice_t3983871389  L_8 = ((  WebCamDevice_t3983871389  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  WebCamDevice_t3983871389  InternalEnumerator_1_get_Current_m4173392858_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t547656355 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t547656355 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4173392858(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.EyewearDevice/EyewearCalibrationReading>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m259229782_gshared (InternalEnumerator_1_t2380003853 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m259229782_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2380003853 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2380003853 *>(__this + 1);
	InternalEnumerator_1__ctor_m259229782(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.EyewearDevice/EyewearCalibrationReading>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m267499990_gshared (InternalEnumerator_1_t2380003853 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m267499990_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2380003853 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2380003853 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m267499990(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.EyewearDevice/EyewearCalibrationReading>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2487975208_gshared (InternalEnumerator_1_t2380003853 * __this, const MethodInfo* method)
{
	{
		EyewearCalibrationReading_t1521251591  L_0 = InternalEnumerator_1_get_Current_m1982407567((InternalEnumerator_1_t2380003853 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EyewearCalibrationReading_t1521251591  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2487975208_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2380003853 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2380003853 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2487975208(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.EyewearDevice/EyewearCalibrationReading>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3281763147_gshared (InternalEnumerator_1_t2380003853 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3281763147_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2380003853 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2380003853 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3281763147(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.EyewearDevice/EyewearCalibrationReading>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2237795566_gshared (InternalEnumerator_1_t2380003853 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2237795566_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2380003853 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2380003853 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2237795566(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Vuforia.EyewearDevice/EyewearCalibrationReading>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1982407567_MetadataUsageId;
extern "C"  EyewearCalibrationReading_t1521251591  InternalEnumerator_1_get_Current_m1982407567_gshared (InternalEnumerator_1_t2380003853 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1982407567_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		EyewearCalibrationReading_t1521251591  L_8 = ((  EyewearCalibrationReading_t1521251591  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  EyewearCalibrationReading_t1521251591  InternalEnumerator_1_get_Current_m1982407567_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2380003853 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2380003853 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1982407567(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3974883817_gshared (InternalEnumerator_1_t3869282306 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3974883817_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3869282306 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3869282306 *>(__this + 1);
	InternalEnumerator_1__ctor_m3974883817(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3799197557_gshared (InternalEnumerator_1_t3869282306 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3799197557_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3869282306 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3869282306 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3799197557(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2986416877_gshared (InternalEnumerator_1_t3869282306 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m2017827474((InternalEnumerator_1_t3869282306 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2986416877_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3869282306 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3869282306 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2986416877(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4189869912_gshared (InternalEnumerator_1_t3869282306 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4189869912_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3869282306 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3869282306 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4189869912(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m188381677_gshared (InternalEnumerator_1_t3869282306 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m188381677_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3869282306 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3869282306 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m188381677(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2017827474_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m2017827474_gshared (InternalEnumerator_1_t3869282306 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2017827474_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m2017827474_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3869282306 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3869282306 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2017827474(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.RectangleData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m919517070_gshared (InternalEnumerator_1_t1793284669 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m919517070_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1793284669 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1793284669 *>(__this + 1);
	InternalEnumerator_1__ctor_m919517070(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.RectangleData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1517550442_gshared (InternalEnumerator_1_t1793284669 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1517550442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1793284669 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1793284669 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1517550442(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.RectangleData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1260136012_gshared (InternalEnumerator_1_t1793284669 * __this, const MethodInfo* method)
{
	{
		RectangleData_t934532407  L_0 = InternalEnumerator_1_get_Current_m899617817((InternalEnumerator_1_t1793284669 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RectangleData_t934532407  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1260136012_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1793284669 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1793284669 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1260136012(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.RectangleData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m862598753_gshared (InternalEnumerator_1_t1793284669 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m862598753_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1793284669 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1793284669 *>(__this + 1);
	InternalEnumerator_1_Dispose_m862598753(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.RectangleData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2386538638_gshared (InternalEnumerator_1_t1793284669 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2386538638_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1793284669 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1793284669 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2386538638(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Vuforia.RectangleData>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m899617817_MetadataUsageId;
extern "C"  RectangleData_t934532407  InternalEnumerator_1_get_Current_m899617817_gshared (InternalEnumerator_1_t1793284669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m899617817_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RectangleData_t934532407  L_8 = ((  RectangleData_t934532407  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  RectangleData_t934532407  InternalEnumerator_1_get_Current_m899617817_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1793284669 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1793284669 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m899617817(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2512328639_gshared (InternalEnumerator_1_t2817478768 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2512328639_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2817478768 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2817478768 *>(__this + 1);
	InternalEnumerator_1__ctor_m2512328639(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m483248007_gshared (InternalEnumerator_1_t2817478768 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m483248007_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2817478768 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2817478768 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m483248007(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3525926607_gshared (InternalEnumerator_1_t2817478768 * __this, const MethodInfo* method)
{
	{
		TargetSearchResult_t1958726506  L_0 = InternalEnumerator_1_get_Current_m1905829526((InternalEnumerator_1_t2817478768 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TargetSearchResult_t1958726506  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3525926607_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2817478768 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2817478768 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3525926607(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3773398498_gshared (InternalEnumerator_1_t2817478768 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3773398498_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2817478768 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2817478768 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3773398498(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m548427715_gshared (InternalEnumerator_1_t2817478768 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m548427715_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2817478768 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2817478768 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m548427715(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1905829526_MetadataUsageId;
extern "C"  TargetSearchResult_t1958726506  InternalEnumerator_1_get_Current_m1905829526_gshared (InternalEnumerator_1_t2817478768 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1905829526_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TargetSearchResult_t1958726506  L_8 = ((  TargetSearchResult_t1958726506  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TargetSearchResult_t1958726506  InternalEnumerator_1_get_Current_m1905829526_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2817478768 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2817478768 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1905829526(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m885668182_gshared (InternalEnumerator_1_t597704739 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m885668182_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t597704739 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t597704739 *>(__this + 1);
	InternalEnumerator_1__ctor_m885668182(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1591643682_gshared (InternalEnumerator_1_t597704739 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1591643682_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t597704739 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t597704739 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1591643682(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2208255028_gshared (InternalEnumerator_1_t597704739 * __this, const MethodInfo* method)
{
	{
		PropData_t4033919773  L_0 = InternalEnumerator_1_get_Current_m2330454385((InternalEnumerator_1_t597704739 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		PropData_t4033919773  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2208255028_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t597704739 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t597704739 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2208255028(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3603881113_gshared (InternalEnumerator_1_t597704739 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3603881113_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t597704739 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t597704739 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3603881113(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2460939878_gshared (InternalEnumerator_1_t597704739 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2460939878_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t597704739 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t597704739 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2460939878(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2330454385_MetadataUsageId;
extern "C"  PropData_t4033919773  InternalEnumerator_1_get_Current_m2330454385_gshared (InternalEnumerator_1_t597704739 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2330454385_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		PropData_t4033919773  L_8 = ((  PropData_t4033919773  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  PropData_t4033919773  InternalEnumerator_1_get_Current_m2330454385_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t597704739 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t597704739 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2330454385(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1832662212_gshared (InternalEnumerator_1_t2312124631 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1832662212_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2312124631 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2312124631 *>(__this + 1);
	InternalEnumerator_1__ctor_m1832662212(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m690296152_gshared (InternalEnumerator_1_t2312124631 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m690296152_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2312124631 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2312124631 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m690296152(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1015258694_gshared (InternalEnumerator_1_t2312124631 * __this, const MethodInfo* method)
{
	{
		SmartTerrainRevisionData_t1453372369  L_0 = InternalEnumerator_1_get_Current_m2523228713((InternalEnumerator_1_t2312124631 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SmartTerrainRevisionData_t1453372369  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1015258694_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2312124631 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2312124631 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1015258694(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1899670337_gshared (InternalEnumerator_1_t2312124631 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1899670337_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2312124631 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2312124631 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1899670337(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2036913556_gshared (InternalEnumerator_1_t2312124631 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2036913556_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2312124631 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2312124631 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2036913556(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2523228713_MetadataUsageId;
extern "C"  SmartTerrainRevisionData_t1453372369  InternalEnumerator_1_get_Current_m2523228713_gshared (InternalEnumerator_1_t2312124631 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2523228713_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		SmartTerrainRevisionData_t1453372369  L_8 = ((  SmartTerrainRevisionData_t1453372369  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  SmartTerrainRevisionData_t1453372369  InternalEnumerator_1_get_Current_m2523228713_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2312124631 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2312124631 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2523228713(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2889442372_gshared (InternalEnumerator_1_t3827361251 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2889442372_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3827361251 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3827361251 *>(__this + 1);
	InternalEnumerator_1__ctor_m2889442372(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1606212984_gshared (InternalEnumerator_1_t3827361251 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1606212984_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3827361251 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3827361251 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1606212984(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3042013320_gshared (InternalEnumerator_1_t3827361251 * __this, const MethodInfo* method)
{
	{
		SurfaceData_t2968608989  L_0 = InternalEnumerator_1_get_Current_m2654712653((InternalEnumerator_1_t3827361251 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SurfaceData_t2968608989  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3042013320_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3827361251 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3827361251 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3042013320(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m602152797_gshared (InternalEnumerator_1_t3827361251 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m602152797_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3827361251 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3827361251 *>(__this + 1);
	InternalEnumerator_1_Dispose_m602152797(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1679626180_gshared (InternalEnumerator_1_t3827361251 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1679626180_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3827361251 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3827361251 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1679626180(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2654712653_MetadataUsageId;
extern "C"  SurfaceData_t2968608989  InternalEnumerator_1_get_Current_m2654712653_gshared (InternalEnumerator_1_t3827361251 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2654712653_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		SurfaceData_t2968608989  L_8 = ((  SurfaceData_t2968608989  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  SurfaceData_t2968608989  InternalEnumerator_1_get_Current_m2654712653_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3827361251 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3827361251 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2654712653(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m58767825_gshared (InternalEnumerator_1_t2806280236 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m58767825_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2806280236 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2806280236 *>(__this + 1);
	InternalEnumerator_1__ctor_m58767825(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3420289605_gshared (InternalEnumerator_1_t2806280236 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3420289605_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2806280236 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2806280236 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3420289605(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3627724997_gshared (InternalEnumerator_1_t2806280236 * __this, const MethodInfo* method)
{
	{
		TrackableResultData_t1947527974  L_0 = InternalEnumerator_1_get_Current_m3167185568((InternalEnumerator_1_t2806280236 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TrackableResultData_t1947527974  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3627724997_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2806280236 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2806280236 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3627724997(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3825968986_gshared (InternalEnumerator_1_t2806280236 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3825968986_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2806280236 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2806280236 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3825968986(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m996772029_gshared (InternalEnumerator_1_t2806280236 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m996772029_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2806280236 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2806280236 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m996772029(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3167185568_MetadataUsageId;
extern "C"  TrackableResultData_t1947527974  InternalEnumerator_1_get_Current_m3167185568_gshared (InternalEnumerator_1_t2806280236 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3167185568_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TrackableResultData_t1947527974  L_8 = ((  TrackableResultData_t1947527974  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TrackableResultData_t1947527974  InternalEnumerator_1_get_Current_m3167185568_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2806280236 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2806280236 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3167185568(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m217120752_gshared (InternalEnumerator_1_t2082637913 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m217120752_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2082637913 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2082637913 *>(__this + 1);
	InternalEnumerator_1__ctor_m217120752(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m751209656_gshared (InternalEnumerator_1_t2082637913 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m751209656_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2082637913 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2082637913 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m751209656(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m635288412_gshared (InternalEnumerator_1_t2082637913 * __this, const MethodInfo* method)
{
	{
		VirtualButtonData_t1223885651  L_0 = InternalEnumerator_1_get_Current_m1044939575((InternalEnumerator_1_t2082637913 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		VirtualButtonData_t1223885651  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m635288412_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2082637913 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2082637913 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m635288412(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4097589111_gshared (InternalEnumerator_1_t2082637913 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4097589111_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2082637913 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2082637913 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4097589111(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2571750504_gshared (InternalEnumerator_1_t2082637913 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2571750504_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2082637913 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2082637913 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2571750504(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1044939575_MetadataUsageId;
extern "C"  VirtualButtonData_t1223885651  InternalEnumerator_1_get_Current_m1044939575_gshared (InternalEnumerator_1_t2082637913 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1044939575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		VirtualButtonData_t1223885651  L_8 = ((  VirtualButtonData_t1223885651  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  VirtualButtonData_t1223885651  InternalEnumerator_1_get_Current_m1044939575_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2082637913 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2082637913 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1044939575(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1795755139_gshared (InternalEnumerator_1_t2132716346 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1795755139_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2132716346 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2132716346 *>(__this + 1);
	InternalEnumerator_1__ctor_m1795755139(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2321104507_gshared (InternalEnumerator_1_t2132716346 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2321104507_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2132716346 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2132716346 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2321104507(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1283170491_gshared (InternalEnumerator_1_t2132716346 * __this, const MethodInfo* method)
{
	{
		WordData_t1273964084  L_0 = InternalEnumerator_1_get_Current_m2100927316((InternalEnumerator_1_t2132716346 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		WordData_t1273964084  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1283170491_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2132716346 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2132716346 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1283170491(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1059070000_gshared (InternalEnumerator_1_t2132716346 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1059070000_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2132716346 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2132716346 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1059070000(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2060255871_gshared (InternalEnumerator_1_t2132716346 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2060255871_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2132716346 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2132716346 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2060255871(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordData>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2100927316_MetadataUsageId;
extern "C"  WordData_t1273964084  InternalEnumerator_1_get_Current_m2100927316_gshared (InternalEnumerator_1_t2132716346 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2100927316_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		WordData_t1273964084  L_8 = ((  WordData_t1273964084  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  WordData_t1273964084  InternalEnumerator_1_get_Current_m2100927316_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2132716346 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2132716346 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2100927316(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m514629746_gshared (InternalEnumerator_1_t306021597 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m514629746_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t306021597 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t306021597 *>(__this + 1);
	InternalEnumerator_1__ctor_m514629746(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m546254282_gshared (InternalEnumerator_1_t306021597 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m546254282_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t306021597 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t306021597 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m546254282(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1634587988_gshared (InternalEnumerator_1_t306021597 * __this, const MethodInfo* method)
{
	{
		WordResultData_t3742236631  L_0 = InternalEnumerator_1_get_Current_m3540546459((InternalEnumerator_1_t306021597 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		WordResultData_t3742236631  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1634587988_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t306021597 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t306021597 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1634587988(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1932305119_gshared (InternalEnumerator_1_t306021597 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1932305119_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t306021597 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t306021597 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1932305119(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m650928538_gshared (InternalEnumerator_1_t306021597 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m650928538_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t306021597 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t306021597 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m650928538(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3540546459_MetadataUsageId;
extern "C"  WordResultData_t3742236631  InternalEnumerator_1_get_Current_m3540546459_gshared (InternalEnumerator_1_t306021597 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3540546459_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		WordResultData_t3742236631  L_8 = ((  WordResultData_t3742236631  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  WordResultData_t3742236631  InternalEnumerator_1_get_Current_m3540546459_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t306021597 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t306021597 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3540546459(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3382093585_gshared (InternalEnumerator_1_t2583418750 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3382093585_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2583418750 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2583418750 *>(__this + 1);
	InternalEnumerator_1__ctor_m3382093585(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m462854917_gshared (InternalEnumerator_1_t2583418750 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m462854917_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2583418750 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2583418750 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m462854917(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1535300601_gshared (InternalEnumerator_1_t2583418750 * __this, const MethodInfo* method)
{
	{
		ProfileData_t1724666488  L_0 = InternalEnumerator_1_get_Current_m478013324((InternalEnumerator_1_t2583418750 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ProfileData_t1724666488  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1535300601_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2583418750 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2583418750 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1535300601(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3005244532_gshared (InternalEnumerator_1_t2583418750 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3005244532_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2583418750 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2583418750 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3005244532(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2191396429_gshared (InternalEnumerator_1_t2583418750 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2191396429_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2583418750 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2583418750 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2191396429(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m478013324_MetadataUsageId;
extern "C"  ProfileData_t1724666488  InternalEnumerator_1_get_Current_m478013324_gshared (InternalEnumerator_1_t2583418750 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m478013324_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ProfileData_t1724666488  L_8 = ((  ProfileData_t1724666488  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ProfileData_t1724666488  InternalEnumerator_1_get_Current_m478013324_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2583418750 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2583418750 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m478013324(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor()
extern "C"  void DefaultComparer__ctor_m2039684834_gshared (DefaultComparer_t3458397517 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t3859136403 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t3859136403 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t3859136403 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m1125374391_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m1125374391_gshared (DefaultComparer_t3458397517 * __this, IndexedColumn_t674159988  ___x0, IndexedColumn_t674159988  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m1125374391_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		IndexedColumn_t674159988  L_3 = ___x0;
		IndexedColumn_t674159988  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		IndexedColumn_t674159988  L_6 = ___x0;
		IndexedColumn_t674159988  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		IndexedColumn_t674159988  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, IndexedColumn_t674159988  >::Invoke(0 /* System.Int32 System.IComparable`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (IndexedColumn_t674159988 )L_9);
		return L_10;
	}

IL_004d:
	{
		IndexedColumn_t674159988  L_11 = ___x0;
		IndexedColumn_t674159988  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		IndexedColumn_t674159988  L_14 = ___x0;
		IndexedColumn_t674159988  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		IndexedColumn_t674159988  L_17 = ___y1;
		IndexedColumn_t674159988  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Byte>::.ctor()
extern "C"  void DefaultComparer__ctor_m1321769145_gshared (DefaultComparer_t2172374669 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t2573113555 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t2573113555 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t2573113555 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Byte>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m667764052_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m667764052_gshared (DefaultComparer_t2172374669 * __this, uint8_t ___x0, uint8_t ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m667764052_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		uint8_t L_3 = ___x0;
		uint8_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		uint8_t L_6 = ___x0;
		uint8_t L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		uint8_t L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, uint8_t >::Invoke(0 /* System.Int32 System.IComparable`1<System.Byte>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (uint8_t)L_9);
		return L_10;
	}

IL_004d:
	{
		uint8_t L_11 = ___x0;
		uint8_t L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		uint8_t L_14 = ___x0;
		uint8_t L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		uint8_t L_17 = ___y1;
		uint8_t L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor()
extern "C"  void DefaultComparer__ctor_m1129894432_gshared (DefaultComparer_t2205520327 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t2606259213 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t2606259213 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t2606259213 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m3994715483_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m3994715483_gshared (DefaultComparer_t2205520327 * __this, KeyValuePair_2_t3716250094  ___x0, KeyValuePair_2_t3716250094  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m3994715483_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		KeyValuePair_2_t3716250094  L_3 = ___x0;
		KeyValuePair_2_t3716250094  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		KeyValuePair_2_t3716250094  L_6 = ___x0;
		KeyValuePair_2_t3716250094  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		KeyValuePair_2_t3716250094  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, KeyValuePair_2_t3716250094  >::Invoke(0 /* System.Int32 System.IComparable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (KeyValuePair_2_t3716250094 )L_9);
		return L_10;
	}

IL_004d:
	{
		KeyValuePair_2_t3716250094  L_11 = ___x0;
		KeyValuePair_2_t3716250094  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		KeyValuePair_2_t3716250094  L_14 = ___x0;
		KeyValuePair_2_t3716250094  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		KeyValuePair_2_t3716250094  L_17 = ___y1;
		KeyValuePair_2_t3716250094  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void DefaultComparer__ctor_m923018577_gshared (DefaultComparer_t2823092174 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t3223831060 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t3223831060 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t3223831060 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m1276226958_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m1276226958_gshared (DefaultComparer_t2823092174 * __this, KeyValuePair_2_t38854645  ___x0, KeyValuePair_2_t38854645  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m1276226958_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		KeyValuePair_2_t38854645  L_3 = ___x0;
		KeyValuePair_2_t38854645  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		KeyValuePair_2_t38854645  L_6 = ___x0;
		KeyValuePair_2_t38854645  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		KeyValuePair_2_t38854645  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, KeyValuePair_2_t38854645  >::Invoke(0 /* System.Int32 System.IComparable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (KeyValuePair_2_t38854645 )L_9);
		return L_10;
	}

IL_004d:
	{
		KeyValuePair_2_t38854645  L_11 = ___x0;
		KeyValuePair_2_t38854645  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		KeyValuePair_2_t38854645  L_14 = ___x0;
		KeyValuePair_2_t38854645  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		KeyValuePair_2_t38854645  L_17 = ___y1;
		KeyValuePair_2_t38854645  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>::.ctor()
extern "C"  void DefaultComparer__ctor_m1799227370_gshared (DefaultComparer_t3477443198 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t3878182084 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t3878182084 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t3878182084 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m1606207039_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m1606207039_gshared (DefaultComparer_t3477443198 * __this, DateTime_t693205669  ___x0, DateTime_t693205669  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m1606207039_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		DateTime_t693205669  L_3 = ___x0;
		DateTime_t693205669  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		DateTime_t693205669  L_6 = ___x0;
		DateTime_t693205669  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		DateTime_t693205669  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, DateTime_t693205669  >::Invoke(0 /* System.Int32 System.IComparable`1<System.DateTime>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (DateTime_t693205669 )L_9);
		return L_10;
	}

IL_004d:
	{
		DateTime_t693205669  L_11 = ___x0;
		DateTime_t693205669  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		DateTime_t693205669  L_14 = ___x0;
		DateTime_t693205669  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		DateTime_t693205669  L_17 = ___y1;
		DateTime_t693205669  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::.ctor()
extern "C"  void DefaultComparer__ctor_m732373515_gshared (DefaultComparer_t4147226435 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t252998025 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t252998025 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t252998025 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m3472472212_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m3472472212_gshared (DefaultComparer_t4147226435 * __this, DateTimeOffset_t1362988906  ___x0, DateTimeOffset_t1362988906  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m3472472212_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		DateTimeOffset_t1362988906  L_3 = ___x0;
		DateTimeOffset_t1362988906  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		DateTimeOffset_t1362988906  L_6 = ___x0;
		DateTimeOffset_t1362988906  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		DateTimeOffset_t1362988906  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, DateTimeOffset_t1362988906  >::Invoke(0 /* System.Int32 System.IComparable`1<System.DateTimeOffset>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (DateTimeOffset_t1362988906 )L_9);
		return L_10;
	}

IL_004d:
	{
		DateTimeOffset_t1362988906  L_11 = ___x0;
		DateTimeOffset_t1362988906  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		DateTimeOffset_t1362988906  L_14 = ___x0;
		DateTimeOffset_t1362988906  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		DateTimeOffset_t1362988906  L_17 = ___y1;
		DateTimeOffset_t1362988906  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::.ctor()
extern "C"  void DefaultComparer__ctor_m3668042_gshared (DefaultComparer_t1022871826 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1423610712 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t1423610712 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1423610712 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m3319119721_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m3319119721_gshared (DefaultComparer_t1022871826 * __this, Guid_t2533601593  ___x0, Guid_t2533601593  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m3319119721_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		Guid_t2533601593  L_3 = ___x0;
		Guid_t2533601593  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Guid_t2533601593  L_6 = ___x0;
		Guid_t2533601593  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		Guid_t2533601593  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Guid_t2533601593  >::Invoke(0 /* System.Int32 System.IComparable`1<System.Guid>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Guid_t2533601593 )L_9);
		return L_10;
	}

IL_004d:
	{
		Guid_t2533601593  L_11 = ___x0;
		Guid_t2533601593  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Guid_t2533601593  L_14 = ___x0;
		Guid_t2533601593  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		Guid_t2533601593  L_17 = ___y1;
		Guid_t2533601593  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::.ctor()
extern "C"  void DefaultComparer__ctor_m2859550749_gshared (DefaultComparer_t561147681 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t961886567 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t961886567 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t961886567 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m925902394_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m925902394_gshared (DefaultComparer_t561147681 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m925902394_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		int32_t L_3 = ___x0;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_6 = ___x0;
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		int32_t L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(0 /* System.Int32 System.IComparable`1<System.Int32>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (int32_t)L_9);
		return L_10;
	}

IL_004d:
	{
		int32_t L_11 = ___x0;
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		int32_t L_14 = ___x0;
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		int32_t L_17 = ___y1;
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>::.ctor()
extern "C"  void DefaultComparer__ctor_m84239532_gshared (DefaultComparer_t1178719528 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1579458414 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t1579458414 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1579458414 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m2805784815_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m2805784815_gshared (DefaultComparer_t1178719528 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m2805784815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		return 1;
	}

IL_002b:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject*)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Il2CppObject * L_4 = ___x0;
		Il2CppObject * L_5 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_6 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable`1<System.Object>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Il2CppObject *)L_5);
		return L_6;
	}

IL_004d:
	{
		Il2CppObject * L_7 = ___x0;
		if (!((Il2CppObject *)IsInst(L_7, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Il2CppObject * L_8 = ___x0;
		Il2CppObject * L_9 = ___y1;
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_8, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_8, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_9);
		return L_10;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_11 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_11, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void DefaultComparer__ctor_m1661558765_gshared (DefaultComparer_t2878395072 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t3279133958 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t3279133958 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t3279133958 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m2855268154_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m2855268154_gshared (DefaultComparer_t2878395072 * __this, CustomAttributeNamedArgument_t94157543  ___x0, CustomAttributeNamedArgument_t94157543  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m2855268154_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		CustomAttributeNamedArgument_t94157543  L_3 = ___x0;
		CustomAttributeNamedArgument_t94157543  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		CustomAttributeNamedArgument_t94157543  L_6 = ___x0;
		CustomAttributeNamedArgument_t94157543  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		CustomAttributeNamedArgument_t94157543  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, CustomAttributeNamedArgument_t94157543  >::Invoke(0 /* System.Int32 System.IComparable`1<System.Reflection.CustomAttributeNamedArgument>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (CustomAttributeNamedArgument_t94157543 )L_9);
		return L_10;
	}

IL_004d:
	{
		CustomAttributeNamedArgument_t94157543  L_11 = ___x0;
		CustomAttributeNamedArgument_t94157543  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		CustomAttributeNamedArgument_t94157543  L_14 = ___x0;
		CustomAttributeNamedArgument_t94157543  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		CustomAttributeNamedArgument_t94157543  L_17 = ___y1;
		CustomAttributeNamedArgument_t94157543  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void DefaultComparer__ctor_m1961329658_gshared (DefaultComparer_t4282435443 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t388207033 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t388207033 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t388207033 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m932294475_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m932294475_gshared (DefaultComparer_t4282435443 * __this, CustomAttributeTypedArgument_t1498197914  ___x0, CustomAttributeTypedArgument_t1498197914  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m932294475_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		CustomAttributeTypedArgument_t1498197914  L_3 = ___x0;
		CustomAttributeTypedArgument_t1498197914  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		CustomAttributeTypedArgument_t1498197914  L_6 = ___x0;
		CustomAttributeTypedArgument_t1498197914  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		CustomAttributeTypedArgument_t1498197914  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, CustomAttributeTypedArgument_t1498197914  >::Invoke(0 /* System.Int32 System.IComparable`1<System.Reflection.CustomAttributeTypedArgument>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (CustomAttributeTypedArgument_t1498197914 )L_9);
		return L_10;
	}

IL_004d:
	{
		CustomAttributeTypedArgument_t1498197914  L_11 = ___x0;
		CustomAttributeTypedArgument_t1498197914  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		CustomAttributeTypedArgument_t1498197914  L_14 = ___x0;
		CustomAttributeTypedArgument_t1498197914  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		CustomAttributeTypedArgument_t1498197914  L_17 = ___y1;
		CustomAttributeTypedArgument_t1498197914  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>::.ctor()
extern "C"  void DefaultComparer__ctor_m3791334730_gshared (DefaultComparer_t1919529182 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t2320268068 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t2320268068 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t2320268068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m265474847_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m265474847_gshared (DefaultComparer_t1919529182 * __this, TimeSpan_t3430258949  ___x0, TimeSpan_t3430258949  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m265474847_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		TimeSpan_t3430258949  L_3 = ___x0;
		TimeSpan_t3430258949  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		TimeSpan_t3430258949  L_6 = ___x0;
		TimeSpan_t3430258949  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		TimeSpan_t3430258949  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, TimeSpan_t3430258949  >::Invoke(0 /* System.Int32 System.IComparable`1<System.TimeSpan>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (TimeSpan_t3430258949 )L_9);
		return L_10;
	}

IL_004d:
	{
		TimeSpan_t3430258949  L_11 = ___x0;
		TimeSpan_t3430258949  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		TimeSpan_t3430258949  L_14 = ___x0;
		TimeSpan_t3430258949  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		TimeSpan_t3430258949  L_17 = ___y1;
		TimeSpan_t3430258949  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Color32>::.ctor()
extern "C"  void DefaultComparer__ctor_m2185307103_gshared (DefaultComparer_t3658755047 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t4059493933 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t4059493933 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t4059493933 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Color32>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m1247109616_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m1247109616_gshared (DefaultComparer_t3658755047 * __this, Color32_t874517518  ___x0, Color32_t874517518  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m1247109616_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		Color32_t874517518  L_3 = ___x0;
		Color32_t874517518  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Color32_t874517518  L_6 = ___x0;
		Color32_t874517518  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		Color32_t874517518  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Color32_t874517518  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.Color32>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Color32_t874517518 )L_9);
		return L_10;
	}

IL_004d:
	{
		Color32_t874517518  L_11 = ___x0;
		Color32_t874517518  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Color32_t874517518  L_14 = ___x0;
		Color32_t874517518  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		Color32_t874517518  L_17 = ___y1;
		Color32_t874517518  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C"  void DefaultComparer__ctor_m3180706193_gshared (DefaultComparer_t2805423905 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t3206162791 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t3206162791 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t3206162791 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m851771764_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m851771764_gshared (DefaultComparer_t2805423905 * __this, RaycastResult_t21186376  ___x0, RaycastResult_t21186376  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m851771764_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		RaycastResult_t21186376  L_3 = ___x0;
		RaycastResult_t21186376  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		RaycastResult_t21186376  L_6 = ___x0;
		RaycastResult_t21186376  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		RaycastResult_t21186376  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, RaycastResult_t21186376  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.EventSystems.RaycastResult>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (RaycastResult_t21186376 )L_9);
		return L_10;
	}

IL_004d:
	{
		RaycastResult_t21186376  L_11 = ___x0;
		RaycastResult_t21186376  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		RaycastResult_t21186376  L_14 = ___x0;
		RaycastResult_t21186376  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		RaycastResult_t21186376  L_17 = ___y1;
		RaycastResult_t21186376  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Experimental.Director.Playable>::.ctor()
extern "C"  void DefaultComparer__ctor_m76117625_gshared (DefaultComparer_t2156815781 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t2557554667 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t2557554667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t2557554667 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Experimental.Director.Playable>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m3277849110_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m3277849110_gshared (DefaultComparer_t2156815781 * __this, Playable_t3667545548  ___x0, Playable_t3667545548  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m3277849110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		Playable_t3667545548  L_3 = ___x0;
		Playable_t3667545548  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Playable_t3667545548  L_6 = ___x0;
		Playable_t3667545548  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		Playable_t3667545548  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Playable_t3667545548  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.Experimental.Director.Playable>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Playable_t3667545548 )L_9);
		return L_10;
	}

IL_004d:
	{
		Playable_t3667545548  L_11 = ___x0;
		Playable_t3667545548  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Playable_t3667545548  L_14 = ___x0;
		Playable_t3667545548  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		Playable_t3667545548  L_17 = ___y1;
		Playable_t3667545548  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.RuntimePlatform>::.ctor()
extern "C"  void DefaultComparer__ctor_m3088782776_gshared (DefaultComparer_t358855200 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t759594086 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t759594086 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t759594086 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.RuntimePlatform>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m1497044697_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m1497044697_gshared (DefaultComparer_t358855200 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m1497044697_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		int32_t L_3 = ___x0;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_6 = ___x0;
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		int32_t L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.RuntimePlatform>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (int32_t)L_9);
		return L_10;
	}

IL_004d:
	{
		int32_t L_11 = ___x0;
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		int32_t L_14 = ___x0;
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		int32_t L_17 = ___y1;
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m2470932885_gshared (DefaultComparer_t1545907033 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1946645919 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t1946645919 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1946645919 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m3386135912_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m3386135912_gshared (DefaultComparer_t1545907033 * __this, UICharInfo_t3056636800  ___x0, UICharInfo_t3056636800  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m3386135912_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		UICharInfo_t3056636800  L_3 = ___x0;
		UICharInfo_t3056636800  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		UICharInfo_t3056636800  L_6 = ___x0;
		UICharInfo_t3056636800  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		UICharInfo_t3056636800  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, UICharInfo_t3056636800  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.UICharInfo>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (UICharInfo_t3056636800 )L_9);
		return L_10;
	}

IL_004d:
	{
		UICharInfo_t3056636800  L_11 = ___x0;
		UICharInfo_t3056636800  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		UICharInfo_t3056636800  L_14 = ___x0;
		UICharInfo_t3056636800  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		UICharInfo_t3056636800  L_17 = ___y1;
		UICharInfo_t3056636800  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m709297127_gshared (DefaultComparer_t2110548107 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t2511286993 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t2511286993 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t2511286993 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m2804119458_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m2804119458_gshared (DefaultComparer_t2110548107 * __this, UILineInfo_t3621277874  ___x0, UILineInfo_t3621277874  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m2804119458_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		UILineInfo_t3621277874  L_3 = ___x0;
		UILineInfo_t3621277874  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		UILineInfo_t3621277874  L_6 = ___x0;
		UILineInfo_t3621277874  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		UILineInfo_t3621277874  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, UILineInfo_t3621277874  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.UILineInfo>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (UILineInfo_t3621277874 )L_9);
		return L_10;
	}

IL_004d:
	{
		UILineInfo_t3621277874  L_11 = ___x0;
		UILineInfo_t3621277874  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		UILineInfo_t3621277874  L_14 = ___x0;
		UILineInfo_t3621277874  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		UILineInfo_t3621277874  L_17 = ___y1;
		UILineInfo_t3621277874  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>::.ctor()
extern "C"  void DefaultComparer__ctor_m710539671_gshared (DefaultComparer_t3988496347 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t94267937 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t94267937 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t94267937 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m3564013922_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m3564013922_gshared (DefaultComparer_t3988496347 * __this, UIVertex_t1204258818  ___x0, UIVertex_t1204258818  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m3564013922_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		UIVertex_t1204258818  L_3 = ___x0;
		UIVertex_t1204258818  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		UIVertex_t1204258818  L_6 = ___x0;
		UIVertex_t1204258818  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		UIVertex_t1204258818  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, UIVertex_t1204258818  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.UIVertex>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (UIVertex_t1204258818 )L_9);
		return L_10;
	}

IL_004d:
	{
		UIVertex_t1204258818  L_11 = ___x0;
		UIVertex_t1204258818  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		UIVertex_t1204258818  L_14 = ___x0;
		UIVertex_t1204258818  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		UIVertex_t1204258818  L_17 = ___y1;
		UIVertex_t1204258818  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector2>::.ctor()
extern "C"  void DefaultComparer__ctor_m2251954164_gshared (DefaultComparer_t732977812 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1133716698 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t1133716698 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1133716698 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector2>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m3845579773_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m3845579773_gshared (DefaultComparer_t732977812 * __this, Vector2_t2243707579  ___x0, Vector2_t2243707579  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m3845579773_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		Vector2_t2243707579  L_3 = ___x0;
		Vector2_t2243707579  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Vector2_t2243707579  L_6 = ___x0;
		Vector2_t2243707579  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		Vector2_t2243707579  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Vector2_t2243707579  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.Vector2>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Vector2_t2243707579 )L_9);
		return L_10;
	}

IL_004d:
	{
		Vector2_t2243707579  L_11 = ___x0;
		Vector2_t2243707579  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Vector2_t2243707579  L_14 = ___x0;
		Vector2_t2243707579  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		Vector2_t2243707579  L_17 = ___y1;
		Vector2_t2243707579  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector3>::.ctor()
extern "C"  void DefaultComparer__ctor_m1454979065_gshared (DefaultComparer_t732977813 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1133716699 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t1133716699 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1133716699 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector3>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m2469517726_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m2469517726_gshared (DefaultComparer_t732977813 * __this, Vector3_t2243707580  ___x0, Vector3_t2243707580  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m2469517726_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		Vector3_t2243707580  L_3 = ___x0;
		Vector3_t2243707580  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Vector3_t2243707580  L_6 = ___x0;
		Vector3_t2243707580  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		Vector3_t2243707580  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Vector3_t2243707580  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.Vector3>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Vector3_t2243707580 )L_9);
		return L_10;
	}

IL_004d:
	{
		Vector3_t2243707580  L_11 = ___x0;
		Vector3_t2243707580  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Vector3_t2243707580  L_14 = ___x0;
		Vector3_t2243707580  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		Vector3_t2243707580  L_17 = ___y1;
		Vector3_t2243707580  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector4>::.ctor()
extern "C"  void DefaultComparer__ctor_m3680166634_gshared (DefaultComparer_t732977814 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1133716700 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t1133716700 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1133716700 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector4>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m4039941311_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m4039941311_gshared (DefaultComparer_t732977814 * __this, Vector4_t2243707581  ___x0, Vector4_t2243707581  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m4039941311_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		Vector4_t2243707581  L_3 = ___x0;
		Vector4_t2243707581  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Vector4_t2243707581  L_6 = ___x0;
		Vector4_t2243707581  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		Vector4_t2243707581  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Vector4_t2243707581  >::Invoke(0 /* System.Int32 System.IComparable`1<UnityEngine.Vector4>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Vector4_t2243707581 )L_9);
		return L_10;
	}

IL_004d:
	{
		Vector4_t2243707581  L_11 = ___x0;
		Vector4_t2243707581  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Vector4_t2243707581  L_14 = ___x0;
		Vector4_t2243707581  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		Vector4_t2243707581  L_17 = ___y1;
		Vector4_t2243707581  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>::.ctor()
extern "C"  void DefaultComparer__ctor_m1086799688_gshared (DefaultComparer_t1499800277 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1900539163 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t1900539163 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1900539163 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m401826535_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m401826535_gshared (DefaultComparer_t1499800277 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m401826535_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		int32_t L_3 = ___x0;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_6 = ___x0;
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		int32_t L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(0 /* System.Int32 System.IComparable`1<Vuforia.Image/PIXEL_FORMAT>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (int32_t)L_9);
		return L_10;
	}

IL_004d:
	{
		int32_t L_11 = ___x0;
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		int32_t L_14 = ___x0;
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		int32_t L_17 = ___y1;
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C"  void DefaultComparer__ctor_m552162196_gshared (DefaultComparer_t447996739 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t848735625 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t848735625 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t848735625 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::Compare(T,T)
extern Il2CppClass* IComparable_t1857082765_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864089402;
extern const uint32_t DefaultComparer_Compare_m1685913661_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m1685913661_gshared (DefaultComparer_t447996739 * __this, TargetSearchResult_t1958726506  ___x0, TargetSearchResult_t1958726506  ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m1685913661_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		TargetSearchResult_t1958726506  L_3 = ___x0;
		TargetSearchResult_t1958726506  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		TargetSearchResult_t1958726506  L_6 = ___x0;
		TargetSearchResult_t1958726506  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_7);
		TargetSearchResult_t1958726506  L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, TargetSearchResult_t1958726506  >::Invoke(0 /* System.Int32 System.IComparable`1<Vuforia.TargetFinder/TargetSearchResult>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (TargetSearchResult_t1958726506 )L_9);
		return L_10;
	}

IL_004d:
	{
		TargetSearchResult_t1958726506  L_11 = ___x0;
		TargetSearchResult_t1958726506  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1857082765_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		TargetSearchResult_t1958726506  L_14 = ___x0;
		TargetSearchResult_t1958726506  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_15);
		TargetSearchResult_t1958726506  L_17 = ___y1;
		TargetSearchResult_t1958726506  L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1857082765_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1857082765_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t3259014390 * L_21 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_21, (String_t*)_stringLiteral1864089402, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor()
extern "C"  void Comparer_1__ctor_m534919979_gshared (Comparer_1_t3859136403 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m1579751478_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m1579751478_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m1579751478_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t3859136403_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t3859136403 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3458397517 * L_8 = (DefaultComparer_t3458397517 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3458397517 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t3859136403_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m407422248_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m407422248_gshared (Comparer_1_t3859136403 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m407422248_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t3859136403 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, IndexedColumn_t674159988 , IndexedColumn_t674159988  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Compare(T,T) */, (Comparer_1_t3859136403 *)__this, (IndexedColumn_t674159988 )((*(IndexedColumn_t674159988 *)((IndexedColumn_t674159988 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (IndexedColumn_t674159988 )((*(IndexedColumn_t674159988 *)((IndexedColumn_t674159988 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::get_Default()
extern "C"  Comparer_1_t3859136403 * Comparer_1_get_Default_m2968249755_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t3859136403 * L_0 = ((Comparer_1_t3859136403_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Byte>::.ctor()
extern "C"  void Comparer_1__ctor_m4256167488_gshared (Comparer_1_t2573113555 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Byte>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m2347159423_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m2347159423_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m2347159423_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t2573113555_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t2573113555 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2172374669 * L_8 = (DefaultComparer_t2172374669 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2172374669 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t2573113555_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Byte>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2027162031_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2027162031_gshared (Comparer_1_t2573113555 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2027162031_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t2573113555 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, uint8_t, uint8_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Byte>::Compare(T,T) */, (Comparer_1_t2573113555 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Byte>::get_Default()
extern "C"  Comparer_1_t2573113555 * Comparer_1_get_Default_m2982649620_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t2573113555 * L_0 = ((Comparer_1_t2573113555_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor()
extern "C"  void Comparer_1__ctor_m3155632455_gshared (Comparer_1_t2606259213 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m223060316_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m223060316_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m223060316_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t2606259213_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t2606259213 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2205520327 * L_8 = (DefaultComparer_t2205520327 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2205520327 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t2606259213_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m3196814058_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m3196814058_gshared (Comparer_1_t2606259213 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m3196814058_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t2606259213 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, KeyValuePair_2_t3716250094 , KeyValuePair_2_t3716250094  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Compare(T,T) */, (Comparer_1_t2606259213 *)__this, (KeyValuePair_2_t3716250094 )((*(KeyValuePair_2_t3716250094 *)((KeyValuePair_2_t3716250094 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (KeyValuePair_2_t3716250094 )((*(KeyValuePair_2_t3716250094 *)((KeyValuePair_2_t3716250094 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Default()
extern "C"  Comparer_1_t2606259213 * Comparer_1_get_Default_m3023135_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t2606259213 * L_0 = ((Comparer_1_t2606259213_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void Comparer_1__ctor_m3401410816_gshared (Comparer_1_t3223831060 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m3449696399_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m3449696399_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m3449696399_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t3223831060_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t3223831060 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2823092174 * L_8 = (DefaultComparer_t2823092174 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2823092174 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t3223831060_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m4286853351_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m4286853351_gshared (Comparer_1_t3223831060 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m4286853351_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t3223831060 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, KeyValuePair_2_t38854645 , KeyValuePair_2_t38854645  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Compare(T,T) */, (Comparer_1_t3223831060 *)__this, (KeyValuePair_2_t38854645 )((*(KeyValuePair_2_t38854645 *)((KeyValuePair_2_t38854645 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (KeyValuePair_2_t38854645 )((*(KeyValuePair_2_t38854645 *)((KeyValuePair_2_t38854645 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Default()
extern "C"  Comparer_1_t3223831060 * Comparer_1_get_Default_m4153723304_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t3223831060 * L_0 = ((Comparer_1_t3223831060_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.DateTime>::.ctor()
extern "C"  void Comparer_1__ctor_m1202126643_gshared (Comparer_1_t3878182084 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.DateTime>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m1367179810_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m1367179810_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m1367179810_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t3878182084_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t3878182084 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3477443198 * L_8 = (DefaultComparer_t3477443198 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3477443198 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t3878182084_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.DateTime>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m1712675620_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1712675620_gshared (Comparer_1_t3878182084 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m1712675620_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t3878182084 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, DateTime_t693205669 , DateTime_t693205669  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.DateTime>::Compare(T,T) */, (Comparer_1_t3878182084 *)__this, (DateTime_t693205669 )((*(DateTime_t693205669 *)((DateTime_t693205669 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (DateTime_t693205669 )((*(DateTime_t693205669 *)((DateTime_t693205669 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.DateTime>::get_Default()
extern "C"  Comparer_1_t3878182084 * Comparer_1_get_Default_m3737432123_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t3878182084 * L_0 = ((Comparer_1_t3878182084_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.DateTimeOffset>::.ctor()
extern "C"  void Comparer_1__ctor_m3855093372_gshared (Comparer_1_t252998025 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.DateTimeOffset>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m2809342737_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m2809342737_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m2809342737_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t252998025_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t252998025 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t4147226435 * L_8 = (DefaultComparer_t4147226435 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t4147226435 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t252998025_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.DateTimeOffset>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m1790257529_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1790257529_gshared (Comparer_1_t252998025 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m1790257529_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t252998025 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, DateTimeOffset_t1362988906 , DateTimeOffset_t1362988906  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.DateTimeOffset>::Compare(T,T) */, (Comparer_1_t252998025 *)__this, (DateTimeOffset_t1362988906 )((*(DateTimeOffset_t1362988906 *)((DateTimeOffset_t1362988906 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (DateTimeOffset_t1362988906 )((*(DateTimeOffset_t1362988906 *)((DateTimeOffset_t1362988906 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.DateTimeOffset>::get_Default()
extern "C"  Comparer_1_t252998025 * Comparer_1_get_Default_m1766380520_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t252998025 * L_0 = ((Comparer_1_t252998025_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Guid>::.ctor()
extern "C"  void Comparer_1__ctor_m2876014041_gshared (Comparer_1_t1423610712 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Guid>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m3801958574_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m3801958574_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m3801958574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1423610712_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1423610712 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1022871826 * L_8 = (DefaultComparer_t1022871826 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1022871826 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1423610712_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Guid>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m674728644_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m674728644_gshared (Comparer_1_t1423610712 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m674728644_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1423610712 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Guid_t2533601593 , Guid_t2533601593  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Guid>::Compare(T,T) */, (Comparer_1_t1423610712 *)__this, (Guid_t2533601593 )((*(Guid_t2533601593 *)((Guid_t2533601593 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Guid_t2533601593 )((*(Guid_t2533601593 *)((Guid_t2533601593 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Guid>::get_Default()
extern "C"  Comparer_1_t1423610712 * Comparer_1_get_Default_m3982792633_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1423610712 * L_0 = ((Comparer_1_t1423610712_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Int32>::.ctor()
extern "C"  void Comparer_1__ctor_m2074421588_gshared (Comparer_1_t961886567 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Int32>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m2780604723_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m2780604723_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m2780604723_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t961886567_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t961886567 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t561147681 * L_8 = (DefaultComparer_t561147681 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t561147681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t961886567_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Int32>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m3477896499_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m3477896499_gshared (Comparer_1_t961886567 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m3477896499_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t961886567 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Int32>::Compare(T,T) */, (Comparer_1_t961886567 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Int32>::get_Default()
extern "C"  Comparer_1_t961886567 * Comparer_1_get_Default_m699808348_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t961886567 * L_0 = ((Comparer_1_t961886567_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Object>::.ctor()
extern "C"  void Comparer_1__ctor_m4082958187_gshared (Comparer_1_t1579458414 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Object>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m2962395036_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m2962395036_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m2962395036_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1579458414_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1579458414 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1178719528 * L_8 = (DefaultComparer_t1178719528 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1178719528 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1579458414_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Object>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m872902762_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m872902762_gshared (Comparer_1_t1579458414 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m872902762_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1579458414 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Object>::Compare(T,T) */, (Comparer_1_t1579458414 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (Il2CppObject *)((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Object>::get_Default()
extern "C"  Comparer_1_t1579458414 * Comparer_1_get_Default_m40106963_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1579458414 * L_0 = ((Comparer_1_t1579458414_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void Comparer_1__ctor_m844571340_gshared (Comparer_1_t3279133958 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeNamedArgument>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m3112251759_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m3112251759_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m3112251759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t3279133958_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t3279133958 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2878395072 * L_8 = (DefaultComparer_t2878395072 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2878395072 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t3279133958_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m3203078743_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m3203078743_gshared (Comparer_1_t3279133958 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m3203078743_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t3279133958 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, CustomAttributeNamedArgument_t94157543 , CustomAttributeNamedArgument_t94157543  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeNamedArgument>::Compare(T,T) */, (Comparer_1_t3279133958 *)__this, (CustomAttributeNamedArgument_t94157543 )((*(CustomAttributeNamedArgument_t94157543 *)((CustomAttributeNamedArgument_t94157543 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (CustomAttributeNamedArgument_t94157543 )((*(CustomAttributeNamedArgument_t94157543 *)((CustomAttributeNamedArgument_t94157543 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeNamedArgument>::get_Default()
extern "C"  Comparer_1_t3279133958 * Comparer_1_get_Default_m2605397692_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t3279133958 * L_0 = ((Comparer_1_t3279133958_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void Comparer_1__ctor_m2364183619_gshared (Comparer_1_t388207033 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeTypedArgument>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m580294992_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m580294992_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m580294992_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t388207033_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t388207033 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t4282435443 * L_8 = (DefaultComparer_t4282435443 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t4282435443 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t388207033_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m1635186002_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1635186002_gshared (Comparer_1_t388207033 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m1635186002_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t388207033 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, CustomAttributeTypedArgument_t1498197914 , CustomAttributeTypedArgument_t1498197914  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeTypedArgument>::Compare(T,T) */, (Comparer_1_t388207033 *)__this, (CustomAttributeTypedArgument_t1498197914 )((*(CustomAttributeTypedArgument_t1498197914 *)((CustomAttributeTypedArgument_t1498197914 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (CustomAttributeTypedArgument_t1498197914 )((*(CustomAttributeTypedArgument_t1498197914 *)((CustomAttributeTypedArgument_t1498197914 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeTypedArgument>::get_Default()
extern "C"  Comparer_1_t388207033 * Comparer_1_get_Default_m3643271627_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t388207033 * L_0 = ((Comparer_1_t388207033_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.TimeSpan>::.ctor()
extern "C"  void Comparer_1__ctor_m2195903267_gshared (Comparer_1_t2320268068 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.TimeSpan>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m2494715342_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m2494715342_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m2494715342_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t2320268068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t2320268068 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1919529182 * L_8 = (DefaultComparer_t1919529182 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1919529182 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t2320268068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.TimeSpan>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2490067344_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2490067344_gshared (Comparer_1_t2320268068 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2490067344_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t2320268068 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, TimeSpan_t3430258949 , TimeSpan_t3430258949  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.TimeSpan>::Compare(T,T) */, (Comparer_1_t2320268068 *)__this, (TimeSpan_t3430258949 )((*(TimeSpan_t3430258949 *)((TimeSpan_t3430258949 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (TimeSpan_t3430258949 )((*(TimeSpan_t3430258949 *)((TimeSpan_t3430258949 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.TimeSpan>::get_Default()
extern "C"  Comparer_1_t2320268068 * Comparer_1_get_Default_m2204997355_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t2320268068 * L_0 = ((Comparer_1_t2320268068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Color32>::.ctor()
extern "C"  void Comparer_1__ctor_m2264852056_gshared (Comparer_1_t4059493933 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Color32>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m179359609_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m179359609_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m179359609_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t4059493933_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t4059493933 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3658755047 * L_8 = (DefaultComparer_t3658755047 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3658755047 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t4059493933_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Color32>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2785607073_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2785607073_gshared (Comparer_1_t4059493933 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2785607073_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t4059493933 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Color32_t874517518 , Color32_t874517518  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Color32>::Compare(T,T) */, (Comparer_1_t4059493933 *)__this, (Color32_t874517518 )((*(Color32_t874517518 *)((Color32_t874517518 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Color32_t874517518 )((*(Color32_t874517518 *)((Color32_t874517518 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Color32>::get_Default()
extern "C"  Comparer_1_t4059493933 * Comparer_1_get_Default_m1826646524_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t4059493933 * L_0 = ((Comparer_1_t4059493933_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C"  void Comparer_1__ctor_m1728777074_gshared (Comparer_1_t3206162791 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.RaycastResult>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m3237813171_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m3237813171_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m3237813171_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t3206162791_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t3206162791 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2805423905 * L_8 = (DefaultComparer_t2805423905 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2805423905 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t3206162791_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m1153499515_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1153499515_gshared (Comparer_1_t3206162791 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m1153499515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t3206162791 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, RaycastResult_t21186376 , RaycastResult_t21186376  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.RaycastResult>::Compare(T,T) */, (Comparer_1_t3206162791 *)__this, (RaycastResult_t21186376 )((*(RaycastResult_t21186376 *)((RaycastResult_t21186376 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (RaycastResult_t21186376 )((*(RaycastResult_t21186376 *)((RaycastResult_t21186376 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.RaycastResult>::get_Default()
extern "C"  Comparer_1_t3206162791 * Comparer_1_get_Default_m4282764954_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t3206162791 * L_0 = ((Comparer_1_t3206162791_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Experimental.Director.Playable>::.ctor()
extern "C"  void Comparer_1__ctor_m105573688_gshared (Comparer_1_t2557554667 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Experimental.Director.Playable>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m1146589207_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m1146589207_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m1146589207_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t2557554667_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t2557554667 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2156815781 * L_8 = (DefaultComparer_t2156815781 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2156815781 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t2557554667_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Experimental.Director.Playable>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m4240302767_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m4240302767_gshared (Comparer_1_t2557554667 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m4240302767_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t2557554667 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Playable_t3667545548 , Playable_t3667545548  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Experimental.Director.Playable>::Compare(T,T) */, (Comparer_1_t2557554667 *)__this, (Playable_t3667545548 )((*(Playable_t3667545548 *)((Playable_t3667545548 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Playable_t3667545548 )((*(Playable_t3667545548 *)((Playable_t3667545548 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Experimental.Director.Playable>::get_Default()
extern "C"  Comparer_1_t2557554667 * Comparer_1_get_Default_m1078895976_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t2557554667 * L_0 = ((Comparer_1_t2557554667_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.RuntimePlatform>::.ctor()
extern "C"  void Comparer_1__ctor_m2685011545_gshared (Comparer_1_t759594086 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.RuntimePlatform>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m1057157428_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m1057157428_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m1057157428_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t759594086_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t759594086 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t358855200 * L_8 = (DefaultComparer_t358855200 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t358855200 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t759594086_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.RuntimePlatform>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m479919198_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m479919198_gshared (Comparer_1_t759594086 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m479919198_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t759594086 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.RuntimePlatform>::Compare(T,T) */, (Comparer_1_t759594086 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.RuntimePlatform>::get_Default()
extern "C"  Comparer_1_t759594086 * Comparer_1_get_Default_m2285864681_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t759594086 * L_0 = ((Comparer_1_t759594086_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::.ctor()
extern "C"  void Comparer_1__ctor_m1184061702_gshared (Comparer_1_t1946645919 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m3069041651_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m3069041651_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m3069041651_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1946645919_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1946645919 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1545907033 * L_8 = (DefaultComparer_t1545907033 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1545907033 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1946645919_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m1621919467_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1621919467_gshared (Comparer_1_t1946645919 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m1621919467_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1946645919 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, UICharInfo_t3056636800 , UICharInfo_t3056636800  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::Compare(T,T) */, (Comparer_1_t1946645919 *)__this, (UICharInfo_t3056636800 )((*(UICharInfo_t3056636800 *)((UICharInfo_t3056636800 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (UICharInfo_t3056636800 )((*(UICharInfo_t3056636800 *)((UICharInfo_t3056636800 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::get_Default()
extern "C"  Comparer_1_t1946645919 * Comparer_1_get_Default_m91842798_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1946645919 * L_0 = ((Comparer_1_t1946645919_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::.ctor()
extern "C"  void Comparer_1__ctor_m806168336_gshared (Comparer_1_t2511286993 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m3996541505_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m3996541505_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m3996541505_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t2511286993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t2511286993 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2110548107 * L_8 = (DefaultComparer_t2110548107 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2110548107 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t2511286993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2964757477_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2964757477_gshared (Comparer_1_t2511286993 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2964757477_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t2511286993 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, UILineInfo_t3621277874 , UILineInfo_t3621277874  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::Compare(T,T) */, (Comparer_1_t2511286993 *)__this, (UILineInfo_t3621277874 )((*(UILineInfo_t3621277874 *)((UILineInfo_t3621277874 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (UILineInfo_t3621277874 )((*(UILineInfo_t3621277874 *)((UILineInfo_t3621277874 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::get_Default()
extern "C"  Comparer_1_t2511286993 * Comparer_1_get_Default_m501796660_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t2511286993 * L_0 = ((Comparer_1_t2511286993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::.ctor()
extern "C"  void Comparer_1__ctor_m1157133632_gshared (Comparer_1_t94267937 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m4067993089_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m4067993089_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m4067993089_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t94267937_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t94267937 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3988496347 * L_8 = (DefaultComparer_t3988496347 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3988496347 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t94267937_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2324509253_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2324509253_gshared (Comparer_1_t94267937 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2324509253_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t94267937 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, UIVertex_t1204258818 , UIVertex_t1204258818  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::Compare(T,T) */, (Comparer_1_t94267937 *)__this, (UIVertex_t1204258818 )((*(UIVertex_t1204258818 *)((UIVertex_t1204258818 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (UIVertex_t1204258818 )((*(UIVertex_t1204258818 *)((UIVertex_t1204258818 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::get_Default()
extern "C"  Comparer_1_t94267937 * Comparer_1_get_Default_m1960140044_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t94267937 * L_0 = ((Comparer_1_t94267937_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::.ctor()
extern "C"  void Comparer_1__ctor_m2941434245_gshared (Comparer_1_t1133716698 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m2253684996_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m2253684996_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m2253684996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1133716698_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1133716698 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t732977812 * L_8 = (DefaultComparer_t732977812 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t732977812 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1133716698_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m637596782_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m637596782_gshared (Comparer_1_t1133716698 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m637596782_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1133716698 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Vector2_t2243707579 , Vector2_t2243707579  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::Compare(T,T) */, (Comparer_1_t1133716698 *)__this, (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::get_Default()
extern "C"  Comparer_1_t1133716698 * Comparer_1_get_Default_m492688901_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1133716698 * L_0 = ((Comparer_1_t1133716698_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::.ctor()
extern "C"  void Comparer_1__ctor_m1169723274_gshared (Comparer_1_t1133716699 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m1573451391_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m1573451391_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m1573451391_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1133716699_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1133716699 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t732977813 * L_8 = (DefaultComparer_t732977813 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t732977813 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1133716699_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2615431023_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2615431023_gshared (Comparer_1_t1133716699 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2615431023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1133716699 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Vector3_t2243707580 , Vector3_t2243707580  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::Compare(T,T) */, (Comparer_1_t1133716699 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::get_Default()
extern "C"  Comparer_1_t1133716699 * Comparer_1_get_Default_m3185432070_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1133716699 * L_0 = ((Comparer_1_t1133716699_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::.ctor()
extern "C"  void Comparer_1__ctor_m4052560291_gshared (Comparer_1_t1133716700 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m1911230094_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m1911230094_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m1911230094_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1133716700_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1133716700 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t732977814 * L_8 = (DefaultComparer_t732977814 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t732977814 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1133716700_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m577428976_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m577428976_gshared (Comparer_1_t1133716700 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m577428976_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1133716700 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Vector4_t2243707581 , Vector4_t2243707581  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::Compare(T,T) */, (Comparer_1_t1133716700 *)__this, (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::get_Default()
extern "C"  Comparer_1_t1133716700 * Comparer_1_get_Default_m48739979_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1133716700 * L_0 = ((Comparer_1_t1133716700_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<Vuforia.Image/PIXEL_FORMAT>::.ctor()
extern "C"  void Comparer_1__ctor_m293299247_gshared (Comparer_1_t1900539163 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<Vuforia.Image/PIXEL_FORMAT>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m4276151266_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m4276151266_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m4276151266_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1900539163_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1900539163 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1499800277 * L_8 = (DefaultComparer_t1499800277 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1499800277 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1900539163_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2410890752_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2410890752_gshared (Comparer_1_t1900539163 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2410890752_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1900539163 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<Vuforia.Image/PIXEL_FORMAT>::Compare(T,T) */, (Comparer_1_t1900539163 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.Image/PIXEL_FORMAT>::get_Default()
extern "C"  Comparer_1_t1900539163 * Comparer_1_get_Default_m2524906639_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1900539163 * L_0 = ((Comparer_1_t1900539163_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C"  void Comparer_1__ctor_m3872802565_gshared (Comparer_1_t848735625 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m924623556_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m924623556_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m924623556_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(85 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t848735625_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t848735625 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t447996739 * L_8 = (DefaultComparer_t447996739 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t447996739 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t848735625_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m3145824206_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m3145824206_gshared (Comparer_1_t848735625 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m3145824206_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t848735625 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, TargetSearchResult_t1958726506 , TargetSearchResult_t1958726506  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>::Compare(T,T) */, (Comparer_1_t848735625 *)__this, (TargetSearchResult_t1958726506 )((*(TargetSearchResult_t1958726506 *)((TargetSearchResult_t1958726506 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (TargetSearchResult_t1958726506 )((*(TargetSearchResult_t1958726506 *)((TargetSearchResult_t1958726506 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>::get_Default()
extern "C"  Comparer_1_t848735625 * Comparer_1_get_Default_m2408258445_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t848735625 * L_0 = ((Comparer_1_t848735625_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1702560852_gshared (Enumerator_t3017299632 * __this, Dictionary_2_t1697274930 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t1697274930 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1702560852_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1697274930 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator__ctor_m1702560852(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4246196125((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3749587448  L_0 = (KeyValuePair_2_t3749587448 )__this->get_current_3();
		KeyValuePair_2_t3749587448  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1631145297(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m761796566((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2828524109_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2828524109(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4246196125((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3749587448 * L_0 = (KeyValuePair_2_t3749587448 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t3749587448 * L_4 = (KeyValuePair_2_t3749587448 *)__this->get_address_of_current_3();
		Il2CppObject * L_5 = KeyValuePair_2_get_Value_m3690000728((KeyValuePair_2_t3749587448 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentKey_m447338908((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentValue_m3562053380((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2770956757_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m2118679243((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1697274930 * L_4 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1697274930 * L_8 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck(L_8);
		Int32U5BU5D_t3030399641* L_9 = (Int32U5BU5D_t3030399641*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t1697274930 * L_13 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t3749587448  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m3201181706(&L_18, (int32_t)L_12, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t1697274930 * L_20 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2770956757_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_MoveNext_m2770956757(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t3749587448  Enumerator_get_Current_m2230224741_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3749587448  L_0 = (KeyValuePair_2_t3749587448 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t3749587448  Enumerator_get_Current_m2230224741_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_get_Current_m2230224741(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m447338908_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4246196125((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3749587448 * L_0 = (KeyValuePair_2_t3749587448 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentKey_m447338908_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_get_CurrentKey_m447338908(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3562053380_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4246196125((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3749587448 * L_0 = (KeyValuePair_2_t3749587448 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m3690000728((KeyValuePair_2_t3749587448 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3562053380_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_get_CurrentValue_m3562053380(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m761796566_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2118679243((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m761796566_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator_Reset_m761796566(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m2118679243_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2118679243_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2118679243_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t1697274930 * L_2 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2118679243_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator_VerifyState_m2118679243(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m4246196125_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m4246196125_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m4246196125_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m2118679243((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m4246196125_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator_VerifyCurrent_m4246196125(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2243145188_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t1697274930 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2243145188_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator_Dispose_m2243145188(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m725289720_gshared (Enumerator_t2275378311 * __this, Dictionary_2_t955353609 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t955353609 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t955353609 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m725289720_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t955353609 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t2275378311 * _thisAdjusted = reinterpret_cast<Enumerator_t2275378311 *>(__this + 1);
	Enumerator__ctor_m725289720(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2736581469_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2386527189((Enumerator_t2275378311 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3007666127  L_0 = (KeyValuePair_2_t3007666127 )__this->get_current_3();
		KeyValuePair_2_t3007666127  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2736581469_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2275378311 * _thisAdjusted = reinterpret_cast<Enumerator_t2275378311 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2736581469(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3226745029_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m326090910((Enumerator_t2275378311 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3226745029_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2275378311 * _thisAdjusted = reinterpret_cast<Enumerator_t2275378311 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3226745029(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1706763446_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2386527189((Enumerator_t2275378311 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3007666127 * L_0 = (KeyValuePair_2_t3007666127 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m2161654122((KeyValuePair_2_t3007666127 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t3007666127 * L_4 = (KeyValuePair_2_t3007666127 *)__this->get_address_of_current_3();
		TrackableResultData_t1947527974  L_5 = KeyValuePair_2_get_Value_m616722410((KeyValuePair_2_t3007666127 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		TrackableResultData_t1947527974  L_6 = L_5;
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_6);
		DictionaryEntry_t3048875398  L_8;
		memset(&L_8, 0, sizeof(L_8));
		DictionaryEntry__ctor_m2901884110(&L_8, (Il2CppObject *)L_3, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1706763446_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2275378311 * _thisAdjusted = reinterpret_cast<Enumerator_t2275378311 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1706763446(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m691547087_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentKey_m1027350056((Enumerator_t2275378311 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m691547087_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2275378311 * _thisAdjusted = reinterpret_cast<Enumerator_t2275378311 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m691547087(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2696031375_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method)
{
	{
		TrackableResultData_t1947527974  L_0 = Enumerator_get_CurrentValue_m602212904((Enumerator_t2275378311 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		TrackableResultData_t1947527974  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2696031375_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2275378311 * _thisAdjusted = reinterpret_cast<Enumerator_t2275378311 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2696031375(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2540159901_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m669141279((Enumerator_t2275378311 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t955353609 * L_4 = (Dictionary_2_t955353609 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t955353609 * L_8 = (Dictionary_2_t955353609 *)__this->get_dictionary_0();
		NullCheck(L_8);
		Int32U5BU5D_t3030399641* L_9 = (Int32U5BU5D_t3030399641*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t955353609 * L_13 = (Dictionary_2_t955353609 *)__this->get_dictionary_0();
		NullCheck(L_13);
		TrackableResultDataU5BU5D_t1439643011* L_14 = (TrackableResultDataU5BU5D_t1439643011*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		TrackableResultData_t1947527974  L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t3007666127  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m4017094104(&L_18, (int32_t)L_12, (TrackableResultData_t1947527974 )L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t955353609 * L_20 = (Dictionary_2_t955353609 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2540159901_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2275378311 * _thisAdjusted = reinterpret_cast<Enumerator_t2275378311 *>(__this + 1);
	return Enumerator_MoveNext_m2540159901(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern "C"  KeyValuePair_2_t3007666127  Enumerator_get_Current_m2904646657_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3007666127  L_0 = (KeyValuePair_2_t3007666127 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t3007666127  Enumerator_get_Current_m2904646657_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2275378311 * _thisAdjusted = reinterpret_cast<Enumerator_t2275378311 *>(__this + 1);
	return Enumerator_get_Current_m2904646657(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m1027350056_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2386527189((Enumerator_t2275378311 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3007666127 * L_0 = (KeyValuePair_2_t3007666127 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m2161654122((KeyValuePair_2_t3007666127 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentKey_m1027350056_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2275378311 * _thisAdjusted = reinterpret_cast<Enumerator_t2275378311 *>(__this + 1);
	return Enumerator_get_CurrentKey_m1027350056(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_CurrentValue()
extern "C"  TrackableResultData_t1947527974  Enumerator_get_CurrentValue_m602212904_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2386527189((Enumerator_t2275378311 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3007666127 * L_0 = (KeyValuePair_2_t3007666127 *)__this->get_address_of_current_3();
		TrackableResultData_t1947527974  L_1 = KeyValuePair_2_get_Value_m616722410((KeyValuePair_2_t3007666127 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  TrackableResultData_t1947527974  Enumerator_get_CurrentValue_m602212904_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2275378311 * _thisAdjusted = reinterpret_cast<Enumerator_t2275378311 *>(__this + 1);
	return Enumerator_get_CurrentValue_m602212904(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Reset()
extern "C"  void Enumerator_Reset_m326090910_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m669141279((Enumerator_t2275378311 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m326090910_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2275378311 * _thisAdjusted = reinterpret_cast<Enumerator_t2275378311 *>(__this + 1);
	Enumerator_Reset_m326090910(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m669141279_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m669141279_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m669141279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t955353609 * L_0 = (Dictionary_2_t955353609 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t955353609 * L_2 = (Dictionary_2_t955353609 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m669141279_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2275378311 * _thisAdjusted = reinterpret_cast<Enumerator_t2275378311 *>(__this + 1);
	Enumerator_VerifyState_m669141279(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m2386527189_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m2386527189_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m2386527189_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m669141279((Enumerator_t2275378311 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m2386527189_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2275378311 * _thisAdjusted = reinterpret_cast<Enumerator_t2275378311 *>(__this + 1);
	Enumerator_VerifyCurrent_m2386527189(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Dispose()
extern "C"  void Enumerator_Dispose_m560967656_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t955353609 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m560967656_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2275378311 * _thisAdjusted = reinterpret_cast<Enumerator_t2275378311 *>(__this + 1);
	Enumerator_Dispose_m560967656(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4066539995_gshared (Enumerator_t1551735988 * __this, Dictionary_2_t231711286 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t231711286 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t231711286 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m4066539995_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t231711286 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t1551735988 * _thisAdjusted = reinterpret_cast<Enumerator_t1551735988 *>(__this + 1);
	Enumerator__ctor_m4066539995(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2370880042_gshared (Enumerator_t1551735988 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1739478830((Enumerator_t1551735988 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2284023804  L_0 = (KeyValuePair_2_t2284023804 )__this->get_current_3();
		KeyValuePair_2_t2284023804  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2370880042_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1551735988 * _thisAdjusted = reinterpret_cast<Enumerator_t1551735988 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2370880042(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1930039422_gshared (Enumerator_t1551735988 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m497205((Enumerator_t1551735988 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1930039422_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1551735988 * _thisAdjusted = reinterpret_cast<Enumerator_t1551735988 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1930039422(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3897413773_gshared (Enumerator_t1551735988 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1739478830((Enumerator_t1551735988 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2284023804 * L_0 = (KeyValuePair_2_t2284023804 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m737348899((KeyValuePair_2_t2284023804 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t2284023804 * L_4 = (KeyValuePair_2_t2284023804 *)__this->get_address_of_current_3();
		VirtualButtonData_t1223885651  L_5 = KeyValuePair_2_get_Value_m2492481859((KeyValuePair_2_t2284023804 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		VirtualButtonData_t1223885651  L_6 = L_5;
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_6);
		DictionaryEntry_t3048875398  L_8;
		memset(&L_8, 0, sizeof(L_8));
		DictionaryEntry__ctor_m2901884110(&L_8, (Il2CppObject *)L_3, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3897413773_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1551735988 * _thisAdjusted = reinterpret_cast<Enumerator_t1551735988 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3897413773(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3551896392_gshared (Enumerator_t1551735988 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentKey_m1550107223((Enumerator_t1551735988 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3551896392_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1551735988 * _thisAdjusted = reinterpret_cast<Enumerator_t1551735988 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3551896392(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2125488002_gshared (Enumerator_t1551735988 * __this, const MethodInfo* method)
{
	{
		VirtualButtonData_t1223885651  L_0 = Enumerator_get_CurrentValue_m2040133495((Enumerator_t1551735988 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		VirtualButtonData_t1223885651  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2125488002_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1551735988 * _thisAdjusted = reinterpret_cast<Enumerator_t1551735988 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2125488002(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2819456918_gshared (Enumerator_t1551735988 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m2655659678((Enumerator_t1551735988 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t231711286 * L_4 = (Dictionary_2_t231711286 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t231711286 * L_8 = (Dictionary_2_t231711286 *)__this->get_dictionary_0();
		NullCheck(L_8);
		Int32U5BU5D_t3030399641* L_9 = (Int32U5BU5D_t3030399641*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t231711286 * L_13 = (Dictionary_2_t231711286 *)__this->get_dictionary_0();
		NullCheck(L_13);
		VirtualButtonDataU5BU5D_t2198625794* L_14 = (VirtualButtonDataU5BU5D_t2198625794*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		VirtualButtonData_t1223885651  L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t2284023804  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m3037433849(&L_18, (int32_t)L_12, (VirtualButtonData_t1223885651 )L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t231711286 * L_20 = (Dictionary_2_t231711286 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2819456918_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1551735988 * _thisAdjusted = reinterpret_cast<Enumerator_t1551735988 *>(__this + 1);
	return Enumerator_MoveNext_m2819456918(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Current()
extern "C"  KeyValuePair_2_t2284023804  Enumerator_get_Current_m573307294_gshared (Enumerator_t1551735988 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2284023804  L_0 = (KeyValuePair_2_t2284023804 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t2284023804  Enumerator_get_Current_m573307294_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1551735988 * _thisAdjusted = reinterpret_cast<Enumerator_t1551735988 *>(__this + 1);
	return Enumerator_get_Current_m573307294(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m1550107223_gshared (Enumerator_t1551735988 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1739478830((Enumerator_t1551735988 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2284023804 * L_0 = (KeyValuePair_2_t2284023804 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m737348899((KeyValuePair_2_t2284023804 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentKey_m1550107223_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1551735988 * _thisAdjusted = reinterpret_cast<Enumerator_t1551735988 *>(__this + 1);
	return Enumerator_get_CurrentKey_m1550107223(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_CurrentValue()
extern "C"  VirtualButtonData_t1223885651  Enumerator_get_CurrentValue_m2040133495_gshared (Enumerator_t1551735988 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1739478830((Enumerator_t1551735988 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2284023804 * L_0 = (KeyValuePair_2_t2284023804 *)__this->get_address_of_current_3();
		VirtualButtonData_t1223885651  L_1 = KeyValuePair_2_get_Value_m2492481859((KeyValuePair_2_t2284023804 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  VirtualButtonData_t1223885651  Enumerator_get_CurrentValue_m2040133495_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1551735988 * _thisAdjusted = reinterpret_cast<Enumerator_t1551735988 *>(__this + 1);
	return Enumerator_get_CurrentValue_m2040133495(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Reset()
extern "C"  void Enumerator_Reset_m497205_gshared (Enumerator_t1551735988 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2655659678((Enumerator_t1551735988 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m497205_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1551735988 * _thisAdjusted = reinterpret_cast<Enumerator_t1551735988 *>(__this + 1);
	Enumerator_Reset_m497205(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m2655659678_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2655659678_gshared (Enumerator_t1551735988 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2655659678_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t231711286 * L_0 = (Dictionary_2_t231711286 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t231711286 * L_2 = (Dictionary_2_t231711286 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2655659678_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1551735988 * _thisAdjusted = reinterpret_cast<Enumerator_t1551735988 *>(__this + 1);
	Enumerator_VerifyState_m2655659678(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m1739478830_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m1739478830_gshared (Enumerator_t1551735988 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m1739478830_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m2655659678((Enumerator_t1551735988 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m1739478830_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1551735988 * _thisAdjusted = reinterpret_cast<Enumerator_t1551735988 *>(__this + 1);
	Enumerator_VerifyCurrent_m1739478830(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Dispose()
extern "C"  void Enumerator_Dispose_m2687880623_gshared (Enumerator_t1551735988 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t231711286 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2687880623_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1551735988 * _thisAdjusted = reinterpret_cast<Enumerator_t1551735988 *>(__this + 1);
	Enumerator_Dispose_m2687880623(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ArrayMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m351739840_gshared (Enumerator_t2920919292 * __this, Dictionary_2_t1600894590 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t1600894590 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t1600894590 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m351739840_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1600894590 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t2920919292 * _thisAdjusted = reinterpret_cast<Enumerator_t2920919292 *>(__this + 1);
	Enumerator__ctor_m351739840(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ArrayMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3385423807_gshared (Enumerator_t2920919292 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2207859971((Enumerator_t2920919292 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3653207108  L_0 = (KeyValuePair_2_t3653207108 )__this->get_current_3();
		KeyValuePair_2_t3653207108  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3385423807_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2920919292 * _thisAdjusted = reinterpret_cast<Enumerator_t2920919292 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3385423807(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ArrayMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2956573939_gshared (Enumerator_t2920919292 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m1870708158((Enumerator_t2920919292 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2956573939_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2920919292 * _thisAdjusted = reinterpret_cast<Enumerator_t2920919292 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2956573939(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ArrayMetadata>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4207684922_gshared (Enumerator_t2920919292 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2207859971((Enumerator_t2920919292 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3653207108 * L_0 = (KeyValuePair_2_t3653207108 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m3635972942((KeyValuePair_2_t3653207108 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t3653207108 * L_2 = (KeyValuePair_2_t3653207108 *)__this->get_address_of_current_3();
		ArrayMetadata_t2008834462  L_3 = KeyValuePair_2_get_Value_m2561735502((KeyValuePair_2_t3653207108 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ArrayMetadata_t2008834462  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4207684922_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2920919292 * _thisAdjusted = reinterpret_cast<Enumerator_t2920919292 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4207684922(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ArrayMetadata>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3391243017_gshared (Enumerator_t2920919292 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m2142897640((Enumerator_t2920919292 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3391243017_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2920919292 * _thisAdjusted = reinterpret_cast<Enumerator_t2920919292 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3391243017(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ArrayMetadata>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m196467817_gshared (Enumerator_t2920919292 * __this, const MethodInfo* method)
{
	{
		ArrayMetadata_t2008834462  L_0 = Enumerator_get_CurrentValue_m715422568((Enumerator_t2920919292 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		ArrayMetadata_t2008834462  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m196467817_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2920919292 * _thisAdjusted = reinterpret_cast<Enumerator_t2920919292 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m196467817(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ArrayMetadata>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3582197447_gshared (Enumerator_t2920919292 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m121954845((Enumerator_t2920919292 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1600894590 * L_4 = (Dictionary_2_t1600894590 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1600894590 * L_8 = (Dictionary_2_t1600894590 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t1600894590 * L_13 = (Dictionary_2_t1600894590 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ArrayMetadataU5BU5D_t1077509675* L_14 = (ArrayMetadataU5BU5D_t1077509675*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		ArrayMetadata_t2008834462  L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t3653207108  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m1237286508(&L_18, (Il2CppObject *)L_12, (ArrayMetadata_t2008834462 )L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t1600894590 * L_20 = (Dictionary_2_t1600894590 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3582197447_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2920919292 * _thisAdjusted = reinterpret_cast<Enumerator_t2920919292 *>(__this + 1);
	return Enumerator_MoveNext_m3582197447(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ArrayMetadata>::get_Current()
extern "C"  KeyValuePair_2_t3653207108  Enumerator_get_Current_m959113679_gshared (Enumerator_t2920919292 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3653207108  L_0 = (KeyValuePair_2_t3653207108 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t3653207108  Enumerator_get_Current_m959113679_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2920919292 * _thisAdjusted = reinterpret_cast<Enumerator_t2920919292 *>(__this + 1);
	return Enumerator_get_Current_m959113679(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ArrayMetadata>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m2142897640_gshared (Enumerator_t2920919292 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2207859971((Enumerator_t2920919292 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3653207108 * L_0 = (KeyValuePair_2_t3653207108 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m3635972942((KeyValuePair_2_t3653207108 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m2142897640_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2920919292 * _thisAdjusted = reinterpret_cast<Enumerator_t2920919292 *>(__this + 1);
	return Enumerator_get_CurrentKey_m2142897640(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ArrayMetadata>::get_CurrentValue()
extern "C"  ArrayMetadata_t2008834462  Enumerator_get_CurrentValue_m715422568_gshared (Enumerator_t2920919292 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2207859971((Enumerator_t2920919292 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3653207108 * L_0 = (KeyValuePair_2_t3653207108 *)__this->get_address_of_current_3();
		ArrayMetadata_t2008834462  L_1 = KeyValuePair_2_get_Value_m2561735502((KeyValuePair_2_t3653207108 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  ArrayMetadata_t2008834462  Enumerator_get_CurrentValue_m715422568_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2920919292 * _thisAdjusted = reinterpret_cast<Enumerator_t2920919292 *>(__this + 1);
	return Enumerator_get_CurrentValue_m715422568(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ArrayMetadata>::Reset()
extern "C"  void Enumerator_Reset_m1870708158_gshared (Enumerator_t2920919292 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m121954845((Enumerator_t2920919292 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m1870708158_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2920919292 * _thisAdjusted = reinterpret_cast<Enumerator_t2920919292 *>(__this + 1);
	Enumerator_Reset_m1870708158(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ArrayMetadata>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m121954845_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m121954845_gshared (Enumerator_t2920919292 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m121954845_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1600894590 * L_0 = (Dictionary_2_t1600894590 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t1600894590 * L_2 = (Dictionary_2_t1600894590 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m121954845_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2920919292 * _thisAdjusted = reinterpret_cast<Enumerator_t2920919292 *>(__this + 1);
	Enumerator_VerifyState_m121954845(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ArrayMetadata>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m2207859971_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m2207859971_gshared (Enumerator_t2920919292 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m2207859971_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m121954845((Enumerator_t2920919292 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m2207859971_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2920919292 * _thisAdjusted = reinterpret_cast<Enumerator_t2920919292 *>(__this + 1);
	Enumerator_VerifyCurrent_m2207859971(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ArrayMetadata>::Dispose()
extern "C"  void Enumerator_Dispose_m1811353652_gshared (Enumerator_t2920919292 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t1600894590 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1811353652_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2920919292 * _thisAdjusted = reinterpret_cast<Enumerator_t2920919292 *>(__this + 1);
	Enumerator_Dispose_m1811353652(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ObjectMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1033005566_gshared (Enumerator_t613039932 * __this, Dictionary_2_t3587982526 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t3587982526 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t3587982526 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1033005566_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3587982526 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t613039932 * _thisAdjusted = reinterpret_cast<Enumerator_t613039932 *>(__this + 1);
	Enumerator__ctor_m1033005566(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ObjectMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3957184719_gshared (Enumerator_t613039932 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2887132983((Enumerator_t613039932 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1345327748  L_0 = (KeyValuePair_2_t1345327748 )__this->get_current_3();
		KeyValuePair_2_t1345327748  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3957184719_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t613039932 * _thisAdjusted = reinterpret_cast<Enumerator_t613039932 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3957184719(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ObjectMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1051011311_gshared (Enumerator_t613039932 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m445711624((Enumerator_t613039932 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1051011311_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t613039932 * _thisAdjusted = reinterpret_cast<Enumerator_t613039932 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1051011311(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ObjectMetadata>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2197440366_gshared (Enumerator_t613039932 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2887132983((Enumerator_t613039932 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1345327748 * L_0 = (KeyValuePair_2_t1345327748 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m3628483074((KeyValuePair_2_t1345327748 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t1345327748 * L_2 = (KeyValuePair_2_t1345327748 *)__this->get_address_of_current_3();
		ObjectMetadata_t3995922398  L_3 = KeyValuePair_2_get_Value_m1238388442((KeyValuePair_2_t1345327748 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectMetadata_t3995922398  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2197440366_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t613039932 * _thisAdjusted = reinterpret_cast<Enumerator_t613039932 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2197440366(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ObjectMetadata>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2942645861_gshared (Enumerator_t613039932 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m1413334494((Enumerator_t613039932 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2942645861_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t613039932 * _thisAdjusted = reinterpret_cast<Enumerator_t613039932 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2942645861(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ObjectMetadata>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3441746565_gshared (Enumerator_t613039932 * __this, const MethodInfo* method)
{
	{
		ObjectMetadata_t3995922398  L_0 = Enumerator_get_CurrentValue_m1288075310((Enumerator_t613039932 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		ObjectMetadata_t3995922398  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3441746565_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t613039932 * _thisAdjusted = reinterpret_cast<Enumerator_t613039932 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3441746565(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ObjectMetadata>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2190712347_gshared (Enumerator_t613039932 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m3859484213((Enumerator_t613039932 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t3587982526 * L_4 = (Dictionary_2_t3587982526 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t3587982526 * L_8 = (Dictionary_2_t3587982526 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t3587982526 * L_13 = (Dictionary_2_t3587982526 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectMetadataU5BU5D_t3070734059* L_14 = (ObjectMetadataU5BU5D_t3070734059*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		ObjectMetadata_t3995922398  L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t1345327748  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m3056189548(&L_18, (Il2CppObject *)L_12, (ObjectMetadata_t3995922398 )L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t3587982526 * L_20 = (Dictionary_2_t3587982526 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2190712347_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t613039932 * _thisAdjusted = reinterpret_cast<Enumerator_t613039932 *>(__this + 1);
	return Enumerator_MoveNext_m2190712347(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ObjectMetadata>::get_Current()
extern "C"  KeyValuePair_2_t1345327748  Enumerator_get_Current_m2002173171_gshared (Enumerator_t613039932 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1345327748  L_0 = (KeyValuePair_2_t1345327748 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t1345327748  Enumerator_get_Current_m2002173171_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t613039932 * _thisAdjusted = reinterpret_cast<Enumerator_t613039932 *>(__this + 1);
	return Enumerator_get_Current_m2002173171(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ObjectMetadata>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1413334494_gshared (Enumerator_t613039932 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2887132983((Enumerator_t613039932 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1345327748 * L_0 = (KeyValuePair_2_t1345327748 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m3628483074((KeyValuePair_2_t1345327748 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1413334494_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t613039932 * _thisAdjusted = reinterpret_cast<Enumerator_t613039932 *>(__this + 1);
	return Enumerator_get_CurrentKey_m1413334494(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ObjectMetadata>::get_CurrentValue()
extern "C"  ObjectMetadata_t3995922398  Enumerator_get_CurrentValue_m1288075310_gshared (Enumerator_t613039932 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2887132983((Enumerator_t613039932 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1345327748 * L_0 = (KeyValuePair_2_t1345327748 *)__this->get_address_of_current_3();
		ObjectMetadata_t3995922398  L_1 = KeyValuePair_2_get_Value_m1238388442((KeyValuePair_2_t1345327748 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  ObjectMetadata_t3995922398  Enumerator_get_CurrentValue_m1288075310_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t613039932 * _thisAdjusted = reinterpret_cast<Enumerator_t613039932 *>(__this + 1);
	return Enumerator_get_CurrentValue_m1288075310(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ObjectMetadata>::Reset()
extern "C"  void Enumerator_Reset_m445711624_gshared (Enumerator_t613039932 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3859484213((Enumerator_t613039932 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m445711624_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t613039932 * _thisAdjusted = reinterpret_cast<Enumerator_t613039932 *>(__this + 1);
	Enumerator_Reset_m445711624(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ObjectMetadata>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m3859484213_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3859484213_gshared (Enumerator_t613039932 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3859484213_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t3587982526 * L_0 = (Dictionary_2_t3587982526 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t3587982526 * L_2 = (Dictionary_2_t3587982526 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3859484213_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t613039932 * _thisAdjusted = reinterpret_cast<Enumerator_t613039932 *>(__this + 1);
	Enumerator_VerifyState_m3859484213(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ObjectMetadata>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m2887132983_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m2887132983_gshared (Enumerator_t613039932 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m2887132983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m3859484213((Enumerator_t613039932 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m2887132983_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t613039932 * _thisAdjusted = reinterpret_cast<Enumerator_t613039932 *>(__this + 1);
	Enumerator_VerifyCurrent_m2887132983(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.ObjectMetadata>::Dispose()
extern "C"  void Enumerator_Dispose_m376347258_gshared (Enumerator_t613039932 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t3587982526 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m376347258_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t613039932 * _thisAdjusted = reinterpret_cast<Enumerator_t613039932 *>(__this + 1);
	Enumerator_Dispose_m376347258(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.PropertyMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m363835124_gshared (Enumerator_t310943670 * __this, Dictionary_2_t3285886264 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t3285886264 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t3285886264 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m363835124_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3285886264 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t310943670 * _thisAdjusted = reinterpret_cast<Enumerator_t310943670 *>(__this + 1);
	Enumerator__ctor_m363835124(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.PropertyMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2794069385_gshared (Enumerator_t310943670 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3003157341((Enumerator_t310943670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1043231486  L_0 = (KeyValuePair_2_t1043231486 )__this->get_current_3();
		KeyValuePair_2_t1043231486  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2794069385_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t310943670 * _thisAdjusted = reinterpret_cast<Enumerator_t310943670 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2794069385(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.PropertyMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1951022005_gshared (Enumerator_t310943670 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m2970174674((Enumerator_t310943670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1951022005_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t310943670 * _thisAdjusted = reinterpret_cast<Enumerator_t310943670 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1951022005(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.PropertyMetadata>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1377868864_gshared (Enumerator_t310943670 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3003157341((Enumerator_t310943670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1043231486 * L_0 = (KeyValuePair_2_t1043231486 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2516001448((KeyValuePair_2_t1043231486 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t1043231486 * L_2 = (KeyValuePair_2_t1043231486 *)__this->get_address_of_current_3();
		PropertyMetadata_t3693826136  L_3 = KeyValuePair_2_get_Value_m264611424((KeyValuePair_2_t1043231486 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		PropertyMetadata_t3693826136  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1377868864_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t310943670 * _thisAdjusted = reinterpret_cast<Enumerator_t310943670 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1377868864(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.PropertyMetadata>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3950246699_gshared (Enumerator_t310943670 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m4092394412((Enumerator_t310943670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3950246699_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t310943670 * _thisAdjusted = reinterpret_cast<Enumerator_t310943670 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3950246699(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.PropertyMetadata>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2746784795_gshared (Enumerator_t310943670 * __this, const MethodInfo* method)
{
	{
		PropertyMetadata_t3693826136  L_0 = Enumerator_get_CurrentValue_m1850788124((Enumerator_t310943670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		PropertyMetadata_t3693826136  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2746784795_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t310943670 * _thisAdjusted = reinterpret_cast<Enumerator_t310943670 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2746784795(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.PropertyMetadata>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2584530621_gshared (Enumerator_t310943670 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m4264987007((Enumerator_t310943670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t3285886264 * L_4 = (Dictionary_2_t3285886264 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t3285886264 * L_8 = (Dictionary_2_t3285886264 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t3285886264 * L_13 = (Dictionary_2_t3285886264 *)__this->get_dictionary_0();
		NullCheck(L_13);
		PropertyMetadataU5BU5D_t3621088073* L_14 = (PropertyMetadataU5BU5D_t3621088073*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		PropertyMetadata_t3693826136  L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t1043231486  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m3835749886(&L_18, (Il2CppObject *)L_12, (PropertyMetadata_t3693826136 )L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t3285886264 * L_20 = (Dictionary_2_t3285886264 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2584530621_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t310943670 * _thisAdjusted = reinterpret_cast<Enumerator_t310943670 *>(__this + 1);
	return Enumerator_MoveNext_m2584530621(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.PropertyMetadata>::get_Current()
extern "C"  KeyValuePair_2_t1043231486  Enumerator_get_Current_m3363997749_gshared (Enumerator_t310943670 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1043231486  L_0 = (KeyValuePair_2_t1043231486 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t1043231486  Enumerator_get_Current_m3363997749_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t310943670 * _thisAdjusted = reinterpret_cast<Enumerator_t310943670 *>(__this + 1);
	return Enumerator_get_Current_m3363997749(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.PropertyMetadata>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m4092394412_gshared (Enumerator_t310943670 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3003157341((Enumerator_t310943670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1043231486 * L_0 = (KeyValuePair_2_t1043231486 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2516001448((KeyValuePair_2_t1043231486 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m4092394412_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t310943670 * _thisAdjusted = reinterpret_cast<Enumerator_t310943670 *>(__this + 1);
	return Enumerator_get_CurrentKey_m4092394412(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.PropertyMetadata>::get_CurrentValue()
extern "C"  PropertyMetadata_t3693826136  Enumerator_get_CurrentValue_m1850788124_gshared (Enumerator_t310943670 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3003157341((Enumerator_t310943670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1043231486 * L_0 = (KeyValuePair_2_t1043231486 *)__this->get_address_of_current_3();
		PropertyMetadata_t3693826136  L_1 = KeyValuePair_2_get_Value_m264611424((KeyValuePair_2_t1043231486 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  PropertyMetadata_t3693826136  Enumerator_get_CurrentValue_m1850788124_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t310943670 * _thisAdjusted = reinterpret_cast<Enumerator_t310943670 *>(__this + 1);
	return Enumerator_get_CurrentValue_m1850788124(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.PropertyMetadata>::Reset()
extern "C"  void Enumerator_Reset_m2970174674_gshared (Enumerator_t310943670 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m4264987007((Enumerator_t310943670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m2970174674_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t310943670 * _thisAdjusted = reinterpret_cast<Enumerator_t310943670 *>(__this + 1);
	Enumerator_Reset_m2970174674(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.PropertyMetadata>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m4264987007_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m4264987007_gshared (Enumerator_t310943670 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m4264987007_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t3285886264 * L_0 = (Dictionary_2_t3285886264 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t3285886264 * L_2 = (Dictionary_2_t3285886264 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m4264987007_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t310943670 * _thisAdjusted = reinterpret_cast<Enumerator_t310943670 *>(__this + 1);
	Enumerator_VerifyState_m4264987007(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.PropertyMetadata>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m3003157341_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3003157341_gshared (Enumerator_t310943670 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3003157341_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m4264987007((Enumerator_t310943670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m3003157341_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t310943670 * _thisAdjusted = reinterpret_cast<Enumerator_t310943670 *>(__this + 1);
	Enumerator_VerifyCurrent_m3003157341(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,LitJson.PropertyMetadata>::Dispose()
extern "C"  void Enumerator_Dispose_m859353700_gshared (Enumerator_t310943670 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t3285886264 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m859353700_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t310943670 * _thisAdjusted = reinterpret_cast<Enumerator_t310943670 *>(__this + 1);
	Enumerator_Dispose_m859353700(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m811214818_gshared (Enumerator_t1760119595 * __this, Dictionary_2_t440094893 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t440094893 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t440094893 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m811214818_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t440094893 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t1760119595 * _thisAdjusted = reinterpret_cast<Enumerator_t1760119595 *>(__this + 1);
	Enumerator__ctor_m811214818(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4203850179_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m207879567((Enumerator_t1760119595 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2492407411  L_0 = (KeyValuePair_2_t2492407411 )__this->get_current_3();
		KeyValuePair_2_t2492407411  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4203850179_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1760119595 * _thisAdjusted = reinterpret_cast<Enumerator_t1760119595 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m4203850179(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m24795391_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m2958379748((Enumerator_t1760119595 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m24795391_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1760119595 * _thisAdjusted = reinterpret_cast<Enumerator_t1760119595 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m24795391(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2251395880_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m207879567((Enumerator_t1760119595 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2492407411 * L_0 = (KeyValuePair_2_t2492407411 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m3094063940((KeyValuePair_2_t2492407411 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t2492407411 * L_2 = (KeyValuePair_2_t2492407411 *)__this->get_address_of_current_3();
		IndexInfo_t848034765  L_3 = KeyValuePair_2_get_Value_m1745261316((KeyValuePair_2_t2492407411 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		IndexInfo_t848034765  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2251395880_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1760119595 * _thisAdjusted = reinterpret_cast<Enumerator_t1760119595 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2251395880(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4171739561_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m2900523578((Enumerator_t1760119595 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4171739561_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1760119595 * _thisAdjusted = reinterpret_cast<Enumerator_t1760119595 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4171739561(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2176216665_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method)
{
	{
		IndexInfo_t848034765  L_0 = Enumerator_get_CurrentValue_m2282140538((Enumerator_t1760119595 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		IndexInfo_t848034765  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2176216665_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1760119595 * _thisAdjusted = reinterpret_cast<Enumerator_t1760119595 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2176216665(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3944863691_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m2402519621((Enumerator_t1760119595 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t440094893 * L_4 = (Dictionary_2_t440094893 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t440094893 * L_8 = (Dictionary_2_t440094893 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t440094893 * L_13 = (Dictionary_2_t440094893 *)__this->get_dictionary_0();
		NullCheck(L_13);
		IndexInfoU5BU5D_t625615456* L_14 = (IndexInfoU5BU5D_t625615456*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		IndexInfo_t848034765  L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t2492407411  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m950010662(&L_18, (Il2CppObject *)L_12, (IndexInfo_t848034765 )L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t440094893 * L_20 = (Dictionary_2_t440094893 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3944863691_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1760119595 * _thisAdjusted = reinterpret_cast<Enumerator_t1760119595 *>(__this + 1);
	return Enumerator_MoveNext_m3944863691(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Current()
extern "C"  KeyValuePair_2_t2492407411  Enumerator_get_Current_m505501491_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2492407411  L_0 = (KeyValuePair_2_t2492407411 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t2492407411  Enumerator_get_Current_m505501491_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1760119595 * _thisAdjusted = reinterpret_cast<Enumerator_t1760119595 *>(__this + 1);
	return Enumerator_get_Current_m505501491(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m2900523578_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m207879567((Enumerator_t1760119595 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2492407411 * L_0 = (KeyValuePair_2_t2492407411 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m3094063940((KeyValuePair_2_t2492407411 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m2900523578_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1760119595 * _thisAdjusted = reinterpret_cast<Enumerator_t1760119595 *>(__this + 1);
	return Enumerator_get_CurrentKey_m2900523578(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_CurrentValue()
extern "C"  IndexInfo_t848034765  Enumerator_get_CurrentValue_m2282140538_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m207879567((Enumerator_t1760119595 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2492407411 * L_0 = (KeyValuePair_2_t2492407411 *)__this->get_address_of_current_3();
		IndexInfo_t848034765  L_1 = KeyValuePair_2_get_Value_m1745261316((KeyValuePair_2_t2492407411 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  IndexInfo_t848034765  Enumerator_get_CurrentValue_m2282140538_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1760119595 * _thisAdjusted = reinterpret_cast<Enumerator_t1760119595 *>(__this + 1);
	return Enumerator_get_CurrentValue_m2282140538(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::Reset()
extern "C"  void Enumerator_Reset_m2958379748_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2402519621((Enumerator_t1760119595 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m2958379748_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1760119595 * _thisAdjusted = reinterpret_cast<Enumerator_t1760119595 *>(__this + 1);
	Enumerator_Reset_m2958379748(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m2402519621_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2402519621_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2402519621_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t440094893 * L_0 = (Dictionary_2_t440094893 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t440094893 * L_2 = (Dictionary_2_t440094893 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2402519621_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1760119595 * _thisAdjusted = reinterpret_cast<Enumerator_t1760119595 *>(__this + 1);
	Enumerator_VerifyState_m2402519621(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m207879567_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m207879567_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m207879567_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m2402519621((Enumerator_t1760119595 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m207879567_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1760119595 * _thisAdjusted = reinterpret_cast<Enumerator_t1760119595 *>(__this + 1);
	Enumerator_VerifyCurrent_m207879567(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m1427874974_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t440094893 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1427874974_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1760119595 * _thisAdjusted = reinterpret_cast<Enumerator_t1760119595 *>(__this + 1);
	Enumerator_Dispose_m1427874974(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m661036428_gshared (Enumerator_t442692252 * __this, Dictionary_2_t3417634846 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t3417634846 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m661036428_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3417634846 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator__ctor_m661036428(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1692692619_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3330382363((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1174980068  L_0 = (KeyValuePair_2_t1174980068 )__this->get_current_3();
		KeyValuePair_2_t1174980068  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1692692619_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1692692619(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m70453843_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m3115320746((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m70453843_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m70453843(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3667889028_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3330382363((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1174980068 * L_0 = (KeyValuePair_2_t1174980068 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t1174980068 * L_2 = (KeyValuePair_2_t1174980068 *)__this->get_address_of_current_3();
		bool L_3 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3667889028_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3667889028(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1214978221_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m565000604((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1214978221_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1214978221(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m313528997_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Enumerator_get_CurrentValue_m4143929484((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m313528997_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m313528997(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1856697671_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m1165543189((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t3417634846 * L_4 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t3417634846 * L_8 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t3417634846 * L_13 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck(L_13);
		BooleanU5BU5D_t3568034315* L_14 = (BooleanU5BU5D_t3568034315*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		bool L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t1174980068  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m4040336782(&L_18, (Il2CppObject *)L_12, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t3417634846 * L_20 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1856697671_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_MoveNext_m1856697671(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C"  KeyValuePair_2_t1174980068  Enumerator_get_Current_m1020413567_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1174980068  L_0 = (KeyValuePair_2_t1174980068 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t1174980068  Enumerator_get_Current_m1020413567_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_get_Current_m1020413567(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m565000604_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3330382363((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1174980068 * L_0 = (KeyValuePair_2_t1174980068 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m565000604_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_get_CurrentKey_m565000604(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentValue()
extern "C"  bool Enumerator_get_CurrentValue_m4143929484_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3330382363((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1174980068 * L_0 = (KeyValuePair_2_t1174980068 *)__this->get_address_of_current_3();
		bool L_1 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_get_CurrentValue_m4143929484_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_get_CurrentValue_m4143929484(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Reset()
extern "C"  void Enumerator_Reset_m3115320746_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1165543189((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m3115320746_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator_Reset_m3115320746(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m1165543189_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1165543189_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1165543189_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t3417634846 * L_0 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t3417634846 * L_2 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1165543189_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator_VerifyState_m1165543189(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m3330382363_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3330382363_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3330382363_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m1165543189((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m3330382363_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator_VerifyCurrent_m3330382363(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C"  void Enumerator_Dispose_m2711120408_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t3417634846 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2711120408_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator_Dispose_m2711120408(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2153525756_gshared (Enumerator_t950939475 * __this, Dictionary_2_t3925882069 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t3925882069 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t3925882069 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2153525756_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3925882069 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	Enumerator__ctor_m2153525756(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m469144049_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4104698621((Enumerator_t950939475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1683227291  L_0 = (KeyValuePair_2_t1683227291 )__this->get_current_3();
		KeyValuePair_2_t1683227291  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m469144049_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m469144049(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2381541637_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m535811698((Enumerator_t950939475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2381541637_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2381541637(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2303546672_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4104698621((Enumerator_t950939475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1683227291 * L_0 = (KeyValuePair_2_t1683227291 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m4247498472((KeyValuePair_2_t1683227291 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t1683227291 * L_2 = (KeyValuePair_2_t1683227291 *)__this->get_address_of_current_3();
		KeyValuePair_2_t38854645  L_3 = KeyValuePair_2_get_Value_m667219360((KeyValuePair_2_t1683227291 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		KeyValuePair_2_t38854645  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2303546672_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2303546672(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1158804235_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m1920795876((Enumerator_t950939475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1158804235_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1158804235(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1378836675_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = Enumerator_get_CurrentValue_m960757924((Enumerator_t950939475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		KeyValuePair_2_t38854645  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1378836675_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1378836675(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1022654781_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m2572643079((Enumerator_t950939475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t3925882069 * L_4 = (Dictionary_2_t3925882069 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t3925882069 * L_8 = (Dictionary_2_t3925882069 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t3925882069 * L_13 = (Dictionary_2_t3925882069 *)__this->get_dictionary_0();
		NullCheck(L_13);
		KeyValuePair_2U5BU5D_t2854920344* L_14 = (KeyValuePair_2U5BU5D_t2854920344*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		KeyValuePair_2_t38854645  L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t1683227291  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m400583398(&L_18, (Il2CppObject *)L_12, (KeyValuePair_2_t38854645 )L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t3925882069 * L_20 = (Dictionary_2_t3925882069 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1022654781_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	return Enumerator_MoveNext_m1022654781(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t1683227291  Enumerator_get_Current_m304262005_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1683227291  L_0 = (KeyValuePair_2_t1683227291 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t1683227291  Enumerator_get_Current_m304262005_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	return Enumerator_get_Current_m304262005(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1920795876_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4104698621((Enumerator_t950939475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1683227291 * L_0 = (KeyValuePair_2_t1683227291 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m4247498472((KeyValuePair_2_t1683227291 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1920795876_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	return Enumerator_get_CurrentKey_m1920795876(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_CurrentValue()
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_CurrentValue_m960757924_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4104698621((Enumerator_t950939475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1683227291 * L_0 = (KeyValuePair_2_t1683227291 *)__this->get_address_of_current_3();
		KeyValuePair_2_t38854645  L_1 = KeyValuePair_2_get_Value_m667219360((KeyValuePair_2_t1683227291 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_CurrentValue_m960757924_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	return Enumerator_get_CurrentValue_m960757924(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Reset()
extern "C"  void Enumerator_Reset_m535811698_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2572643079((Enumerator_t950939475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m535811698_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	Enumerator_Reset_m535811698(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m2572643079_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2572643079_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2572643079_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t3925882069 * L_0 = (Dictionary_2_t3925882069 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t3925882069 * L_2 = (Dictionary_2_t3925882069 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2572643079_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	Enumerator_VerifyState_m2572643079(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m4104698621_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m4104698621_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m4104698621_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m2572643079((Enumerator_t950939475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m4104698621_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	Enumerator_VerifyCurrent_m4104698621(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void Enumerator_Dispose_m3456852724_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t3925882069 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3456852724_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	Enumerator_Dispose_m3456852724(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3597047336_gshared (Enumerator_t2983962278 * __this, Dictionary_2_t1663937576 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t1663937576 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3597047336_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1663937576 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator__ctor_m3597047336(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2010873149_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3278789713((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3716250094  L_0 = (KeyValuePair_2_t3716250094 )__this->get_current_3();
		KeyValuePair_2_t3716250094  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2010873149_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2010873149(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3085583937_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m1132695838((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3085583937_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3085583937(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m487599172_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3278789713((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3716250094 * L_0 = (KeyValuePair_2_t3716250094 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t3716250094 * L_2 = (KeyValuePair_2_t3716250094 *)__this->get_address_of_current_3();
		int32_t L_3 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m487599172_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m487599172(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m677423231_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m1408186928((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m677423231_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m677423231(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3005608231_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentValue_m2645962456((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3005608231_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3005608231(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m435964161_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m3173176371((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1663937576 * L_4 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1663937576 * L_8 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t1663937576 * L_13 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck(L_13);
		Int32U5BU5D_t3030399641* L_14 = (Int32U5BU5D_t3030399641*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t3716250094  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m1877755778(&L_18, (Il2CppObject *)L_12, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t1663937576 * L_20 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m435964161_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_MoveNext_m435964161(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t3716250094  Enumerator_get_Current_m1932198897_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3716250094  L_0 = (KeyValuePair_2_t3716250094 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t3716250094  Enumerator_get_Current_m1932198897_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_get_Current_m1932198897(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1408186928_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3278789713((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3716250094 * L_0 = (KeyValuePair_2_t3716250094 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1408186928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_get_CurrentKey_m1408186928(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m2645962456_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3278789713((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3716250094 * L_0 = (KeyValuePair_2_t3716250094 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentValue_m2645962456_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_get_CurrentValue_m2645962456(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Reset()
extern "C"  void Enumerator_Reset_m1132695838_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3173176371((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m1132695838_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator_Reset_m1132695838(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m3173176371_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3173176371_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3173176371_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t1663937576 * L_2 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3173176371_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator_VerifyState_m3173176371(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m3278789713_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3278789713_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3278789713_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m3173176371((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m3278789713_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator_VerifyCurrent_m3278789713(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m401572848_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t1663937576 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m401572848_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator_Dispose_m401572848(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3742107451_gshared (Enumerator_t3601534125 * __this, Dictionary_2_t2281509423 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t2281509423 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3742107451_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2281509423 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator__ctor_m3742107451(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1702320752((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645  L_0 = (KeyValuePair_2_t38854645 )__this->get_current_3();
		KeyValuePair_2_t38854645  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m229223308_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m229223308(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m3129803197((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3225937576_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3225937576(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1702320752((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645 * L_0 = (KeyValuePair_2_t38854645 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t38854645 * L_2 = (KeyValuePair_2_t38854645 *)__this->get_address_of_current_3();
		Il2CppObject * L_3 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_4;
		memset(&L_4, 0, sizeof(L_4));
		DictionaryEntry__ctor_m2901884110(&L_4, (Il2CppObject *)L_1, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m3839846791((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentValue_m402763047((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3349738440_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m262343092((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t2281509423 * L_4 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t2281509423 * L_8 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t2281509423 * L_13 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t38854645  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m1640124561(&L_18, (Il2CppObject *)L_12, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t2281509423 * L_20 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3349738440_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_MoveNext_m3349738440(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_Current_m25299632_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = (KeyValuePair_2_t38854645 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_Current_m25299632_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_get_Current_m25299632(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m3839846791_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1702320752((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645 * L_0 = (KeyValuePair_2_t38854645 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m3839846791_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_get_CurrentKey_m3839846791(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m402763047_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1702320752((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645 * L_0 = (KeyValuePair_2_t38854645 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m402763047_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_get_CurrentValue_m402763047(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m3129803197_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m262343092((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m3129803197_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator_Reset_m3129803197(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m262343092_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m262343092_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m262343092_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t2281509423 * L_2 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m262343092_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator_VerifyState_m262343092(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m1702320752_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m1702320752_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m1702320752_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m262343092((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m1702320752_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator_VerifyCurrent_m1702320752(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1905011127_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t2281509423 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1905011127_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator_Dispose_m1905011127(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3918347599_gshared (Enumerator_t1898967441 * __this, Dictionary_2_t578942739 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t578942739 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t578942739 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3918347599_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t578942739 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t1898967441 * _thisAdjusted = reinterpret_cast<Enumerator_t1898967441 *>(__this + 1);
	Enumerator__ctor_m3918347599(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2431579728_gshared (Enumerator_t1898967441 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3478717716((Enumerator_t1898967441 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2631255257  L_0 = (KeyValuePair_2_t2631255257 )__this->get_current_3();
		KeyValuePair_2_t2631255257  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2431579728_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1898967441 * _thisAdjusted = reinterpret_cast<Enumerator_t1898967441 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2431579728(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2088818500_gshared (Enumerator_t1898967441 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m1141574313((Enumerator_t1898967441 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2088818500_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1898967441 * _thisAdjusted = reinterpret_cast<Enumerator_t1898967441 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2088818500(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m928208745_gshared (Enumerator_t1898967441 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3478717716((Enumerator_t1898967441 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2631255257 * L_0 = (KeyValuePair_2_t2631255257 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2800370719((KeyValuePair_2_t2631255257 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t2631255257 * L_2 = (KeyValuePair_2_t2631255257 *)__this->get_address_of_current_3();
		uint16_t L_3 = KeyValuePair_2_get_Value_m3658972159((KeyValuePair_2_t2631255257 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		uint16_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m928208745_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1898967441 * _thisAdjusted = reinterpret_cast<Enumerator_t1898967441 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m928208745(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3920137966_gshared (Enumerator_t1898967441 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m480552147((Enumerator_t1898967441 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3920137966_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1898967441 * _thisAdjusted = reinterpret_cast<Enumerator_t1898967441 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3920137966(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2904498604_gshared (Enumerator_t1898967441 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = Enumerator_get_CurrentValue_m3339719667((Enumerator_t1898967441 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		uint16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2904498604_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1898967441 * _thisAdjusted = reinterpret_cast<Enumerator_t1898967441 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2904498604(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4229610700_gshared (Enumerator_t1898967441 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m3153917128((Enumerator_t1898967441 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t578942739 * L_4 = (Dictionary_2_t578942739 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t578942739 * L_8 = (Dictionary_2_t578942739 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t578942739 * L_13 = (Dictionary_2_t578942739 *)__this->get_dictionary_0();
		NullCheck(L_13);
		UInt16U5BU5D_t2527266722* L_14 = (UInt16U5BU5D_t2527266722*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		uint16_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t2631255257  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m2351688349(&L_18, (Il2CppObject *)L_12, (uint16_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t578942739 * L_20 = (Dictionary_2_t578942739 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m4229610700_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1898967441 * _thisAdjusted = reinterpret_cast<Enumerator_t1898967441 *>(__this + 1);
	return Enumerator_MoveNext_m4229610700(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::get_Current()
extern "C"  KeyValuePair_2_t2631255257  Enumerator_get_Current_m3660240340_gshared (Enumerator_t1898967441 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2631255257  L_0 = (KeyValuePair_2_t2631255257 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t2631255257  Enumerator_get_Current_m3660240340_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1898967441 * _thisAdjusted = reinterpret_cast<Enumerator_t1898967441 *>(__this + 1);
	return Enumerator_get_Current_m3660240340(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m480552147_gshared (Enumerator_t1898967441 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3478717716((Enumerator_t1898967441 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2631255257 * L_0 = (KeyValuePair_2_t2631255257 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2800370719((KeyValuePair_2_t2631255257 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m480552147_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1898967441 * _thisAdjusted = reinterpret_cast<Enumerator_t1898967441 *>(__this + 1);
	return Enumerator_get_CurrentKey_m480552147(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::get_CurrentValue()
extern "C"  uint16_t Enumerator_get_CurrentValue_m3339719667_gshared (Enumerator_t1898967441 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3478717716((Enumerator_t1898967441 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2631255257 * L_0 = (KeyValuePair_2_t2631255257 *)__this->get_address_of_current_3();
		uint16_t L_1 = KeyValuePair_2_get_Value_m3658972159((KeyValuePair_2_t2631255257 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  uint16_t Enumerator_get_CurrentValue_m3339719667_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1898967441 * _thisAdjusted = reinterpret_cast<Enumerator_t1898967441 *>(__this + 1);
	return Enumerator_get_CurrentValue_m3339719667(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::Reset()
extern "C"  void Enumerator_Reset_m1141574313_gshared (Enumerator_t1898967441 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3153917128((Enumerator_t1898967441 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m1141574313_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1898967441 * _thisAdjusted = reinterpret_cast<Enumerator_t1898967441 *>(__this + 1);
	Enumerator_Reset_m1141574313(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m3153917128_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3153917128_gshared (Enumerator_t1898967441 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3153917128_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t578942739 * L_0 = (Dictionary_2_t578942739 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t578942739 * L_2 = (Dictionary_2_t578942739 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3153917128_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1898967441 * _thisAdjusted = reinterpret_cast<Enumerator_t1898967441 *>(__this + 1);
	Enumerator_VerifyState_m3153917128(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m3478717716_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3478717716_gshared (Enumerator_t1898967441 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3478717716_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m3153917128((Enumerator_t1898967441 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m3478717716_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1898967441 * _thisAdjusted = reinterpret_cast<Enumerator_t1898967441 *>(__this + 1);
	Enumerator_VerifyCurrent_m3478717716(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::Dispose()
extern "C"  void Enumerator_Dispose_m2601453635_gshared (Enumerator_t1898967441 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t578942739 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2601453635_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1898967441 * _thisAdjusted = reinterpret_cast<Enumerator_t1898967441 *>(__this + 1);
	Enumerator_Dispose_m2601453635(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1940740351_gshared (Enumerator_t2636751318 * __this, Dictionary_2_t1316726616 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t1316726616 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t1316726616 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1940740351_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1316726616 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t2636751318 * _thisAdjusted = reinterpret_cast<Enumerator_t2636751318 *>(__this + 1);
	Enumerator__ctor_m1940740351(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4131083214_gshared (Enumerator_t2636751318 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m652120618((Enumerator_t2636751318 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3369039134  L_0 = (KeyValuePair_2_t3369039134 )__this->get_current_3();
		KeyValuePair_2_t3369039134  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4131083214_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2636751318 * _thisAdjusted = reinterpret_cast<Enumerator_t2636751318 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m4131083214(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m161770386_gshared (Enumerator_t2636751318 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m4069406173((Enumerator_t2636751318 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m161770386_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2636751318 * _thisAdjusted = reinterpret_cast<Enumerator_t2636751318 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m161770386(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2850596997_gshared (Enumerator_t2636751318 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m652120618((Enumerator_t2636751318 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3369039134 * L_0 = (KeyValuePair_2_t3369039134 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m1277606655((KeyValuePair_2_t3369039134 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t3369039134 * L_2 = (KeyValuePair_2_t3369039134 *)__this->get_address_of_current_3();
		ProfileData_t1724666488  L_3 = KeyValuePair_2_get_Value_m699551839((KeyValuePair_2_t3369039134 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ProfileData_t1724666488  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2850596997_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2636751318 * _thisAdjusted = reinterpret_cast<Enumerator_t2636751318 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2850596997(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m541211856_gshared (Enumerator_t2636751318 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m3527536851((Enumerator_t2636751318 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m541211856_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2636751318 * _thisAdjusted = reinterpret_cast<Enumerator_t2636751318 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m541211856(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m123814906_gshared (Enumerator_t2636751318 * __this, const MethodInfo* method)
{
	{
		ProfileData_t1724666488  L_0 = Enumerator_get_CurrentValue_m2118029427((Enumerator_t2636751318 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		ProfileData_t1724666488  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m123814906_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2636751318 * _thisAdjusted = reinterpret_cast<Enumerator_t2636751318 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m123814906(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2696697426_gshared (Enumerator_t2636751318 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m2730167878((Enumerator_t2636751318 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1316726616 * L_4 = (Dictionary_2_t1316726616 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1316726616 * L_8 = (Dictionary_2_t1316726616 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t1316726616 * L_13 = (Dictionary_2_t1316726616 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ProfileDataU5BU5D_t3861544105* L_14 = (ProfileDataU5BU5D_t3861544105*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		ProfileData_t1724666488  L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t3369039134  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m833149801(&L_18, (Il2CppObject *)L_12, (ProfileData_t1724666488 )L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t1316726616 * L_20 = (Dictionary_2_t1316726616 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2696697426_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2636751318 * _thisAdjusted = reinterpret_cast<Enumerator_t2636751318 *>(__this + 1);
	return Enumerator_MoveNext_m2696697426(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C"  KeyValuePair_2_t3369039134  Enumerator_get_Current_m3061672298_gshared (Enumerator_t2636751318 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3369039134  L_0 = (KeyValuePair_2_t3369039134 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t3369039134  Enumerator_get_Current_m3061672298_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2636751318 * _thisAdjusted = reinterpret_cast<Enumerator_t2636751318 *>(__this + 1);
	return Enumerator_get_Current_m3061672298(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m3527536851_gshared (Enumerator_t2636751318 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m652120618((Enumerator_t2636751318 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3369039134 * L_0 = (KeyValuePair_2_t3369039134 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m1277606655((KeyValuePair_2_t3369039134 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m3527536851_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2636751318 * _thisAdjusted = reinterpret_cast<Enumerator_t2636751318 *>(__this + 1);
	return Enumerator_get_CurrentKey_m3527536851(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_CurrentValue()
extern "C"  ProfileData_t1724666488  Enumerator_get_CurrentValue_m2118029427_gshared (Enumerator_t2636751318 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m652120618((Enumerator_t2636751318 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3369039134 * L_0 = (KeyValuePair_2_t3369039134 *)__this->get_address_of_current_3();
		ProfileData_t1724666488  L_1 = KeyValuePair_2_get_Value_m699551839((KeyValuePair_2_t3369039134 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  ProfileData_t1724666488  Enumerator_get_CurrentValue_m2118029427_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2636751318 * _thisAdjusted = reinterpret_cast<Enumerator_t2636751318 *>(__this + 1);
	return Enumerator_get_CurrentValue_m2118029427(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Reset()
extern "C"  void Enumerator_Reset_m4069406173_gshared (Enumerator_t2636751318 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2730167878((Enumerator_t2636751318 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m4069406173_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2636751318 * _thisAdjusted = reinterpret_cast<Enumerator_t2636751318 *>(__this + 1);
	Enumerator_Reset_m4069406173(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m2730167878_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2730167878_gshared (Enumerator_t2636751318 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2730167878_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1316726616 * L_0 = (Dictionary_2_t1316726616 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t1316726616 * L_2 = (Dictionary_2_t1316726616 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2730167878_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2636751318 * _thisAdjusted = reinterpret_cast<Enumerator_t2636751318 *>(__this + 1);
	Enumerator_VerifyState_m2730167878(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m652120618_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m652120618_gshared (Enumerator_t2636751318 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m652120618_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m2730167878((Enumerator_t2636751318 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m652120618_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2636751318 * _thisAdjusted = reinterpret_cast<Enumerator_t2636751318 *>(__this + 1);
	Enumerator_VerifyCurrent_m652120618(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Dispose()
extern "C"  void Enumerator_Dispose_m346153571_gshared (Enumerator_t2636751318 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t1316726616 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m346153571_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2636751318 * _thisAdjusted = reinterpret_cast<Enumerator_t2636751318 *>(__this + 1);
	Enumerator_Dispose_m346153571(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3882032771_gshared (Enumerator_t1408760236 * __this, Dictionary_2_t88735534 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t88735534 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t88735534 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3882032771_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t88735534 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t1408760236 * _thisAdjusted = reinterpret_cast<Enumerator_t1408760236 *>(__this + 1);
	Enumerator__ctor_m3882032771(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2921220050_gshared (Enumerator_t1408760236 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2698504284((Enumerator_t1408760236 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2141048052  L_0 = (KeyValuePair_2_t2141048052 )__this->get_current_3();
		KeyValuePair_2_t2141048052  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2921220050_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1408760236 * _thisAdjusted = reinterpret_cast<Enumerator_t1408760236 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2921220050(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m585263276_gshared (Enumerator_t1408760236 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m3214492505((Enumerator_t1408760236 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m585263276_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1408760236 * _thisAdjusted = reinterpret_cast<Enumerator_t1408760236 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m585263276(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3264525985_gshared (Enumerator_t1408760236 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2698504284((Enumerator_t1408760236 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2141048052 * L_0 = (KeyValuePair_2_t2141048052 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m1279069287((KeyValuePair_2_t2141048052 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t2141048052 * L_4 = (KeyValuePair_2_t2141048052 *)__this->get_address_of_current_3();
		Il2CppObject * L_5 = KeyValuePair_2_get_Value_m4144029831((KeyValuePair_2_t2141048052 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3264525985_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1408760236 * _thisAdjusted = reinterpret_cast<Enumerator_t1408760236 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3264525985(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m787042308_gshared (Enumerator_t1408760236 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentKey_m1284131751((Enumerator_t1408760236 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m787042308_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1408760236 * _thisAdjusted = reinterpret_cast<Enumerator_t1408760236 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m787042308(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2885850078_gshared (Enumerator_t1408760236 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentValue_m1347854879((Enumerator_t1408760236 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2885850078_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1408760236 * _thisAdjusted = reinterpret_cast<Enumerator_t1408760236 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2885850078(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1380568772_gshared (Enumerator_t1408760236 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m1419837100((Enumerator_t1408760236 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t88735534 * L_4 = (Dictionary_2_t88735534 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t88735534 * L_8 = (Dictionary_2_t88735534 *)__this->get_dictionary_0();
		NullCheck(L_8);
		PIXEL_FORMATU5BU5D_t1421860245* L_9 = (PIXEL_FORMATU5BU5D_t1421860245*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t88735534 * L_13 = (Dictionary_2_t88735534 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t2141048052  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m1737106953(&L_18, (int32_t)L_12, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t88735534 * L_20 = (Dictionary_2_t88735534 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1380568772_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1408760236 * _thisAdjusted = reinterpret_cast<Enumerator_t1408760236 *>(__this + 1);
	return Enumerator_MoveNext_m1380568772(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t2141048052  Enumerator_get_Current_m144006020_gshared (Enumerator_t1408760236 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2141048052  L_0 = (KeyValuePair_2_t2141048052 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t2141048052  Enumerator_get_Current_m144006020_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1408760236 * _thisAdjusted = reinterpret_cast<Enumerator_t1408760236 *>(__this + 1);
	return Enumerator_get_Current_m144006020(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m1284131751_gshared (Enumerator_t1408760236 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2698504284((Enumerator_t1408760236 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2141048052 * L_0 = (KeyValuePair_2_t2141048052 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m1279069287((KeyValuePair_2_t2141048052 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentKey_m1284131751_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1408760236 * _thisAdjusted = reinterpret_cast<Enumerator_t1408760236 *>(__this + 1);
	return Enumerator_get_CurrentKey_m1284131751(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1347854879_gshared (Enumerator_t1408760236 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2698504284((Enumerator_t1408760236 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2141048052 * L_0 = (KeyValuePair_2_t2141048052 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m4144029831((KeyValuePair_2_t2141048052 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1347854879_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1408760236 * _thisAdjusted = reinterpret_cast<Enumerator_t1408760236 *>(__this + 1);
	return Enumerator_get_CurrentValue_m1347854879(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m3214492505_gshared (Enumerator_t1408760236 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1419837100((Enumerator_t1408760236 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m3214492505_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1408760236 * _thisAdjusted = reinterpret_cast<Enumerator_t1408760236 *>(__this + 1);
	Enumerator_Reset_m3214492505(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m1419837100_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1419837100_gshared (Enumerator_t1408760236 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1419837100_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t88735534 * L_0 = (Dictionary_2_t88735534 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t88735534 * L_2 = (Dictionary_2_t88735534 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1419837100_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1408760236 * _thisAdjusted = reinterpret_cast<Enumerator_t1408760236 *>(__this + 1);
	Enumerator_VerifyState_m1419837100(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m2698504284_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m2698504284_gshared (Enumerator_t1408760236 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m2698504284_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m1419837100((Enumerator_t1408760236 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m2698504284_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1408760236 * _thisAdjusted = reinterpret_cast<Enumerator_t1408760236 *>(__this + 1);
	Enumerator_VerifyCurrent_m2698504284(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3604910775_gshared (Enumerator_t1408760236 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t88735534 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3604910775_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1408760236 * _thisAdjusted = reinterpret_cast<Enumerator_t1408760236 *>(__this + 1);
	Enumerator_Dispose_m3604910775(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2975000652_gshared (Enumerator_t91811072 * __this, Dictionary_2_t1697274930 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		Enumerator_t3017299632  L_1 = ((  Enumerator_t3017299632  (*) (Dictionary_2_t1697274930 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1697274930 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2975000652_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1697274930 * ___host0, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	Enumerator__ctor_m2975000652(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1197115851_gshared (Enumerator_t91811072 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentKey_m447338908((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1197115851_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1197115851(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1291229611_gshared (Enumerator_t91811072 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m761796566((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1291229611_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1291229611(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3298895664_gshared (Enumerator_t91811072 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2243145188((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3298895664_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	Enumerator_Dispose_m3298895664(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1388125215_gshared (Enumerator_t91811072 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2770956757((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1388125215_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	return Enumerator_MoveNext_m1388125215(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m862874469_gshared (Enumerator_t91811072 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3749587448 * L_1 = (KeyValuePair_2_t3749587448 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m862874469_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	return Enumerator_get_Current_m862874469(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
