﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.JsonMapper/<RegisterImporter>c__AnonStorey19`2<System.Object,System.Object>
struct U3CRegisterImporterU3Ec__AnonStorey19_2_t1127651614;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void LitJson.JsonMapper/<RegisterImporter>c__AnonStorey19`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CRegisterImporterU3Ec__AnonStorey19_2__ctor_m239913474_gshared (U3CRegisterImporterU3Ec__AnonStorey19_2_t1127651614 * __this, const MethodInfo* method);
#define U3CRegisterImporterU3Ec__AnonStorey19_2__ctor_m239913474(__this, method) ((  void (*) (U3CRegisterImporterU3Ec__AnonStorey19_2_t1127651614 *, const MethodInfo*))U3CRegisterImporterU3Ec__AnonStorey19_2__ctor_m239913474_gshared)(__this, method)
// System.Object LitJson.JsonMapper/<RegisterImporter>c__AnonStorey19`2<System.Object,System.Object>::<>m__33(System.Object)
extern "C"  Il2CppObject * U3CRegisterImporterU3Ec__AnonStorey19_2_U3CU3Em__33_m541805620_gshared (U3CRegisterImporterU3Ec__AnonStorey19_2_t1127651614 * __this, Il2CppObject * ___input0, const MethodInfo* method);
#define U3CRegisterImporterU3Ec__AnonStorey19_2_U3CU3Em__33_m541805620(__this, ___input0, method) ((  Il2CppObject * (*) (U3CRegisterImporterU3Ec__AnonStorey19_2_t1127651614 *, Il2CppObject *, const MethodInfo*))U3CRegisterImporterU3Ec__AnonStorey19_2_U3CU3Em__33_m541805620_gshared)(__this, ___input0, method)
