﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Attribute542643598.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonAlias
struct  JsonAlias_t2355411216  : public Attribute_t542643598
{
public:
	// System.String LitJson.JsonAlias::<Alias>k__BackingField
	String_t* ___U3CAliasU3Ek__BackingField_0;
	// System.Boolean LitJson.JsonAlias::<AcceptOriginal>k__BackingField
	bool ___U3CAcceptOriginalU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CAliasU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonAlias_t2355411216, ___U3CAliasU3Ek__BackingField_0)); }
	inline String_t* get_U3CAliasU3Ek__BackingField_0() const { return ___U3CAliasU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CAliasU3Ek__BackingField_0() { return &___U3CAliasU3Ek__BackingField_0; }
	inline void set_U3CAliasU3Ek__BackingField_0(String_t* value)
	{
		___U3CAliasU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAliasU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CAcceptOriginalU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonAlias_t2355411216, ___U3CAcceptOriginalU3Ek__BackingField_1)); }
	inline bool get_U3CAcceptOriginalU3Ek__BackingField_1() const { return ___U3CAcceptOriginalU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CAcceptOriginalU3Ek__BackingField_1() { return &___U3CAcceptOriginalU3Ek__BackingField_1; }
	inline void set_U3CAcceptOriginalU3Ek__BackingField_1(bool value)
	{
		___U3CAcceptOriginalU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
