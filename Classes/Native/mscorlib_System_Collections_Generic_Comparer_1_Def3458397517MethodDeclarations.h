﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct DefaultComparer_t3458397517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I674159988.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor()
extern "C"  void DefaultComparer__ctor_m2039684834_gshared (DefaultComparer_t3458397517 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2039684834(__this, method) ((  void (*) (DefaultComparer_t3458397517 *, const MethodInfo*))DefaultComparer__ctor_m2039684834_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1125374391_gshared (DefaultComparer_t3458397517 * __this, IndexedColumn_t674159988  ___x0, IndexedColumn_t674159988  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m1125374391(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t3458397517 *, IndexedColumn_t674159988 , IndexedColumn_t674159988 , const MethodInfo*))DefaultComparer_Compare_m1125374391_gshared)(__this, ___x0, ___y1, method)
