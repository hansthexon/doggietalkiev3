﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey12
struct U3CInsertAllU3Ec__AnonStorey12_t1483990457;

#include "codegen/il2cpp-codegen.h"

// System.Void SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey12::.ctor()
extern "C"  void U3CInsertAllU3Ec__AnonStorey12__ctor_m4249170780 (U3CInsertAllU3Ec__AnonStorey12_t1483990457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey12::<>m__6()
extern "C"  void U3CInsertAllU3Ec__AnonStorey12_U3CU3Em__6_m2062909773 (U3CInsertAllU3Ec__AnonStorey12_t1483990457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
