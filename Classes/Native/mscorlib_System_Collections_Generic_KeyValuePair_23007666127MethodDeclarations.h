﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23007666127.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1947527974.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m4017094104_gshared (KeyValuePair_2_t3007666127 * __this, int32_t ___key0, TrackableResultData_t1947527974  ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m4017094104(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3007666127 *, int32_t, TrackableResultData_t1947527974 , const MethodInfo*))KeyValuePair_2__ctor_m4017094104_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2161654122_gshared (KeyValuePair_2_t3007666127 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2161654122(__this, method) ((  int32_t (*) (KeyValuePair_2_t3007666127 *, const MethodInfo*))KeyValuePair_2_get_Key_m2161654122_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3007662271_gshared (KeyValuePair_2_t3007666127 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3007662271(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3007666127 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3007662271_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Value()
extern "C"  TrackableResultData_t1947527974  KeyValuePair_2_get_Value_m616722410_gshared (KeyValuePair_2_t3007666127 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m616722410(__this, method) ((  TrackableResultData_t1947527974  (*) (KeyValuePair_2_t3007666127 *, const MethodInfo*))KeyValuePair_2_get_Value_m616722410_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m4293346479_gshared (KeyValuePair_2_t3007666127 * __this, TrackableResultData_t1947527974  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m4293346479(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3007666127 *, TrackableResultData_t1947527974 , const MethodInfo*))KeyValuePair_2_set_Value_m4293346479_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1671329_gshared (KeyValuePair_2_t3007666127 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1671329(__this, method) ((  String_t* (*) (KeyValuePair_2_t3007666127 *, const MethodInfo*))KeyValuePair_2_ToString_m1671329_gshared)(__this, method)
