﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameManager1/<ShowReplay>c__IteratorA
struct U3CShowReplayU3Ec__IteratorA_t3260812983;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameManager1/<ShowReplay>c__IteratorA::.ctor()
extern "C"  void U3CShowReplayU3Ec__IteratorA__ctor_m832771700 (U3CShowReplayU3Ec__IteratorA_t3260812983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager1/<ShowReplay>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowReplayU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3008405490 (U3CShowReplayU3Ec__IteratorA_t3260812983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager1/<ShowReplay>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowReplayU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m493128714 (U3CShowReplayU3Ec__IteratorA_t3260812983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameManager1/<ShowReplay>c__IteratorA::MoveNext()
extern "C"  bool U3CShowReplayU3Ec__IteratorA_MoveNext_m2450738740 (U3CShowReplayU3Ec__IteratorA_t3260812983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1/<ShowReplay>c__IteratorA::Dispose()
extern "C"  void U3CShowReplayU3Ec__IteratorA_Dispose_m2700627601 (U3CShowReplayU3Ec__IteratorA_t3260812983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1/<ShowReplay>c__IteratorA::Reset()
extern "C"  void U3CShowReplayU3Ec__IteratorA_Reset_m2410575231 (U3CShowReplayU3Ec__IteratorA_t3260812983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
