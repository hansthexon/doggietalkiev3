﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CheckforUpdates/<dowloadVideo>c__Iterator9
struct U3CdowloadVideoU3Ec__Iterator9_t3720676291;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CheckforUpdates/<dowloadVideo>c__Iterator9::.ctor()
extern "C"  void U3CdowloadVideoU3Ec__Iterator9__ctor_m2551836420 (U3CdowloadVideoU3Ec__Iterator9_t3720676291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CheckforUpdates/<dowloadVideo>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdowloadVideoU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2106390440 (U3CdowloadVideoU3Ec__Iterator9_t3720676291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CheckforUpdates/<dowloadVideo>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdowloadVideoU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m1284119328 (U3CdowloadVideoU3Ec__Iterator9_t3720676291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CheckforUpdates/<dowloadVideo>c__Iterator9::MoveNext()
extern "C"  bool U3CdowloadVideoU3Ec__Iterator9_MoveNext_m937799056 (U3CdowloadVideoU3Ec__Iterator9_t3720676291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CheckforUpdates/<dowloadVideo>c__Iterator9::Dispose()
extern "C"  void U3CdowloadVideoU3Ec__Iterator9_Dispose_m2511565785 (U3CdowloadVideoU3Ec__Iterator9_t3720676291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CheckforUpdates/<dowloadVideo>c__Iterator9::Reset()
extern "C"  void U3CdowloadVideoU3Ec__Iterator9_Reset_m2371386067 (U3CdowloadVideoU3Ec__Iterator9_t3720676291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
