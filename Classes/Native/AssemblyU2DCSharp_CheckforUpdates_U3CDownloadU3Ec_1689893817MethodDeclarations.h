﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CheckforUpdates/<Download>c__Iterator8
struct U3CDownloadU3Ec__Iterator8_t1689893817;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CheckforUpdates/<Download>c__Iterator8::.ctor()
extern "C"  void U3CDownloadU3Ec__Iterator8__ctor_m3244934448 (U3CDownloadU3Ec__Iterator8_t1689893817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CheckforUpdates/<Download>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDownloadU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m786420124 (U3CDownloadU3Ec__Iterator8_t1689893817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CheckforUpdates/<Download>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDownloadU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m2236849748 (U3CDownloadU3Ec__Iterator8_t1689893817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CheckforUpdates/<Download>c__Iterator8::MoveNext()
extern "C"  bool U3CDownloadU3Ec__Iterator8_MoveNext_m12018952 (U3CDownloadU3Ec__Iterator8_t1689893817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CheckforUpdates/<Download>c__Iterator8::Dispose()
extern "C"  void U3CDownloadU3Ec__Iterator8_Dispose_m2538920563 (U3CDownloadU3Ec__Iterator8_t1689893817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CheckforUpdates/<Download>c__Iterator8::Reset()
extern "C"  void U3CDownloadU3Ec__Iterator8_Reset_m4073367369 (U3CDownloadU3Ec__Iterator8_t1689893817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
