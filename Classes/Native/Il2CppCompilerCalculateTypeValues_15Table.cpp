﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Analytics_UnityAnalyticsHa3238795095.h"
#include "UnityEngine_SimpleJson_JsonArray1679500587.h"
#include "UnityEngine_SimpleJson_JsonObject2300545015.h"
#include "UnityEngine_SimpleJson_SimpleJson3569903358.h"
#include "UnityEngine_SimpleJson_PocoJsonSerializerStrategy2810850750.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils3032483338.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_G352281633.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_4206365109.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_3084043859.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_1606021565.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_1342095133.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_3264409434.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_2636512595.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_4202596536.h"
#include "UnityEngine_UnityEngine_AndroidJavaException3997329726.h"
#include "UnityEngine_UnityEngine_AndroidJavaRunnableProxy1710049828.h"
#include "UnityEngine_UnityEngine_AndroidJavaProxy4274989947.h"
#include "UnityEngine_UnityEngine_AndroidReflection3899972422.h"
#include "UnityEngine_UnityEngine__AndroidJNIHelper1279286291.h"
#include "UnityEngine_UnityEngine_AndroidJNISafe1205072797.h"
#include "UnityEngine_AOT_MonoPInvokeCallbackAttribute2934651840.h"
#include "UnityEngine_UnityEngine_WrapperlessIcall2585285711.h"
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttrib130316838.h"
#include "UnityEngine_UnityEngine_ThreadAndSerializationSafe2122816804.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine958797062.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent2656950.h"
#include "UnityEngine_UnityEngine_RequireComponent864575032.h"
#include "UnityEngine_UnityEngine_AddComponentMenu1099699699.h"
#include "UnityEngine_UnityEngine_ExecuteInEditMode3043633143.h"
#include "UnityEngine_UnityEngine_HideInInspector2503583610.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_SetupCoroutine3582942563.h"
#include "UnityEngine_UnityEngine_WritableAttribute3715198420.h"
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly1557026495.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3198293052.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_960725851.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1754866149.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3676783238.h"
#include "UnityEngine_UnityEngine_RenderBuffer2767087968.h"
#include "UnityEngine_UnityEngine_CameraClearFlags452084705.h"
#include "UnityEngine_UnityEngine_ColorSpace627621177.h"
#include "UnityEngine_UnityEngine_ScreenOrientation4019489636.h"
#include "UnityEngine_UnityEngine_FilterMode10814199.h"
#include "UnityEngine_UnityEngine_TextureWrapMode3683976566.h"
#include "UnityEngine_UnityEngine_TextureFormat1386130234.h"
#include "UnityEngine_UnityEngine_CubemapFace1725775554.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat3360518468.h"
#include "UnityEngine_UnityEngine_RenderTextureReadWrite2842868372.h"
#include "UnityEngine_UnityEngine_Rendering_CompareFunction457874581.h"
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask926634530.h"
#include "UnityEngine_UnityEngine_Rendering_StencilOp2936374925.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Local3019851150.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP3365630962.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie1333316625.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie3110978151.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score2307748940.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leade4160680639.h"
#include "UnityEngine_UnityEngine_SendMouseEvents3505065032.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo1761367055.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState455716270.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope3775842435.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope2583939667.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range3455291607.h"
#include "UnityEngine_UnityEngine_PropertyAttribute2606999759.h"
#include "UnityEngine_UnityEngine_TooltipAttribute4278647215.h"
#include "UnityEngine_UnityEngine_SpaceAttribute952253354.h"
#include "UnityEngine_UnityEngine_RangeAttribute3336560921.h"
#include "UnityEngine_UnityEngine_TextAreaAttribute2454598508.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttribute936505999.h"
#include "UnityEngine_UnityEngine_StackTraceUtility1881293839.h"
#include "UnityEngine_UnityEngine_UnityException2687879050.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr1565472209.h"
#include "UnityEngine_UnityEngine_StateMachineBehaviour2151245329.h"
#include "UnityEngine_UnityEngine_TrackedReference1045890189.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMo857969000.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache4810721.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2229564840.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall2183506063.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallState3420894182.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall3793436469.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup339478082.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList2295673753.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase828812576.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1500 = { sizeof (UnityAnalyticsHandler_t3238795095), sizeof(UnityAnalyticsHandler_t3238795095_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1500[1] = 
{
	UnityAnalyticsHandler_t3238795095::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1501 = { sizeof (JsonArray_t1679500587), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1502 = { sizeof (JsonObject_t2300545015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1502[1] = 
{
	JsonObject_t2300545015::get_offset_of__members_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1503 = { sizeof (SimpleJson_t3569903358), -1, sizeof(SimpleJson_t3569903358_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1503[2] = 
{
	SimpleJson_t3569903358_StaticFields::get_offset_of__currentJsonSerializerStrategy_0(),
	SimpleJson_t3569903358_StaticFields::get_offset_of__pocoJsonSerializerStrategy_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1504 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1505 = { sizeof (PocoJsonSerializerStrategy_t2810850750), -1, sizeof(PocoJsonSerializerStrategy_t2810850750_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1505[6] = 
{
	PocoJsonSerializerStrategy_t2810850750::get_offset_of_ConstructorCache_0(),
	PocoJsonSerializerStrategy_t2810850750::get_offset_of_GetCache_1(),
	PocoJsonSerializerStrategy_t2810850750::get_offset_of_SetCache_2(),
	PocoJsonSerializerStrategy_t2810850750_StaticFields::get_offset_of_EmptyTypes_3(),
	PocoJsonSerializerStrategy_t2810850750_StaticFields::get_offset_of_ArrayConstructorParameterTypes_4(),
	PocoJsonSerializerStrategy_t2810850750_StaticFields::get_offset_of_Iso8601Format_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1506 = { sizeof (ReflectionUtils_t3032483338), -1, sizeof(ReflectionUtils_t3032483338_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1506[1] = 
{
	ReflectionUtils_t3032483338_StaticFields::get_offset_of_EmptyObjects_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1507 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1507[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1508 = { sizeof (GetDelegate_t352281633), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1509 = { sizeof (SetDelegate_t4206365109), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1510 = { sizeof (ConstructorDelegate_t3084043859), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1511 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1512 = { sizeof (U3CGetConstructorByReflectionU3Ec__AnonStorey2_t1606021565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1512[1] = 
{
	U3CGetConstructorByReflectionU3Ec__AnonStorey2_t1606021565::get_offset_of_constructorInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1513 = { sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1342095133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1513[1] = 
{
	U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1342095133::get_offset_of_methodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1514 = { sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey4_t3264409434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1514[1] = 
{
	U3CGetGetMethodByReflectionU3Ec__AnonStorey4_t3264409434::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1515 = { sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t2636512595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1515[1] = 
{
	U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t2636512595::get_offset_of_methodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1516 = { sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey6_t4202596536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1516[1] = 
{
	U3CGetSetMethodByReflectionU3Ec__AnonStorey6_t4202596536::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1517 = { sizeof (AndroidJavaException_t3997329726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1517[1] = 
{
	AndroidJavaException_t3997329726::get_offset_of_mJavaStackTrace_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1518 = { sizeof (AndroidJavaRunnableProxy_t1710049828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1518[1] = 
{
	AndroidJavaRunnableProxy_t1710049828::get_offset_of_mRunnable_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1519 = { sizeof (AndroidJavaProxy_t4274989947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1519[1] = 
{
	AndroidJavaProxy_t4274989947::get_offset_of_javaInterface_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1520 = { sizeof (AndroidReflection_t3899972422), -1, sizeof(AndroidReflection_t3899972422_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1520[5] = 
{
	AndroidReflection_t3899972422_StaticFields::get_offset_of_s_ReflectionHelperClass_0(),
	AndroidReflection_t3899972422_StaticFields::get_offset_of_s_ReflectionHelperGetConstructorID_1(),
	AndroidReflection_t3899972422_StaticFields::get_offset_of_s_ReflectionHelperGetMethodID_2(),
	AndroidReflection_t3899972422_StaticFields::get_offset_of_s_ReflectionHelperGetFieldID_3(),
	AndroidReflection_t3899972422_StaticFields::get_offset_of_s_ReflectionHelperNewProxyInstance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1521 = { sizeof (_AndroidJNIHelper_t1279286291), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1522 = { sizeof (AndroidJNISafe_t1205072797), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1523 = { sizeof (MonoPInvokeCallbackAttribute_t2934651840), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1524 = { sizeof (WrapperlessIcall_t2585285711), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1525 = { sizeof (IL2CPPStructAlignmentAttribute_t130316838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1525[1] = 
{
	IL2CPPStructAlignmentAttribute_t130316838::get_offset_of_Align_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1526 = { sizeof (ThreadAndSerializationSafe_t2122816804), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1527 = { sizeof (AttributeHelperEngine_t958797062), -1, sizeof(AttributeHelperEngine_t958797062_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1527[3] = 
{
	AttributeHelperEngine_t958797062_StaticFields::get_offset_of__disallowMultipleComponentArray_0(),
	AttributeHelperEngine_t958797062_StaticFields::get_offset_of__executeInEditModeArray_1(),
	AttributeHelperEngine_t958797062_StaticFields::get_offset_of__requireComponentArray_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1528 = { sizeof (DisallowMultipleComponent_t2656950), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1529 = { sizeof (RequireComponent_t864575032), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1529[3] = 
{
	RequireComponent_t864575032::get_offset_of_m_Type0_0(),
	RequireComponent_t864575032::get_offset_of_m_Type1_1(),
	RequireComponent_t864575032::get_offset_of_m_Type2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1530 = { sizeof (AddComponentMenu_t1099699699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1530[2] = 
{
	AddComponentMenu_t1099699699::get_offset_of_m_AddComponentMenu_0(),
	AddComponentMenu_t1099699699::get_offset_of_m_Ordering_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1531 = { sizeof (ExecuteInEditMode_t3043633143), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1532 = { sizeof (HideInInspector_t2503583610), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1533 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1533[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1534 = { sizeof (Color_t2020392075)+ sizeof (Il2CppObject), sizeof(Color_t2020392075_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1534[4] = 
{
	Color_t2020392075::get_offset_of_r_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t2020392075::get_offset_of_g_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t2020392075::get_offset_of_b_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t2020392075::get_offset_of_a_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1535 = { sizeof (SetupCoroutine_t3582942563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1536 = { sizeof (WritableAttribute_t3715198420), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1537 = { sizeof (AssemblyIsEditorAssembly_t1557026495), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1538 = { sizeof (GcUserProfileData_t3198293052)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1538[4] = 
{
	GcUserProfileData_t3198293052::get_offset_of_userName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcUserProfileData_t3198293052::get_offset_of_userID_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcUserProfileData_t3198293052::get_offset_of_isFriend_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcUserProfileData_t3198293052::get_offset_of_image_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1539 = { sizeof (GcAchievementDescriptionData_t960725851)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1539[7] = 
{
	GcAchievementDescriptionData_t960725851::get_offset_of_m_Identifier_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t960725851::get_offset_of_m_Title_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t960725851::get_offset_of_m_Image_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t960725851::get_offset_of_m_AchievedDescription_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t960725851::get_offset_of_m_UnachievedDescription_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t960725851::get_offset_of_m_Hidden_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t960725851::get_offset_of_m_Points_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1540 = { sizeof (GcAchievementData_t1754866149)+ sizeof (Il2CppObject), sizeof(GcAchievementData_t1754866149_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1540[5] = 
{
	GcAchievementData_t1754866149::get_offset_of_m_Identifier_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t1754866149::get_offset_of_m_PercentCompleted_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t1754866149::get_offset_of_m_Completed_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t1754866149::get_offset_of_m_Hidden_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t1754866149::get_offset_of_m_LastReportedDate_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1541 = { sizeof (GcScoreData_t3676783238)+ sizeof (Il2CppObject), sizeof(GcScoreData_t3676783238_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1541[7] = 
{
	GcScoreData_t3676783238::get_offset_of_m_Category_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t3676783238::get_offset_of_m_ValueLow_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t3676783238::get_offset_of_m_ValueHigh_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t3676783238::get_offset_of_m_Date_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t3676783238::get_offset_of_m_FormattedValue_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t3676783238::get_offset_of_m_PlayerID_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t3676783238::get_offset_of_m_Rank_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1542 = { sizeof (RenderBuffer_t2767087968)+ sizeof (Il2CppObject), sizeof(RenderBuffer_t2767087968_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1542[2] = 
{
	RenderBuffer_t2767087968::get_offset_of_m_RenderTextureInstanceID_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RenderBuffer_t2767087968::get_offset_of_m_BufferPtr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1543 = { sizeof (CameraClearFlags_t452084705)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1543[6] = 
{
	CameraClearFlags_t452084705::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1544 = { sizeof (ColorSpace_t627621177)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1544[4] = 
{
	ColorSpace_t627621177::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1545 = { sizeof (ScreenOrientation_t4019489636)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1545[8] = 
{
	ScreenOrientation_t4019489636::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1546 = { sizeof (FilterMode_t10814199)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1546[4] = 
{
	FilterMode_t10814199::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1547 = { sizeof (TextureWrapMode_t3683976566)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1547[3] = 
{
	TextureWrapMode_t3683976566::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1548 = { sizeof (TextureFormat_t1386130234)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1548[47] = 
{
	TextureFormat_t1386130234::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1549 = { sizeof (CubemapFace_t1725775554)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1549[8] = 
{
	CubemapFace_t1725775554::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1550 = { sizeof (RenderTextureFormat_t3360518468)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1550[22] = 
{
	RenderTextureFormat_t3360518468::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1551 = { sizeof (RenderTextureReadWrite_t2842868372)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1551[4] = 
{
	RenderTextureReadWrite_t2842868372::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1552 = { sizeof (CompareFunction_t457874581)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1552[10] = 
{
	CompareFunction_t457874581::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1553 = { sizeof (ColorWriteMask_t926634530)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1553[6] = 
{
	ColorWriteMask_t926634530::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1554 = { sizeof (StencilOp_t2936374925)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1554[9] = 
{
	StencilOp_t2936374925::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1555 = { sizeof (LocalUser_t3019851150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1555[3] = 
{
	LocalUser_t3019851150::get_offset_of_m_Friends_5(),
	LocalUser_t3019851150::get_offset_of_m_Authenticated_6(),
	LocalUser_t3019851150::get_offset_of_m_Underage_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1556 = { sizeof (UserProfile_t3365630962), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1556[5] = 
{
	UserProfile_t3365630962::get_offset_of_m_UserName_0(),
	UserProfile_t3365630962::get_offset_of_m_ID_1(),
	UserProfile_t3365630962::get_offset_of_m_IsFriend_2(),
	UserProfile_t3365630962::get_offset_of_m_State_3(),
	UserProfile_t3365630962::get_offset_of_m_Image_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1557 = { sizeof (Achievement_t1333316625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1557[5] = 
{
	Achievement_t1333316625::get_offset_of_m_Completed_0(),
	Achievement_t1333316625::get_offset_of_m_Hidden_1(),
	Achievement_t1333316625::get_offset_of_m_LastReportedDate_2(),
	Achievement_t1333316625::get_offset_of_U3CidU3Ek__BackingField_3(),
	Achievement_t1333316625::get_offset_of_U3CpercentCompletedU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1558 = { sizeof (AchievementDescription_t3110978151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1558[7] = 
{
	AchievementDescription_t3110978151::get_offset_of_m_Title_0(),
	AchievementDescription_t3110978151::get_offset_of_m_Image_1(),
	AchievementDescription_t3110978151::get_offset_of_m_AchievedDescription_2(),
	AchievementDescription_t3110978151::get_offset_of_m_UnachievedDescription_3(),
	AchievementDescription_t3110978151::get_offset_of_m_Hidden_4(),
	AchievementDescription_t3110978151::get_offset_of_m_Points_5(),
	AchievementDescription_t3110978151::get_offset_of_U3CidU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1559 = { sizeof (Score_t2307748940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1559[6] = 
{
	Score_t2307748940::get_offset_of_m_Date_0(),
	Score_t2307748940::get_offset_of_m_FormattedValue_1(),
	Score_t2307748940::get_offset_of_m_UserID_2(),
	Score_t2307748940::get_offset_of_m_Rank_3(),
	Score_t2307748940::get_offset_of_U3CleaderboardIDU3Ek__BackingField_4(),
	Score_t2307748940::get_offset_of_U3CvalueU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1560 = { sizeof (Leaderboard_t4160680639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1560[10] = 
{
	Leaderboard_t4160680639::get_offset_of_m_Loading_0(),
	Leaderboard_t4160680639::get_offset_of_m_LocalUserScore_1(),
	Leaderboard_t4160680639::get_offset_of_m_MaxRange_2(),
	Leaderboard_t4160680639::get_offset_of_m_Scores_3(),
	Leaderboard_t4160680639::get_offset_of_m_Title_4(),
	Leaderboard_t4160680639::get_offset_of_m_UserIDs_5(),
	Leaderboard_t4160680639::get_offset_of_U3CidU3Ek__BackingField_6(),
	Leaderboard_t4160680639::get_offset_of_U3CuserScopeU3Ek__BackingField_7(),
	Leaderboard_t4160680639::get_offset_of_U3CrangeU3Ek__BackingField_8(),
	Leaderboard_t4160680639::get_offset_of_U3CtimeScopeU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1561 = { sizeof (SendMouseEvents_t3505065032), -1, sizeof(SendMouseEvents_t3505065032_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1561[5] = 
{
	SendMouseEvents_t3505065032_StaticFields::get_offset_of_s_MouseUsed_0(),
	SendMouseEvents_t3505065032_StaticFields::get_offset_of_m_LastHit_1(),
	SendMouseEvents_t3505065032_StaticFields::get_offset_of_m_MouseDownHit_2(),
	SendMouseEvents_t3505065032_StaticFields::get_offset_of_m_CurrentHit_3(),
	SendMouseEvents_t3505065032_StaticFields::get_offset_of_m_Cameras_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1562 = { sizeof (HitInfo_t1761367055)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1562[2] = 
{
	HitInfo_t1761367055::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HitInfo_t1761367055::get_offset_of_camera_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1563 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1564 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1565 = { sizeof (UserState_t455716270)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1565[6] = 
{
	UserState_t455716270::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1566 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1567 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1568 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1569 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1570 = { sizeof (UserScope_t3775842435)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1570[3] = 
{
	UserScope_t3775842435::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1571 = { sizeof (TimeScope_t2583939667)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1571[4] = 
{
	TimeScope_t2583939667::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1572 = { sizeof (Range_t3455291607)+ sizeof (Il2CppObject), sizeof(Range_t3455291607_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1572[2] = 
{
	Range_t3455291607::get_offset_of_from_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Range_t3455291607::get_offset_of_count_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1573 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1574 = { sizeof (PropertyAttribute_t2606999759), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1575 = { sizeof (TooltipAttribute_t4278647215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1575[1] = 
{
	TooltipAttribute_t4278647215::get_offset_of_tooltip_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1576 = { sizeof (SpaceAttribute_t952253354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1576[1] = 
{
	SpaceAttribute_t952253354::get_offset_of_height_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1577 = { sizeof (RangeAttribute_t3336560921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1577[2] = 
{
	RangeAttribute_t3336560921::get_offset_of_min_0(),
	RangeAttribute_t3336560921::get_offset_of_max_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1578 = { sizeof (TextAreaAttribute_t2454598508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1578[2] = 
{
	TextAreaAttribute_t2454598508::get_offset_of_minLines_0(),
	TextAreaAttribute_t2454598508::get_offset_of_maxLines_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1579 = { sizeof (SelectionBaseAttribute_t936505999), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1580 = { sizeof (StackTraceUtility_t1881293839), -1, sizeof(StackTraceUtility_t1881293839_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1580[1] = 
{
	StackTraceUtility_t1881293839_StaticFields::get_offset_of_projectFolder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1581 = { sizeof (UnityException_t2687879050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1581[2] = 
{
	0,
	UnityException_t2687879050::get_offset_of_unityStackTrace_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1582 = { sizeof (SharedBetweenAnimatorsAttribute_t1565472209), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1583 = { sizeof (StateMachineBehaviour_t2151245329), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1584 = { sizeof (TrackedReference_t1045890189), sizeof(TrackedReference_t1045890189_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1584[1] = 
{
	TrackedReference_t1045890189::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1585 = { sizeof (PersistentListenerMode_t857969000)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1585[8] = 
{
	PersistentListenerMode_t857969000::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1586 = { sizeof (ArgumentCache_t4810721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1586[6] = 
{
	ArgumentCache_t4810721::get_offset_of_m_ObjectArgument_0(),
	ArgumentCache_t4810721::get_offset_of_m_ObjectArgumentAssemblyTypeName_1(),
	ArgumentCache_t4810721::get_offset_of_m_IntArgument_2(),
	ArgumentCache_t4810721::get_offset_of_m_FloatArgument_3(),
	ArgumentCache_t4810721::get_offset_of_m_StringArgument_4(),
	ArgumentCache_t4810721::get_offset_of_m_BoolArgument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1587 = { sizeof (BaseInvokableCall_t2229564840), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1588 = { sizeof (InvokableCall_t2183506063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1588[1] = 
{
	InvokableCall_t2183506063::get_offset_of_Delegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1589 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1589[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1590 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1590[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1591 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1591[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1592 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1592[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1593 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1593[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1594 = { sizeof (UnityEventCallState_t3420894182)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1594[4] = 
{
	UnityEventCallState_t3420894182::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1595 = { sizeof (PersistentCall_t3793436469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1595[5] = 
{
	PersistentCall_t3793436469::get_offset_of_m_Target_0(),
	PersistentCall_t3793436469::get_offset_of_m_MethodName_1(),
	PersistentCall_t3793436469::get_offset_of_m_Mode_2(),
	PersistentCall_t3793436469::get_offset_of_m_Arguments_3(),
	PersistentCall_t3793436469::get_offset_of_m_CallState_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1596 = { sizeof (PersistentCallGroup_t339478082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1596[1] = 
{
	PersistentCallGroup_t339478082::get_offset_of_m_Calls_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1597 = { sizeof (InvokableCallList_t2295673753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1597[4] = 
{
	InvokableCallList_t2295673753::get_offset_of_m_PersistentCalls_0(),
	InvokableCallList_t2295673753::get_offset_of_m_RuntimeCalls_1(),
	InvokableCallList_t2295673753::get_offset_of_m_ExecutingCalls_2(),
	InvokableCallList_t2295673753::get_offset_of_m_NeedsUpdate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1598 = { sizeof (UnityEventBase_t828812576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1598[4] = 
{
	UnityEventBase_t828812576::get_offset_of_m_Calls_0(),
	UnityEventBase_t828812576::get_offset_of_m_PersistentCalls_1(),
	UnityEventBase_t828812576::get_offset_of_m_TypeName_2(),
	UnityEventBase_t828812576::get_offset_of_m_CallsDirty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1599 = { sizeof (UnityEvent_t408735097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1599[1] = 
{
	UnityEvent_t408735097::get_offset_of_m_InvokeArray_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
