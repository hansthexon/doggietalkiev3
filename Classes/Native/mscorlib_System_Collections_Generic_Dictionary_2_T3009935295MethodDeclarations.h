﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo,System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>>
struct Transform_1_t3009935295;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22492407411.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I848034765.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo,System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2855470006_gshared (Transform_1_t3009935295 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m2855470006(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3009935295 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m2855470006_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo,System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2492407411  Transform_1_Invoke_m1196143926_gshared (Transform_1_t3009935295 * __this, Il2CppObject * ___key0, IndexInfo_t848034765  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m1196143926(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t2492407411  (*) (Transform_1_t3009935295 *, Il2CppObject *, IndexInfo_t848034765 , const MethodInfo*))Transform_1_Invoke_m1196143926_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo,System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m540102111_gshared (Transform_1_t3009935295 * __this, Il2CppObject * ___key0, IndexInfo_t848034765  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m540102111(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3009935295 *, Il2CppObject *, IndexInfo_t848034765 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m540102111_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo,System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2492407411  Transform_1_EndInvoke_m166390924_gshared (Transform_1_t3009935295 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m166390924(__this, ___result0, method) ((  KeyValuePair_2_t2492407411  (*) (Transform_1_t3009935295 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m166390924_gshared)(__this, ___result0, method)
