﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Person
struct Person_t3241917763;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Person::.ctor()
extern "C"  void Person__ctor_m3564590992 (Person_t3241917763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Person::get_Id()
extern "C"  int32_t Person_get_Id_m3348941488 (Person_t3241917763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Person::set_Id(System.Int32)
extern "C"  void Person_set_Id_m257094623 (Person_t3241917763 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Person::get_Name()
extern "C"  String_t* Person_get_Name_m1380952829 (Person_t3241917763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Person::set_Name(System.String)
extern "C"  void Person_set_Name_m1821783746 (Person_t3241917763 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Person::get_Surname()
extern "C"  String_t* Person_get_Surname_m1155548065 (Person_t3241917763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Person::set_Surname(System.String)
extern "C"  void Person_set_Surname_m407263796 (Person_t3241917763 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Person::get_Age()
extern "C"  int32_t Person_get_Age_m899276194 (Person_t3241917763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Person::set_Age(System.Int32)
extern "C"  void Person_set_Age_m1134690065 (Person_t3241917763 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
