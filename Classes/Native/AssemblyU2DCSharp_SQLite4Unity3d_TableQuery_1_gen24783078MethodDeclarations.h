﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableQuery_1_gen4026329353MethodDeclarations.h"

// System.Void SQLite4Unity3d.TableQuery`1<RoomObject>::.ctor(SQLite4Unity3d.SQLiteConnection,SQLite4Unity3d.TableMapping)
#define TableQuery_1__ctor_m1208630748(__this, ___conn0, ___table1, method) ((  void (*) (TableQuery_1_t24783078 *, SQLiteConnection_t3529499386 *, TableMapping_t3898710812 *, const MethodInfo*))TableQuery_1__ctor_m2525682756_gshared)(__this, ___conn0, ___table1, method)
// System.Void SQLite4Unity3d.TableQuery`1<RoomObject>::.ctor(SQLite4Unity3d.SQLiteConnection)
#define TableQuery_1__ctor_m439902850(__this, ___conn0, method) ((  void (*) (TableQuery_1_t24783078 *, SQLiteConnection_t3529499386 *, const MethodInfo*))TableQuery_1__ctor_m821176910_gshared)(__this, ___conn0, method)
// System.Collections.IEnumerator SQLite4Unity3d.TableQuery`1<RoomObject>::System.Collections.IEnumerable.GetEnumerator()
#define TableQuery_1_System_Collections_IEnumerable_GetEnumerator_m3664364547(__this, method) ((  Il2CppObject * (*) (TableQuery_1_t24783078 *, const MethodInfo*))TableQuery_1_System_Collections_IEnumerable_GetEnumerator_m2306269163_gshared)(__this, method)
// SQLite4Unity3d.SQLiteConnection SQLite4Unity3d.TableQuery`1<RoomObject>::get_Connection()
#define TableQuery_1_get_Connection_m151732620(__this, method) ((  SQLiteConnection_t3529499386 * (*) (TableQuery_1_t24783078 *, const MethodInfo*))TableQuery_1_get_Connection_m2018438234_gshared)(__this, method)
// System.Void SQLite4Unity3d.TableQuery`1<RoomObject>::set_Connection(SQLite4Unity3d.SQLiteConnection)
#define TableQuery_1_set_Connection_m21788905(__this, ___value0, method) ((  void (*) (TableQuery_1_t24783078 *, SQLiteConnection_t3529499386 *, const MethodInfo*))TableQuery_1_set_Connection_m3046933261_gshared)(__this, ___value0, method)
// SQLite4Unity3d.TableMapping SQLite4Unity3d.TableQuery`1<RoomObject>::get_Table()
#define TableQuery_1_get_Table_m1271069712(__this, method) ((  TableMapping_t3898710812 * (*) (TableQuery_1_t24783078 *, const MethodInfo*))TableQuery_1_get_Table_m2172867230_gshared)(__this, method)
// System.Void SQLite4Unity3d.TableQuery`1<RoomObject>::set_Table(SQLite4Unity3d.TableMapping)
#define TableQuery_1_set_Table_m2710725687(__this, ___value0, method) ((  void (*) (TableQuery_1_t24783078 *, TableMapping_t3898710812 *, const MethodInfo*))TableQuery_1_set_Table_m3580780791_gshared)(__this, ___value0, method)
// SQLite4Unity3d.TableQuery`1<T> SQLite4Unity3d.TableQuery`1<RoomObject>::Where(System.Linq.Expressions.Expression`1<System.Func`2<T,System.Boolean>>)
#define TableQuery_1_Where_m1662653116(__this, ___predExpr0, method) ((  TableQuery_1_t24783078 * (*) (TableQuery_1_t24783078 *, Expression_1_t2968240103 *, const MethodInfo*))TableQuery_1_Where_m4178518972_gshared)(__this, ___predExpr0, method)
// SQLite4Unity3d.TableQuery`1<T> SQLite4Unity3d.TableQuery`1<RoomObject>::Take(System.Int32)
#define TableQuery_1_Take_m2143700386(__this, ___n0, method) ((  TableQuery_1_t24783078 * (*) (TableQuery_1_t24783078 *, int32_t, const MethodInfo*))TableQuery_1_Take_m2215285898_gshared)(__this, ___n0, method)
// SQLite4Unity3d.TableQuery`1<T> SQLite4Unity3d.TableQuery`1<RoomObject>::Skip(System.Int32)
#define TableQuery_1_Skip_m704073938(__this, ___n0, method) ((  TableQuery_1_t24783078 * (*) (TableQuery_1_t24783078 *, int32_t, const MethodInfo*))TableQuery_1_Skip_m2335687758_gshared)(__this, ___n0, method)
// T SQLite4Unity3d.TableQuery`1<RoomObject>::ElementAt(System.Int32)
#define TableQuery_1_ElementAt_m3958515943(__this, ___index0, method) ((  RoomObject_t2982870316 * (*) (TableQuery_1_t24783078 *, int32_t, const MethodInfo*))TableQuery_1_ElementAt_m4176579987_gshared)(__this, ___index0, method)
// SQLite4Unity3d.TableQuery`1<T> SQLite4Unity3d.TableQuery`1<RoomObject>::Deferred()
#define TableQuery_1_Deferred_m104499129(__this, method) ((  TableQuery_1_t24783078 * (*) (TableQuery_1_t24783078 *, const MethodInfo*))TableQuery_1_Deferred_m2927110549_gshared)(__this, method)
// System.Void SQLite4Unity3d.TableQuery`1<RoomObject>::AddWhere(System.Linq.Expressions.Expression)
#define TableQuery_1_AddWhere_m41330394(__this, ___pred0, method) ((  void (*) (TableQuery_1_t24783078 *, Expression_t114864668 *, const MethodInfo*))TableQuery_1_AddWhere_m752477770_gshared)(__this, ___pred0, method)
// SQLite4Unity3d.SQLiteCommand SQLite4Unity3d.TableQuery`1<RoomObject>::GenerateCommand(System.String)
#define TableQuery_1_GenerateCommand_m1817274898(__this, ___selectionList0, method) ((  SQLiteCommand_t2935685145 * (*) (TableQuery_1_t24783078 *, String_t*, const MethodInfo*))TableQuery_1_GenerateCommand_m4193037006_gshared)(__this, ___selectionList0, method)
// SQLite4Unity3d.TableQuery`1/CompileResult<T> SQLite4Unity3d.TableQuery`1<RoomObject>::CompileExpr(System.Linq.Expressions.Expression,System.Collections.Generic.List`1<System.Object>)
#define TableQuery_1_CompileExpr_m2134338265(__this, ___expr0, ___queryArgs1, method) ((  CompileResult_t162244657 * (*) (TableQuery_1_t24783078 *, Expression_t114864668 *, List_1_t2058570427 *, const MethodInfo*))TableQuery_1_CompileExpr_m3818611973_gshared)(__this, ___expr0, ___queryArgs1, method)
// System.Object SQLite4Unity3d.TableQuery`1<RoomObject>::ConvertTo(System.Object,System.Type)
#define TableQuery_1_ConvertTo_m1797845532(__this /* static, unused */, ___obj0, ___t1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Type_t *, const MethodInfo*))TableQuery_1_ConvertTo_m2602095610_gshared)(__this /* static, unused */, ___obj0, ___t1, method)
// System.String SQLite4Unity3d.TableQuery`1<RoomObject>::CompileNullBinaryExpression(System.Linq.Expressions.BinaryExpression,SQLite4Unity3d.TableQuery`1/CompileResult<T>)
#define TableQuery_1_CompileNullBinaryExpression_m848554081(__this, ___expression0, ___parameter1, method) ((  String_t* (*) (TableQuery_1_t24783078 *, BinaryExpression_t2159924157 *, CompileResult_t162244657 *, const MethodInfo*))TableQuery_1_CompileNullBinaryExpression_m4251067313_gshared)(__this, ___expression0, ___parameter1, method)
// System.String SQLite4Unity3d.TableQuery`1<RoomObject>::GetSqlName(System.Linq.Expressions.Expression)
#define TableQuery_1_GetSqlName_m4189799860(__this, ___expr0, method) ((  String_t* (*) (TableQuery_1_t24783078 *, Expression_t114864668 *, const MethodInfo*))TableQuery_1_GetSqlName_m1128366698_gshared)(__this, ___expr0, method)
// System.Int32 SQLite4Unity3d.TableQuery`1<RoomObject>::Count()
#define TableQuery_1_Count_m3211517087(__this, method) ((  int32_t (*) (TableQuery_1_t24783078 *, const MethodInfo*))TableQuery_1_Count_m3413050791_gshared)(__this, method)
// System.Int32 SQLite4Unity3d.TableQuery`1<RoomObject>::Count(System.Linq.Expressions.Expression`1<System.Func`2<T,System.Boolean>>)
#define TableQuery_1_Count_m1265095288(__this, ___predExpr0, method) ((  int32_t (*) (TableQuery_1_t24783078 *, Expression_1_t2968240103 *, const MethodInfo*))TableQuery_1_Count_m3405720028_gshared)(__this, ___predExpr0, method)
// System.Collections.Generic.IEnumerator`1<T> SQLite4Unity3d.TableQuery`1<RoomObject>::GetEnumerator()
#define TableQuery_1_GetEnumerator_m2698063272(__this, method) ((  Il2CppObject* (*) (TableQuery_1_t24783078 *, const MethodInfo*))TableQuery_1_GetEnumerator_m710809644_gshared)(__this, method)
// T SQLite4Unity3d.TableQuery`1<RoomObject>::First()
#define TableQuery_1_First_m2997962243(__this, method) ((  RoomObject_t2982870316 * (*) (TableQuery_1_t24783078 *, const MethodInfo*))TableQuery_1_First_m3018168943_gshared)(__this, method)
// T SQLite4Unity3d.TableQuery`1<RoomObject>::FirstOrDefault()
#define TableQuery_1_FirstOrDefault_m2177088689(__this, method) ((  RoomObject_t2982870316 * (*) (TableQuery_1_t24783078 *, const MethodInfo*))TableQuery_1_FirstOrDefault_m3828705525_gshared)(__this, method)
// System.String SQLite4Unity3d.TableQuery`1<RoomObject>::<GenerateCommand>m__11(SQLite4Unity3d.BaseTableQuery/Ordering)
#define TableQuery_1_U3CGenerateCommandU3Em__11_m2917695182(__this /* static, unused */, ___o0, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, Ordering_t1038862794 *, const MethodInfo*))TableQuery_1_U3CGenerateCommandU3Em__11_m1866588696_gshared)(__this /* static, unused */, ___o0, method)
// System.String SQLite4Unity3d.TableQuery`1<RoomObject>::<CompileExpr>m__12(SQLite4Unity3d.TableQuery`1/CompileResult<T>)
#define TableQuery_1_U3CCompileExprU3Em__12_m3016385669(__this /* static, unused */, ___a0, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, CompileResult_t162244657 *, const MethodInfo*))TableQuery_1_U3CCompileExprU3Em__12_m1764005629_gshared)(__this /* static, unused */, ___a0, method)
