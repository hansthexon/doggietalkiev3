﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t1456101953;
// Vuforia.Marker
struct Marker_t1563082680;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"

// Vuforia.Marker Vuforia.MarkerAbstractBehaviour::get_Marker()
extern "C"  Il2CppObject * MarkerAbstractBehaviour_get_Marker_m1448736941 (MarkerAbstractBehaviour_t1456101953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::.ctor()
extern "C"  void MarkerAbstractBehaviour__ctor_m2689139661 (MarkerAbstractBehaviour_t1456101953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::InternalUnregisterTrackable()
extern "C"  void MarkerAbstractBehaviour_InternalUnregisterTrackable_m1113807269 (MarkerAbstractBehaviour_t1456101953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerAbstractBehaviour::CorrectScaleImpl()
extern "C"  bool MarkerAbstractBehaviour_CorrectScaleImpl_m1400180015 (MarkerAbstractBehaviour_t1456101953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorMarkerBehaviour.get_MarkerID()
extern "C"  int32_t MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_get_MarkerID_m444088182 (MarkerAbstractBehaviour_t1456101953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorMarkerBehaviour.SetMarkerID(System.Int32)
extern "C"  bool MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_SetMarkerID_m1463658888 (MarkerAbstractBehaviour_t1456101953 * __this, int32_t ___markerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorMarkerBehaviour.InitializeMarker(Vuforia.Marker)
extern "C"  void MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_InitializeMarker_m814264590 (MarkerAbstractBehaviour_t1456101953 * __this, Il2CppObject * ___marker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C"  bool MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m4120378665 (MarkerAbstractBehaviour_t1456101953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C"  void MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m1158205950 (MarkerAbstractBehaviour_t1456101953 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C"  Transform_t3275118058 * MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m3622578984 (MarkerAbstractBehaviour_t1456101953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C"  GameObject_t1756533147 * MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m2817183104 (MarkerAbstractBehaviour_t1456101953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
