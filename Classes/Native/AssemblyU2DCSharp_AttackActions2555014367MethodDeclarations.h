﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackActions
struct AttackActions_t2555014367;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackActions::.ctor()
extern "C"  void AttackActions__ctor_m2327915594 (AttackActions_t2555014367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackActions::Start()
extern "C"  void AttackActions_Start_m2866084134 (AttackActions_t2555014367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackActions::EnableNinjahand()
extern "C"  void AttackActions_EnableNinjahand_m1660634878 (AttackActions_t2555014367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackActions::DisableNinjahand()
extern "C"  void AttackActions_DisableNinjahand_m3716601507 (AttackActions_t2555014367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackActions::Attack()
extern "C"  void AttackActions_Attack_m2513271428 (AttackActions_t2555014367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
