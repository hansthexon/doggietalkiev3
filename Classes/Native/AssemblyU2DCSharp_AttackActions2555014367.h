﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackActions
struct  AttackActions_t2555014367  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject AttackActions::Ninjahand
	GameObject_t1756533147 * ___Ninjahand_2;
	// UnityEngine.Animator AttackActions::ninjaanim
	Animator_t69676727 * ___ninjaanim_3;

public:
	inline static int32_t get_offset_of_Ninjahand_2() { return static_cast<int32_t>(offsetof(AttackActions_t2555014367, ___Ninjahand_2)); }
	inline GameObject_t1756533147 * get_Ninjahand_2() const { return ___Ninjahand_2; }
	inline GameObject_t1756533147 ** get_address_of_Ninjahand_2() { return &___Ninjahand_2; }
	inline void set_Ninjahand_2(GameObject_t1756533147 * value)
	{
		___Ninjahand_2 = value;
		Il2CppCodeGenWriteBarrier(&___Ninjahand_2, value);
	}

	inline static int32_t get_offset_of_ninjaanim_3() { return static_cast<int32_t>(offsetof(AttackActions_t2555014367, ___ninjaanim_3)); }
	inline Animator_t69676727 * get_ninjaanim_3() const { return ___ninjaanim_3; }
	inline Animator_t69676727 ** get_address_of_ninjaanim_3() { return &___ninjaanim_3; }
	inline void set_ninjaanim_3(Animator_t69676727 * value)
	{
		___ninjaanim_3 = value;
		Il2CppCodeGenWriteBarrier(&___ninjaanim_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
