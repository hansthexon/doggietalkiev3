﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour1779888572.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour4057911311.h"
#include "Vuforia_UnityExtensions_Vuforia_DataSetTrackableBe3452781876.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstra2805337095.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice3827827595.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Camer2705300828.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Focus4087668361.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Camer1654543970.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Video3451594282.h"
#include "Vuforia_UnityExtensions_Vuforia_CloudRecoAbstractB2070832277.h"
#include "Vuforia_UnityExtensions_Vuforia_ReconstructionImpl2149050756.h"
#include "Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbst2687577327.h"
#include "Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbst2100449024.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableImpl3421455115.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTargetImpl1796148526.h"
#include "Vuforia_UnityExtensions_Vuforia_UnityPlayer3164516142.h"
#include "Vuforia_UnityExtensions_Vuforia_NullUnityPlayer754446093.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayer918240325.h"
#include "Vuforia_UnityExtensions_Vuforia_ReconstructionFrom1192715795.h"
#include "Vuforia_UnityExtensions_Vuforia_ViewerButtonType413015468.h"
#include "Vuforia_UnityExtensions_Vuforia_ViewerTrayAlignment791464321.h"
#include "Vuforia_UnityExtensions_Vuforia_ReconstructionFrom4122236588.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilde1628525337.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainInitia1102352056.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracka2391102074.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracke4026264541.h"
#include "Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBeh2669615494.h"
#include "Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstr665872082.h"
#include "Vuforia_UnityExtensions_Vuforia_DataSet626511550.h"
#include "Vuforia_UnityExtensions_Vuforia_DataSet_StorageTyp4268322930.h"
#include "Vuforia_UnityExtensions_Vuforia_DatabaseLoadAbstra1458632096.h"
#include "Vuforia_UnityExtensions_Vuforia_RectangleData934532407.h"
#include "Vuforia_UnityExtensions_Vuforia_RectangleIntData2869769236.h"
#include "Vuforia_UnityExtensions_Vuforia_OrientedBoundingBo3172429123.h"
#include "Vuforia_UnityExtensions_Vuforia_OrientedBoundingBo1484796416.h"
#include "Vuforia_UnityExtensions_Vuforia_BehaviourComponent3267823770.h"
#include "Vuforia_UnityExtensions_Vuforia_BehaviourComponent3849641556.h"
#include "Vuforia_UnityExtensions_Vuforia_CloudRecoImageTarg2427143318.h"
#include "Vuforia_UnityExtensions_Vuforia_CylinderTargetImpl259286421.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetType3906864138.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetData1326050618.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder518883741.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder2061101710.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDeviceImpl610967511.h"
#include "Vuforia_UnityExtensions_Vuforia_DataSetImpl2819025280.h"
#include "Vuforia_UnityExtensions_Vuforia_WordTemplateMode1097144495.h"
#include "Vuforia_UnityExtensions_Vuforia_Image1391689025.h"
#include "Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT3010530044.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageImpl2564717533.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder2457446201.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetImpl869063580.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTracker1568044035.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTrackerImpl1691118791.h"
#include "Vuforia_UnityExtensions_Vuforia_MarkerImpl2101172390.h"
#include "Vuforia_UnityExtensions_Vuforia_MarkerTracker2959118800.h"
#include "Vuforia_UnityExtensions_Vuforia_MarkerTrackerImpl654952814.h"
#include "Vuforia_UnityExtensions_Vuforia_MultiTargetImpl2151485576.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptor2106169489.h"
#include "Vuforia_UnityExtensions_Vuforia_NullWebCamTexAdapt2136158928.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeEditorUtil1237840826.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeEditorUtil1550651348.h"
#include "Vuforia_UnityExtensions_Vuforia_PremiumObjectFactor787152098.h"
#include "Vuforia_UnityExtensions_Vuforia_PremiumObjectFacto2032979060.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager2424874861.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3289840897.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1264148721.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1947527974.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1223885651.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3491121689.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3491121690.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3742236631.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1273964084.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3150040852.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (TrackableBehaviour_t1779888572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2003[8] = 
{
	TrackableBehaviour_t1779888572::get_offset_of_mTrackableName_2(),
	TrackableBehaviour_t1779888572::get_offset_of_mPreviousScale_3(),
	TrackableBehaviour_t1779888572::get_offset_of_mPreserveChildSize_4(),
	TrackableBehaviour_t1779888572::get_offset_of_mInitializedInEditor_5(),
	TrackableBehaviour_t1779888572::get_offset_of_mStatus_6(),
	TrackableBehaviour_t1779888572::get_offset_of_mTrackable_7(),
	TrackableBehaviour_t1779888572::get_offset_of_mTrackableEventHandlers_8(),
	TrackableBehaviour_t1779888572::get_offset_of_U3CTimeStampU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (Status_t4057911311)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2004[7] = 
{
	Status_t4057911311::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (DataSetTrackableBehaviour_t3452781876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2006[11] = 
{
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mDataSetPath_10(),
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mExtendedTracking_11(),
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mInitializeSmartTerrain_12(),
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mReconstructionToInitialize_13(),
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mSmartTerrainOccluderBoundsMin_14(),
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mSmartTerrainOccluderBoundsMax_15(),
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mIsSmartTerrainOccluderOffset_16(),
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mSmartTerrainOccluderOffset_17(),
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mSmartTerrainOccluderRotation_18(),
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mSmartTerrainOccluderLockedInPlace_19(),
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mAutoSetOccluderFromTargetSize_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (ObjectTargetAbstractBehaviour_t2805337095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2007[7] = 
{
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mObjectTarget_21(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mAspectRatioXY_22(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mAspectRatioXZ_23(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mShowBoundingBox_24(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mBBoxMin_25(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mBBoxMax_26(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mPreviewImage_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (CameraDevice_t3827827595), -1, sizeof(CameraDevice_t3827827595_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2008[1] = 
{
	CameraDevice_t3827827595_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (CameraDeviceMode_t2705300828)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2009[4] = 
{
	CameraDeviceMode_t2705300828::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (FocusMode_t4087668361)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2010[6] = 
{
	FocusMode_t4087668361::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (CameraDirection_t1654543970)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2011[4] = 
{
	CameraDirection_t1654543970::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (VideoModeData_t3451594282)+ sizeof (Il2CppObject), sizeof(VideoModeData_t3451594282_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2012[4] = 
{
	VideoModeData_t3451594282::get_offset_of_width_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoModeData_t3451594282::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoModeData_t3451594282::get_offset_of_frameRate_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoModeData_t3451594282::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (CloudRecoAbstractBehaviour_t2070832277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2013[11] = 
{
	CloudRecoAbstractBehaviour_t2070832277::get_offset_of_mObjectTracker_2(),
	CloudRecoAbstractBehaviour_t2070832277::get_offset_of_mCurrentlyInitializing_3(),
	CloudRecoAbstractBehaviour_t2070832277::get_offset_of_mInitSuccess_4(),
	CloudRecoAbstractBehaviour_t2070832277::get_offset_of_mCloudRecoStarted_5(),
	CloudRecoAbstractBehaviour_t2070832277::get_offset_of_mOnInitializedCalled_6(),
	CloudRecoAbstractBehaviour_t2070832277::get_offset_of_mHandlers_7(),
	CloudRecoAbstractBehaviour_t2070832277::get_offset_of_mTargetFinderStartedBeforeDisable_8(),
	CloudRecoAbstractBehaviour_t2070832277::get_offset_of_AccessKey_9(),
	CloudRecoAbstractBehaviour_t2070832277::get_offset_of_SecretKey_10(),
	CloudRecoAbstractBehaviour_t2070832277::get_offset_of_ScanlineColor_11(),
	CloudRecoAbstractBehaviour_t2070832277::get_offset_of_FeaturePointColor_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (ReconstructionImpl_t2149050756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2015[5] = 
{
	ReconstructionImpl_t2149050756::get_offset_of_mNativeReconstructionPtr_0(),
	ReconstructionImpl_t2149050756::get_offset_of_mMaximumAreaIsSet_1(),
	ReconstructionImpl_t2149050756::get_offset_of_mMaximumArea_2(),
	ReconstructionImpl_t2149050756::get_offset_of_mNavMeshPadding_3(),
	ReconstructionImpl_t2149050756::get_offset_of_mNavMeshUpdatesEnabled_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (HideExcessAreaAbstractBehaviour_t2687577327), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2016[7] = 
{
	HideExcessAreaAbstractBehaviour_t2687577327::get_offset_of_mClippingImpl_2(),
	HideExcessAreaAbstractBehaviour_t2687577327::get_offset_of_mClippingMode_3(),
	HideExcessAreaAbstractBehaviour_t2687577327::get_offset_of_mVuforiaBehaviour_4(),
	HideExcessAreaAbstractBehaviour_t2687577327::get_offset_of_mVideoBgMgr_5(),
	HideExcessAreaAbstractBehaviour_t2687577327::get_offset_of_mPlaneOffset_6(),
	HideExcessAreaAbstractBehaviour_t2687577327::get_offset_of_mSceneScaledDown_7(),
	HideExcessAreaAbstractBehaviour_t2687577327::get_offset_of_mStarted_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (CLIPPING_MODE_t2100449024)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2017[4] = 
{
	CLIPPING_MODE_t2100449024::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (TrackableImpl_t3421455115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2018[2] = 
{
	TrackableImpl_t3421455115::get_offset_of_U3CNameU3Ek__BackingField_0(),
	TrackableImpl_t3421455115::get_offset_of_U3CIDU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (ObjectTargetImpl_t1796148526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2019[2] = 
{
	ObjectTargetImpl_t1796148526::get_offset_of_mSize_2(),
	ObjectTargetImpl_t1796148526::get_offset_of_mDataSet_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (UnityPlayer_t3164516142), -1, sizeof(UnityPlayer_t3164516142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2020[1] = 
{
	UnityPlayer_t3164516142_StaticFields::get_offset_of_sPlayer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (NullUnityPlayer_t754446093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (PlayModeUnityPlayer_t918240325), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (ReconstructionFromTargetImpl_t1192715795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2026[6] = 
{
	ReconstructionFromTargetImpl_t1192715795::get_offset_of_mOccluderMin_5(),
	ReconstructionFromTargetImpl_t1192715795::get_offset_of_mOccluderMax_6(),
	ReconstructionFromTargetImpl_t1192715795::get_offset_of_mOccluderOffset_7(),
	ReconstructionFromTargetImpl_t1192715795::get_offset_of_mOccluderRotation_8(),
	ReconstructionFromTargetImpl_t1192715795::get_offset_of_mInitializationTarget_9(),
	ReconstructionFromTargetImpl_t1192715795::get_offset_of_mCanAutoSetInitializationTarget_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (ViewerButtonType_t413015468)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2027[5] = 
{
	ViewerButtonType_t413015468::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (ViewerTrayAlignment_t791464321)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2028[4] = 
{
	ViewerTrayAlignment_t791464321::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (ReconstructionFromTargetAbstractBehaviour_t4122236588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2029[2] = 
{
	ReconstructionFromTargetAbstractBehaviour_t4122236588::get_offset_of_mReconstructionFromTarget_2(),
	ReconstructionFromTargetAbstractBehaviour_t4122236588::get_offset_of_mReconstructionBehaviour_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (SmartTerrainBuilder_t1628525337), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (SmartTerrainInitializationInfo_t1102352056)+ sizeof (Il2CppObject), sizeof(SmartTerrainInitializationInfo_t1102352056_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (SmartTerrainTrackableBehaviour_t2391102074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2032[4] = 
{
	SmartTerrainTrackableBehaviour_t2391102074::get_offset_of_mSmartTerrainTrackable_10(),
	SmartTerrainTrackableBehaviour_t2391102074::get_offset_of_mDisableAutomaticUpdates_11(),
	SmartTerrainTrackableBehaviour_t2391102074::get_offset_of_mMeshFilterToUpdate_12(),
	SmartTerrainTrackableBehaviour_t2391102074::get_offset_of_mMeshColliderToUpdate_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (SmartTerrainTrackerAbstractBehaviour_t4026264541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2034[7] = 
{
	SmartTerrainTrackerAbstractBehaviour_t4026264541::get_offset_of_mAutoInitTracker_2(),
	SmartTerrainTrackerAbstractBehaviour_t4026264541::get_offset_of_mAutoStartTracker_3(),
	SmartTerrainTrackerAbstractBehaviour_t4026264541::get_offset_of_mAutoInitBuilder_4(),
	SmartTerrainTrackerAbstractBehaviour_t4026264541::get_offset_of_mSceneUnitsToMillimeter_5(),
	SmartTerrainTrackerAbstractBehaviour_t4026264541::get_offset_of_mTrackerStarted_6(),
	SmartTerrainTrackerAbstractBehaviour_t4026264541::get_offset_of_mTrackerWasActiveBeforePause_7(),
	SmartTerrainTrackerAbstractBehaviour_t4026264541::get_offset_of_mTrackerWasActiveBeforeDisabling_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (SurfaceAbstractBehaviour_t2669615494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[1] = 
{
	SurfaceAbstractBehaviour_t2669615494::get_offset_of_mSurface_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (CylinderTargetAbstractBehaviour_t665872082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2038[6] = 
{
	CylinderTargetAbstractBehaviour_t665872082::get_offset_of_mCylinderTarget_21(),
	CylinderTargetAbstractBehaviour_t665872082::get_offset_of_mTopDiameterRatio_22(),
	CylinderTargetAbstractBehaviour_t665872082::get_offset_of_mBottomDiameterRatio_23(),
	CylinderTargetAbstractBehaviour_t665872082::get_offset_of_mFrameIndex_24(),
	CylinderTargetAbstractBehaviour_t665872082::get_offset_of_mUpdateFrameIndex_25(),
	CylinderTargetAbstractBehaviour_t665872082::get_offset_of_mFutureScale_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (DataSet_t626511550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (StorageType_t4268322930)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2040[4] = 
{
	StorageType_t4268322930::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (DatabaseLoadAbstractBehaviour_t1458632096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2041[4] = 
{
	DatabaseLoadAbstractBehaviour_t1458632096::get_offset_of_mDatasetsLoaded_2(),
	DatabaseLoadAbstractBehaviour_t1458632096::get_offset_of_mDataSetsToLoad_3(),
	DatabaseLoadAbstractBehaviour_t1458632096::get_offset_of_mDataSetsToActivate_4(),
	DatabaseLoadAbstractBehaviour_t1458632096::get_offset_of_mExternalDatasetRoots_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (RectangleData_t934532407)+ sizeof (Il2CppObject), sizeof(RectangleData_t934532407_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2042[4] = 
{
	RectangleData_t934532407::get_offset_of_leftTopX_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleData_t934532407::get_offset_of_leftTopY_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleData_t934532407::get_offset_of_rightBottomX_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleData_t934532407::get_offset_of_rightBottomY_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (RectangleIntData_t2869769236)+ sizeof (Il2CppObject), sizeof(RectangleIntData_t2869769236_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2043[4] = 
{
	RectangleIntData_t2869769236::get_offset_of_leftTopX_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleIntData_t2869769236::get_offset_of_leftTopY_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleIntData_t2869769236::get_offset_of_rightBottomX_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleIntData_t2869769236::get_offset_of_rightBottomY_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (OrientedBoundingBox_t3172429123)+ sizeof (Il2CppObject), sizeof(OrientedBoundingBox_t3172429123_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2044[3] = 
{
	OrientedBoundingBox_t3172429123::get_offset_of_U3CCenterU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OrientedBoundingBox_t3172429123::get_offset_of_U3CHalfExtentsU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OrientedBoundingBox_t3172429123::get_offset_of_U3CRotationU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (OrientedBoundingBox3D_t1484796416)+ sizeof (Il2CppObject), sizeof(OrientedBoundingBox3D_t1484796416_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2045[3] = 
{
	OrientedBoundingBox3D_t1484796416::get_offset_of_U3CCenterU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OrientedBoundingBox3D_t1484796416::get_offset_of_U3CHalfExtentsU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OrientedBoundingBox3D_t1484796416::get_offset_of_U3CRotationYU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (BehaviourComponentFactory_t3267823770), -1, sizeof(BehaviourComponentFactory_t3267823770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2048[1] = 
{
	BehaviourComponentFactory_t3267823770_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (NullBehaviourComponentFactory_t3849641556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (CloudRecoImageTargetImpl_t2427143318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2052[1] = 
{
	CloudRecoImageTargetImpl_t2427143318::get_offset_of_mSize_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (CylinderTargetImpl_t259286421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2053[3] = 
{
	CylinderTargetImpl_t259286421::get_offset_of_mSideLength_4(),
	CylinderTargetImpl_t259286421::get_offset_of_mTopDiameter_5(),
	CylinderTargetImpl_t259286421::get_offset_of_mBottomDiameter_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (ImageTargetType_t3906864138)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2055[4] = 
{
	ImageTargetType_t3906864138::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (ImageTargetData_t1326050618)+ sizeof (Il2CppObject), sizeof(ImageTargetData_t1326050618_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2056[2] = 
{
	ImageTargetData_t1326050618::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageTargetData_t1326050618::get_offset_of_size_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (ImageTargetBuilder_t518883741), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (FrameQuality_t2061101710)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2058[5] = 
{
	FrameQuality_t2061101710::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (CameraDeviceImpl_t610967511), -1, sizeof(CameraDeviceImpl_t610967511_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2059[12] = 
{
	CameraDeviceImpl_t610967511::get_offset_of_mCameraImages_1(),
	CameraDeviceImpl_t610967511::get_offset_of_mForcedCameraFormats_2(),
	CameraDeviceImpl_t610967511_StaticFields::get_offset_of_mWebCam_3(),
	CameraDeviceImpl_t610967511::get_offset_of_mCameraReady_4(),
	CameraDeviceImpl_t610967511::get_offset_of_mIsDirty_5(),
	CameraDeviceImpl_t610967511::get_offset_of_mActualCameraDirection_6(),
	CameraDeviceImpl_t610967511::get_offset_of_mSelectedCameraDirection_7(),
	CameraDeviceImpl_t610967511::get_offset_of_mCameraDeviceMode_8(),
	CameraDeviceImpl_t610967511::get_offset_of_mVideoModeData_9(),
	CameraDeviceImpl_t610967511::get_offset_of_mVideoModeDataNeedsUpdate_10(),
	CameraDeviceImpl_t610967511::get_offset_of_mHasCameraDeviceModeBeenSet_11(),
	CameraDeviceImpl_t610967511::get_offset_of_mCameraActive_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (DataSetImpl_t2819025280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2060[4] = 
{
	DataSetImpl_t2819025280::get_offset_of_mDataSetPtr_0(),
	DataSetImpl_t2819025280::get_offset_of_mPath_1(),
	DataSetImpl_t2819025280::get_offset_of_mStorageType_2(),
	DataSetImpl_t2819025280::get_offset_of_mTrackablesDict_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (WordTemplateMode_t1097144495)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2067[3] = 
{
	WordTemplateMode_t1097144495::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (Image_t1391689025), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (PIXEL_FORMAT_t3010530044)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2070[7] = 
{
	PIXEL_FORMAT_t3010530044::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (ImageImpl_t2564717533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2071[10] = 
{
	ImageImpl_t2564717533::get_offset_of_mWidth_0(),
	ImageImpl_t2564717533::get_offset_of_mHeight_1(),
	ImageImpl_t2564717533::get_offset_of_mStride_2(),
	ImageImpl_t2564717533::get_offset_of_mBufferWidth_3(),
	ImageImpl_t2564717533::get_offset_of_mBufferHeight_4(),
	ImageImpl_t2564717533::get_offset_of_mPixelFormat_5(),
	ImageImpl_t2564717533::get_offset_of_mData_6(),
	ImageImpl_t2564717533::get_offset_of_mUnmanagedData_7(),
	ImageImpl_t2564717533::get_offset_of_mDataSet_8(),
	ImageImpl_t2564717533::get_offset_of_mPixel32_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (ImageTargetBuilderImpl_t2457446201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2072[2] = 
{
	ImageTargetBuilderImpl_t2457446201::get_offset_of_mTrackableSource_0(),
	ImageTargetBuilderImpl_t2457446201::get_offset_of_mIsScanning_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (ImageTargetImpl_t869063580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2073[2] = 
{
	ImageTargetImpl_t869063580::get_offset_of_mImageTargetType_4(),
	ImageTargetImpl_t869063580::get_offset_of_mVirtualButtons_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (ObjectTracker_t1568044035), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (ObjectTrackerImpl_t1691118791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2075[4] = 
{
	ObjectTrackerImpl_t1691118791::get_offset_of_mActiveDataSets_1(),
	ObjectTrackerImpl_t1691118791::get_offset_of_mDataSets_2(),
	ObjectTrackerImpl_t1691118791::get_offset_of_mImageTargetBuilder_3(),
	ObjectTrackerImpl_t1691118791::get_offset_of_mTargetFinder_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (MarkerImpl_t2101172390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2078[2] = 
{
	MarkerImpl_t2101172390::get_offset_of_mSize_2(),
	MarkerImpl_t2101172390::get_offset_of_U3CMarkerIDU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (MarkerTracker_t2959118800), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (MarkerTrackerImpl_t654952814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2080[1] = 
{
	MarkerTrackerImpl_t654952814::get_offset_of_mMarkerDict_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (MultiTargetImpl_t2151485576), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (WebCamTexAdaptor_t2106169489), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (NullWebCamTexAdaptor_t2136158928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2084[5] = 
{
	0,
	NullWebCamTexAdaptor_t2136158928::get_offset_of_mTexture_1(),
	NullWebCamTexAdaptor_t2136158928::get_offset_of_mPseudoPlaying_2(),
	NullWebCamTexAdaptor_t2136158928::get_offset_of_mMsBetweenFrames_3(),
	NullWebCamTexAdaptor_t2136158928::get_offset_of_mLastFrame_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (PlayModeEditorUtility_t1237840826), -1, sizeof(PlayModeEditorUtility_t1237840826_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2085[1] = 
{
	PlayModeEditorUtility_t1237840826_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (NullPlayModeEditorUtility_t1550651348), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (PremiumObjectFactory_t787152098), -1, sizeof(PremiumObjectFactory_t787152098_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2088[1] = 
{
	PremiumObjectFactory_t787152098_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (NullPremiumObjectFactory_t2032979060), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (VuforiaManager_t2424874861), -1, sizeof(VuforiaManager_t2424874861_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2090[1] = 
{
	VuforiaManager_t2424874861_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (VuforiaManagerImpl_t3289840897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2091[23] = 
{
	VuforiaManagerImpl_t3289840897::get_offset_of_mWorldCenterMode_1(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mWorldCenter_2(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mARCameraTransform_3(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mCentralAnchorPoint_4(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mParentAnchorPoint_5(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mTrackableResultDataArray_6(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mWordDataArray_7(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mWordResultDataArray_8(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mTrackableFoundQueue_9(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mImageHeaderData_10(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mNumImageHeaders_11(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mInjectedFrameIdx_12(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mLastProcessedFrameStatePtr_13(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mInitialized_14(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mPaused_15(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mFrameState_16(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mAutoRotationState_17(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mVideoBackgroundNeedsRedrawing_18(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mDiscardStatesForRendering_19(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mVideoBackgroundMgr_20(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mLastFrameIdx_21(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mIsSeeThroughDevice_22(),
	VuforiaManagerImpl_t3289840897::get_offset_of_U3CVideoBackgroundTextureSetU3Ek__BackingField_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (PoseData_t1264148721)+ sizeof (Il2CppObject), sizeof(PoseData_t1264148721_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2092[3] = 
{
	PoseData_t1264148721::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PoseData_t1264148721::get_offset_of_orientation_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PoseData_t1264148721::get_offset_of_unused_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (TrackableResultData_t1947527974)+ sizeof (Il2CppObject), sizeof(TrackableResultData_t1947527974_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2093[4] = 
{
	TrackableResultData_t1947527974::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackableResultData_t1947527974::get_offset_of_timeStamp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackableResultData_t1947527974::get_offset_of_status_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackableResultData_t1947527974::get_offset_of_id_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (VirtualButtonData_t1223885651)+ sizeof (Il2CppObject), sizeof(VirtualButtonData_t1223885651_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2094[2] = 
{
	VirtualButtonData_t1223885651::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VirtualButtonData_t1223885651::get_offset_of_isPressed_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (Obb2D_t3491121689)+ sizeof (Il2CppObject), sizeof(Obb2D_t3491121689_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2095[4] = 
{
	Obb2D_t3491121689::get_offset_of_center_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb2D_t3491121689::get_offset_of_halfExtents_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb2D_t3491121689::get_offset_of_rotation_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb2D_t3491121689::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (Obb3D_t3491121690)+ sizeof (Il2CppObject), sizeof(Obb3D_t3491121690_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2096[4] = 
{
	Obb3D_t3491121690::get_offset_of_center_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb3D_t3491121690::get_offset_of_halfExtents_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb3D_t3491121690::get_offset_of_rotationZ_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb3D_t3491121690::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (WordResultData_t3742236631)+ sizeof (Il2CppObject), sizeof(WordResultData_t3742236631_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2097[5] = 
{
	WordResultData_t3742236631::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordResultData_t3742236631::get_offset_of_timeStamp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordResultData_t3742236631::get_offset_of_status_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordResultData_t3742236631::get_offset_of_id_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordResultData_t3742236631::get_offset_of_orientedBoundingBox_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (WordData_t1273964084)+ sizeof (Il2CppObject), sizeof(WordData_t1273964084_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2098[4] = 
{
	WordData_t1273964084::get_offset_of_stringValue_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordData_t1273964084::get_offset_of_id_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordData_t1273964084::get_offset_of_size_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordData_t1273964084::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (ImageHeaderData_t3150040852)+ sizeof (Il2CppObject), sizeof(ImageHeaderData_t3150040852_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2099[9] = 
{
	ImageHeaderData_t3150040852::get_offset_of_data_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t3150040852::get_offset_of_width_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t3150040852::get_offset_of_height_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t3150040852::get_offset_of_stride_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t3150040852::get_offset_of_bufferWidth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t3150040852::get_offset_of_bufferHeight_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t3150040852::get_offset_of_format_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t3150040852::get_offset_of_reallocate_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t3150040852::get_offset_of_updated_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
