﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DataService
struct DataService_t2602786891;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestScript
struct  TestScript_t905662927  : public MonoBehaviour_t1158329972
{
public:
	// DataService TestScript::ds
	DataService_t2602786891 * ___ds_2;

public:
	inline static int32_t get_offset_of_ds_2() { return static_cast<int32_t>(offsetof(TestScript_t905662927, ___ds_2)); }
	inline DataService_t2602786891 * get_ds_2() const { return ___ds_2; }
	inline DataService_t2602786891 ** get_address_of_ds_2() { return &___ds_2; }
	inline void set_ds_2(DataService_t2602786891 * value)
	{
		___ds_2 = value;
		Il2CppCodeGenWriteBarrier(&___ds_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
