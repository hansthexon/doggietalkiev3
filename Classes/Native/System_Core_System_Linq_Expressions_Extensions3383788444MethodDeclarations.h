﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Type
struct Type_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"

// System.Boolean System.Linq.Expressions.Extensions::IsGenericInstanceOf(System.Type,System.Type)
extern "C"  bool Extensions_IsGenericInstanceOf_m3746677300 (Il2CppObject * __this /* static, unused */, Type_t * ___self0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Linq.Expressions.Extensions::IsNullable(System.Type)
extern "C"  bool Extensions_IsNullable_m3418075989 (Il2CppObject * __this /* static, unused */, Type_t * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Linq.Expressions.Extensions::IsExpression(System.Type)
extern "C"  bool Extensions_IsExpression_m2224297168 (Il2CppObject * __this /* static, unused */, Type_t * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Linq.Expressions.Extensions::IsAssignableTo(System.Type,System.Type)
extern "C"  bool Extensions_IsAssignableTo_m2181404043 (Il2CppObject * __this /* static, unused */, Type_t * ___self0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Linq.Expressions.Extensions::GetFirstGenericArgument(System.Type)
extern "C"  Type_t * Extensions_GetFirstGenericArgument_m1178456334 (Il2CppObject * __this /* static, unused */, Type_t * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Linq.Expressions.Extensions::MakeNullableType(System.Type)
extern "C"  Type_t * Extensions_MakeNullableType_m3524904357 (Il2CppObject * __this /* static, unused */, Type_t * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Linq.Expressions.Extensions::GetNotNullableType(System.Type)
extern "C"  Type_t * Extensions_GetNotNullableType_m834342662 (Il2CppObject * __this /* static, unused */, Type_t * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Linq.Expressions.Extensions::GetInvokeMethod(System.Type)
extern "C"  MethodInfo_t * Extensions_GetInvokeMethod_m2119565327 (Il2CppObject * __this /* static, unused */, Type_t * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Linq.Expressions.Extensions::ArrayTypeIsAssignableTo(System.Type,System.Type)
extern "C"  bool Extensions_ArrayTypeIsAssignableTo_m2990592902 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, Type_t * ___candidate1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
