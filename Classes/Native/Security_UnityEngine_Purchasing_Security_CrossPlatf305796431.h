﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Purchasing.Security.GooglePlayValidator
struct GooglePlayValidator_t4061171767;
// UnityEngine.Purchasing.Security.AppleValidator
struct AppleValidator_t3837389912;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.CrossPlatformValidator
struct  CrossPlatformValidator_t305796431  : public Il2CppObject
{
public:
	// UnityEngine.Purchasing.Security.GooglePlayValidator UnityEngine.Purchasing.Security.CrossPlatformValidator::google
	GooglePlayValidator_t4061171767 * ___google_0;
	// UnityEngine.Purchasing.Security.AppleValidator UnityEngine.Purchasing.Security.CrossPlatformValidator::apple
	AppleValidator_t3837389912 * ___apple_1;
	// System.String UnityEngine.Purchasing.Security.CrossPlatformValidator::googleBundleId
	String_t* ___googleBundleId_2;
	// System.String UnityEngine.Purchasing.Security.CrossPlatformValidator::appleBundleId
	String_t* ___appleBundleId_3;

public:
	inline static int32_t get_offset_of_google_0() { return static_cast<int32_t>(offsetof(CrossPlatformValidator_t305796431, ___google_0)); }
	inline GooglePlayValidator_t4061171767 * get_google_0() const { return ___google_0; }
	inline GooglePlayValidator_t4061171767 ** get_address_of_google_0() { return &___google_0; }
	inline void set_google_0(GooglePlayValidator_t4061171767 * value)
	{
		___google_0 = value;
		Il2CppCodeGenWriteBarrier(&___google_0, value);
	}

	inline static int32_t get_offset_of_apple_1() { return static_cast<int32_t>(offsetof(CrossPlatformValidator_t305796431, ___apple_1)); }
	inline AppleValidator_t3837389912 * get_apple_1() const { return ___apple_1; }
	inline AppleValidator_t3837389912 ** get_address_of_apple_1() { return &___apple_1; }
	inline void set_apple_1(AppleValidator_t3837389912 * value)
	{
		___apple_1 = value;
		Il2CppCodeGenWriteBarrier(&___apple_1, value);
	}

	inline static int32_t get_offset_of_googleBundleId_2() { return static_cast<int32_t>(offsetof(CrossPlatformValidator_t305796431, ___googleBundleId_2)); }
	inline String_t* get_googleBundleId_2() const { return ___googleBundleId_2; }
	inline String_t** get_address_of_googleBundleId_2() { return &___googleBundleId_2; }
	inline void set_googleBundleId_2(String_t* value)
	{
		___googleBundleId_2 = value;
		Il2CppCodeGenWriteBarrier(&___googleBundleId_2, value);
	}

	inline static int32_t get_offset_of_appleBundleId_3() { return static_cast<int32_t>(offsetof(CrossPlatformValidator_t305796431, ___appleBundleId_3)); }
	inline String_t* get_appleBundleId_3() const { return ___appleBundleId_3; }
	inline String_t** get_address_of_appleBundleId_3() { return &___appleBundleId_3; }
	inline void set_appleBundleId_3(String_t* value)
	{
		___appleBundleId_3 = value;
		Il2CppCodeGenWriteBarrier(&___appleBundleId_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
