﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.ExpressionVisitor
struct ExpressionVisitor_t1427178562;
// System.Linq.Expressions.Expression
struct Expression_t114864668;
// System.Linq.Expressions.MemberBinding
struct MemberBinding_t3487798541;
// System.Linq.Expressions.ElementInit
struct ElementInit_t3898206436;
// System.Linq.Expressions.UnaryExpression
struct UnaryExpression_t4253534555;
// System.Linq.Expressions.BinaryExpression
struct BinaryExpression_t2159924157;
// System.Linq.Expressions.TypeBinaryExpression
struct TypeBinaryExpression_t4211182725;
// System.Linq.Expressions.ConstantExpression
struct ConstantExpression_t305952364;
// System.Linq.Expressions.ConditionalExpression
struct ConditionalExpression_t4183435840;
// System.Linq.Expressions.ParameterExpression
struct ParameterExpression_t3015504955;
// System.Linq.Expressions.MemberExpression
struct MemberExpression_t1790982958;
// System.Linq.Expressions.MethodCallExpression
struct MethodCallExpression_t3367820543;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.Expression>
struct ReadOnlyCollection_1_t300650360;
// System.Linq.Expressions.MemberAssignment
struct MemberAssignment_t3987033531;
// System.Linq.Expressions.MemberMemberBinding
struct MemberMemberBinding_t950649153;
// System.Linq.Expressions.MemberListBinding
struct MemberListBinding_t1321006879;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.MemberBinding>
struct ReadOnlyCollection_1_t3673584233;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.ElementInit>
struct ReadOnlyCollection_1_t4083992128;
// System.Linq.Expressions.LambdaExpression
struct LambdaExpression_t2811402413;
// System.Linq.Expressions.NewExpression
struct NewExpression_t1045017810;
// System.Linq.Expressions.MemberInitExpression
struct MemberInitExpression_t137172540;
// System.Linq.Expressions.ListInitExpression
struct ListInitExpression_t1376237998;
// System.Linq.Expressions.NewArrayExpression
struct NewArrayExpression_t2420949259;
// System.Linq.Expressions.InvocationExpression
struct InvocationExpression_t267914806;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Linq_Expressions_Expression114864668.h"
#include "System_Core_System_Linq_Expressions_MemberBinding3487798541.h"
#include "System_Core_System_Linq_Expressions_ElementInit3898206436.h"
#include "System_Core_System_Linq_Expressions_UnaryExpressio4253534555.h"
#include "System_Core_System_Linq_Expressions_BinaryExpressi2159924157.h"
#include "System_Core_System_Linq_Expressions_TypeBinaryExpr4211182725.h"
#include "System_Core_System_Linq_Expressions_ConstantExpress305952364.h"
#include "System_Core_System_Linq_Expressions_ConditionalExp4183435840.h"
#include "System_Core_System_Linq_Expressions_ParameterExpre3015504955.h"
#include "System_Core_System_Linq_Expressions_MemberExpressi1790982958.h"
#include "System_Core_System_Linq_Expressions_MethodCallExpr3367820543.h"
#include "System_Core_System_Linq_Expressions_MemberAssignme3987033531.h"
#include "System_Core_System_Linq_Expressions_MemberMemberBin950649153.h"
#include "System_Core_System_Linq_Expressions_MemberListBind1321006879.h"
#include "System_Core_System_Linq_Expressions_LambdaExpressi2811402413.h"
#include "System_Core_System_Linq_Expressions_NewExpression1045017810.h"
#include "System_Core_System_Linq_Expressions_MemberInitExpre137172540.h"
#include "System_Core_System_Linq_Expressions_ListInitExpres1376237998.h"
#include "System_Core_System_Linq_Expressions_NewArrayExpres2420949259.h"
#include "System_Core_System_Linq_Expressions_InvocationExpre267914806.h"

// System.Void System.Linq.Expressions.ExpressionVisitor::.ctor()
extern "C"  void ExpressionVisitor__ctor_m1566168689 (ExpressionVisitor_t1427178562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::Visit(System.Linq.Expressions.Expression)
extern "C"  void ExpressionVisitor_Visit_m160944764 (ExpressionVisitor_t1427178562 * __this, Expression_t114864668 * ___expression0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitBinding(System.Linq.Expressions.MemberBinding)
extern "C"  void ExpressionVisitor_VisitBinding_m924763700 (ExpressionVisitor_t1427178562 * __this, MemberBinding_t3487798541 * ___binding0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitElementInitializer(System.Linq.Expressions.ElementInit)
extern "C"  void ExpressionVisitor_VisitElementInitializer_m4194448586 (ExpressionVisitor_t1427178562 * __this, ElementInit_t3898206436 * ___initializer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitUnary(System.Linq.Expressions.UnaryExpression)
extern "C"  void ExpressionVisitor_VisitUnary_m1751186732 (ExpressionVisitor_t1427178562 * __this, UnaryExpression_t4253534555 * ___unary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitBinary(System.Linq.Expressions.BinaryExpression)
extern "C"  void ExpressionVisitor_VisitBinary_m3233127806 (ExpressionVisitor_t1427178562 * __this, BinaryExpression_t2159924157 * ___binary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitTypeIs(System.Linq.Expressions.TypeBinaryExpression)
extern "C"  void ExpressionVisitor_VisitTypeIs_m3158624987 (ExpressionVisitor_t1427178562 * __this, TypeBinaryExpression_t4211182725 * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitConstant(System.Linq.Expressions.ConstantExpression)
extern "C"  void ExpressionVisitor_VisitConstant_m1583676260 (ExpressionVisitor_t1427178562 * __this, ConstantExpression_t305952364 * ___constant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitConditional(System.Linq.Expressions.ConditionalExpression)
extern "C"  void ExpressionVisitor_VisitConditional_m2130412812 (ExpressionVisitor_t1427178562 * __this, ConditionalExpression_t4183435840 * ___conditional0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitParameter(System.Linq.Expressions.ParameterExpression)
extern "C"  void ExpressionVisitor_VisitParameter_m659481516 (ExpressionVisitor_t1427178562 * __this, ParameterExpression_t3015504955 * ___parameter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitMemberAccess(System.Linq.Expressions.MemberExpression)
extern "C"  void ExpressionVisitor_VisitMemberAccess_m1550792650 (ExpressionVisitor_t1427178562 * __this, MemberExpression_t1790982958 * ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitMethodCall(System.Linq.Expressions.MethodCallExpression)
extern "C"  void ExpressionVisitor_VisitMethodCall_m3971188470 (ExpressionVisitor_t1427178562 * __this, MethodCallExpression_t3367820543 * ___methodCall0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitExpressionList(System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.Expression>)
extern "C"  void ExpressionVisitor_VisitExpressionList_m1954462001 (ExpressionVisitor_t1427178562 * __this, ReadOnlyCollection_1_t300650360 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitMemberAssignment(System.Linq.Expressions.MemberAssignment)
extern "C"  void ExpressionVisitor_VisitMemberAssignment_m214503088 (ExpressionVisitor_t1427178562 * __this, MemberAssignment_t3987033531 * ___assignment0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitMemberMemberBinding(System.Linq.Expressions.MemberMemberBinding)
extern "C"  void ExpressionVisitor_VisitMemberMemberBinding_m209981274 (ExpressionVisitor_t1427178562 * __this, MemberMemberBinding_t950649153 * ___binding0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitMemberListBinding(System.Linq.Expressions.MemberListBinding)
extern "C"  void ExpressionVisitor_VisitMemberListBinding_m818569850 (ExpressionVisitor_t1427178562 * __this, MemberListBinding_t1321006879 * ___binding0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitBindingList(System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.MemberBinding>)
extern "C"  void ExpressionVisitor_VisitBindingList_m1381130009 (ExpressionVisitor_t1427178562 * __this, ReadOnlyCollection_1_t3673584233 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitElementInitializerList(System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.ElementInit>)
extern "C"  void ExpressionVisitor_VisitElementInitializerList_m727589273 (ExpressionVisitor_t1427178562 * __this, ReadOnlyCollection_1_t4083992128 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitLambda(System.Linq.Expressions.LambdaExpression)
extern "C"  void ExpressionVisitor_VisitLambda_m1672227406 (ExpressionVisitor_t1427178562 * __this, LambdaExpression_t2811402413 * ___lambda0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitNew(System.Linq.Expressions.NewExpression)
extern "C"  void ExpressionVisitor_VisitNew_m4110677068 (ExpressionVisitor_t1427178562 * __this, NewExpression_t1045017810 * ___nex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitMemberInit(System.Linq.Expressions.MemberInitExpression)
extern "C"  void ExpressionVisitor_VisitMemberInit_m4124552884 (ExpressionVisitor_t1427178562 * __this, MemberInitExpression_t137172540 * ___init0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitListInit(System.Linq.Expressions.ListInitExpression)
extern "C"  void ExpressionVisitor_VisitListInit_m2590643596 (ExpressionVisitor_t1427178562 * __this, ListInitExpression_t1376237998 * ___init0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitNewArray(System.Linq.Expressions.NewArrayExpression)
extern "C"  void ExpressionVisitor_VisitNewArray_m111395206 (ExpressionVisitor_t1427178562 * __this, NewArrayExpression_t2420949259 * ___newArray0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionVisitor::VisitInvocation(System.Linq.Expressions.InvocationExpression)
extern "C"  void ExpressionVisitor_VisitInvocation_m3621192156 (ExpressionVisitor_t1427178562 * __this, InvocationExpression_t267914806 * ___invocation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
