﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.NotNullConstraintViolationException/<NotNullConstraintViolationException>c__AnonStorey10
struct U3CNotNullConstraintViolationExceptionU3Ec__AnonStorey10_t2464633313;
// SQLite4Unity3d.TableMapping/Column
struct Column_t441055761;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableMapping_Colum441055761.h"

// System.Void SQLite4Unity3d.NotNullConstraintViolationException/<NotNullConstraintViolationException>c__AnonStorey10::.ctor()
extern "C"  void U3CNotNullConstraintViolationExceptionU3Ec__AnonStorey10__ctor_m1492393488 (U3CNotNullConstraintViolationExceptionU3Ec__AnonStorey10_t2464633313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.NotNullConstraintViolationException/<NotNullConstraintViolationException>c__AnonStorey10::<>m__1(SQLite4Unity3d.TableMapping/Column)
extern "C"  bool U3CNotNullConstraintViolationExceptionU3Ec__AnonStorey10_U3CU3Em__1_m3399534553 (U3CNotNullConstraintViolationExceptionU3Ec__AnonStorey10_t2464633313 * __this, Column_t441055761 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
