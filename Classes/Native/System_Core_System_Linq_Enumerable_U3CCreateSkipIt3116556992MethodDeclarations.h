﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>
struct U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::.ctor()
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1__ctor_m2739536835_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1__ctor_m2739536835(__this, method) ((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1__ctor_m2739536835_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1340547920_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1340547920(__this, method) ((  Il2CppObject * (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1340547920_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerator_get_Current_m2607218181_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerator_get_Current_m2607218181(__this, method) ((  Il2CppObject * (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerator_get_Current_m2607218181_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerable_GetEnumerator_m1936281608_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerable_GetEnumerator_m1936281608(__this, method) ((  Il2CppObject * (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerable_GetEnumerator_m1936281608_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m4074570415_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m4074570415(__this, method) ((  Il2CppObject* (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m4074570415_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::MoveNext()
extern "C"  bool U3CCreateSkipIteratorU3Ec__Iterator16_1_MoveNext_m2317883885_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_MoveNext_m2317883885(__this, method) ((  bool (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_MoveNext_m2317883885_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::Dispose()
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1_Dispose_m3750295900_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_Dispose_m3750295900(__this, method) ((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_Dispose_m3750295900_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::Reset()
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1_Reset_m3328507806_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_Reset_m3328507806(__this, method) ((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_Reset_m3328507806_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::<>__Finally0()
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1_U3CU3E__Finally0_m1059667724_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_U3CU3E__Finally0_m1059667724(__this, method) ((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t3116556992 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_U3CU3E__Finally0_m1059667724_gshared)(__this, method)
