﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.GooglePlayTangle
struct GooglePlayTangle_t2749524914;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.Security.GooglePlayTangle::.ctor()
extern "C"  void GooglePlayTangle__ctor_m1761951308 (GooglePlayTangle_t2749524914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.GooglePlayTangle::.cctor()
extern "C"  void GooglePlayTangle__cctor_m4030200525 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.Purchasing.Security.GooglePlayTangle::Data()
extern "C"  ByteU5BU5D_t3397334013* GooglePlayTangle_Data_m2557246410 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
