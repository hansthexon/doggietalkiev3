﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Checkandload/<Download>c__Iterator5
struct U3CDownloadU3Ec__Iterator5_t3479011688;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Checkandload/<Download>c__Iterator5::.ctor()
extern "C"  void U3CDownloadU3Ec__Iterator5__ctor_m126658537 (U3CDownloadU3Ec__Iterator5_t3479011688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Checkandload/<Download>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDownloadU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3815818691 (U3CDownloadU3Ec__Iterator5_t3479011688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Checkandload/<Download>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDownloadU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m2212000923 (U3CDownloadU3Ec__Iterator5_t3479011688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Checkandload/<Download>c__Iterator5::MoveNext()
extern "C"  bool U3CDownloadU3Ec__Iterator5_MoveNext_m3854227583 (U3CDownloadU3Ec__Iterator5_t3479011688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Checkandload/<Download>c__Iterator5::Dispose()
extern "C"  void U3CDownloadU3Ec__Iterator5_Dispose_m4214424442 (U3CDownloadU3Ec__Iterator5_t3479011688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Checkandload/<Download>c__Iterator5::Reset()
extern "C"  void U3CDownloadU3Ec__Iterator5_Reset_m144468300 (U3CDownloadU3Ec__Iterator5_t3479011688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
