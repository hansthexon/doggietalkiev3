﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.SQLiteConnection/ColumnInfo
struct ColumnInfo_t4209372729;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SQLite4Unity3d.SQLiteConnection/ColumnInfo::.ctor()
extern "C"  void ColumnInfo__ctor_m1117317126 (ColumnInfo_t4209372729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.SQLiteConnection/ColumnInfo::get_Name()
extern "C"  String_t* ColumnInfo_get_Name_m3660298655 (ColumnInfo_t4209372729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection/ColumnInfo::set_Name(System.String)
extern "C"  void ColumnInfo_set_Name_m1281920292 (ColumnInfo_t4209372729 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection/ColumnInfo::get_notnull()
extern "C"  int32_t ColumnInfo_get_notnull_m2532701341 (ColumnInfo_t4209372729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection/ColumnInfo::set_notnull(System.Int32)
extern "C"  void ColumnInfo_set_notnull_m1730222528 (ColumnInfo_t4209372729 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.SQLiteConnection/ColumnInfo::ToString()
extern "C"  String_t* ColumnInfo_ToString_m871344587 (ColumnInfo_t4209372729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
