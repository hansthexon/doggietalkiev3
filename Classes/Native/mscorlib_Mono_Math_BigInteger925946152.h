﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2510243513;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.BigInteger
struct  BigInteger_t925946152  : public Il2CppObject
{
public:
	// System.UInt32 Mono.Math.BigInteger::length
	uint32_t ___length_2;
	// System.UInt32[] Mono.Math.BigInteger::data
	UInt32U5BU5D_t59386216* ___data_3;

public:
	inline static int32_t get_offset_of_length_2() { return static_cast<int32_t>(offsetof(BigInteger_t925946152, ___length_2)); }
	inline uint32_t get_length_2() const { return ___length_2; }
	inline uint32_t* get_address_of_length_2() { return &___length_2; }
	inline void set_length_2(uint32_t value)
	{
		___length_2 = value;
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(BigInteger_t925946152, ___data_3)); }
	inline UInt32U5BU5D_t59386216* get_data_3() const { return ___data_3; }
	inline UInt32U5BU5D_t59386216** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(UInt32U5BU5D_t59386216* value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier(&___data_3, value);
	}
};

struct BigInteger_t925946152_StaticFields
{
public:
	// System.UInt32[] Mono.Math.BigInteger::smallPrimes
	UInt32U5BU5D_t59386216* ___smallPrimes_4;
	// System.Security.Cryptography.RandomNumberGenerator Mono.Math.BigInteger::rng
	RandomNumberGenerator_t2510243513 * ___rng_5;

public:
	inline static int32_t get_offset_of_smallPrimes_4() { return static_cast<int32_t>(offsetof(BigInteger_t925946152_StaticFields, ___smallPrimes_4)); }
	inline UInt32U5BU5D_t59386216* get_smallPrimes_4() const { return ___smallPrimes_4; }
	inline UInt32U5BU5D_t59386216** get_address_of_smallPrimes_4() { return &___smallPrimes_4; }
	inline void set_smallPrimes_4(UInt32U5BU5D_t59386216* value)
	{
		___smallPrimes_4 = value;
		Il2CppCodeGenWriteBarrier(&___smallPrimes_4, value);
	}

	inline static int32_t get_offset_of_rng_5() { return static_cast<int32_t>(offsetof(BigInteger_t925946152_StaticFields, ___rng_5)); }
	inline RandomNumberGenerator_t2510243513 * get_rng_5() const { return ___rng_5; }
	inline RandomNumberGenerator_t2510243513 ** get_address_of_rng_5() { return &___rng_5; }
	inline void set_rng_5(RandomNumberGenerator_t2510243513 * value)
	{
		___rng_5 = value;
		Il2CppCodeGenWriteBarrier(&___rng_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
