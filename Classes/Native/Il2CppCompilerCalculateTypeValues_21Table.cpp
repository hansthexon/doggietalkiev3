﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1863273727.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1453372369.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl2968608989.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl4033919773.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3635099966.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3090691518.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl2337328204.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer2933102835.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Fp1598668988.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vi4106934884.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vi4137084396.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vec829768013.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vi2617831468.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Ren804170727.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRendererImp3510735303.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRendererImp4076072164.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnityImpl149264205.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackabl32254201.h"
#include "Vuforia_UnityExtensions_Vuforia_SurfaceImpl3646117491.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilder447373045.h"
#include "Vuforia_UnityExtensions_Vuforia_PropImpl1187075139.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracke1462833936.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracke2474837022.h"
#include "Vuforia_UnityExtensions_Vuforia_TextTracker89845299.h"
#include "Vuforia_UnityExtensions_Vuforia_TextTrackerImpl211382711.h"
#include "Vuforia_UnityExtensions_Vuforia_TextTrackerImpl_UpD944577254.h"
#include "Vuforia_UnityExtensions_Vuforia_TypeMapping254417876.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptorIm1817875757.h"
#include "Vuforia_UnityExtensions_Vuforia_WordImpl1843145168.h"
#include "Vuforia_UnityExtensions_Vuforia_WordPrefabCreation3171836134.h"
#include "Vuforia_UnityExtensions_Vuforia_WordManager1585193471.h"
#include "Vuforia_UnityExtensions_Vuforia_WordManagerImpl4282786523.h"
#include "Vuforia_UnityExtensions_Vuforia_WordResult1915507197.h"
#include "Vuforia_UnityExtensions_Vuforia_WordResultImpl911273601.h"
#include "Vuforia_UnityExtensions_Vuforia_WordList1278495262.h"
#include "Vuforia_UnityExtensions_Vuforia_WordListImpl2150426444.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaNativeIosWr1210651633.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaNullWrapper3644069544.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaNativeWrapp2645113514.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaWrapper3750170617.h"
#include "Vuforia_UnityExtensions_Vuforia_KeepAliveAbstractB1486411203.h"
#include "Vuforia_UnityExtensions_Vuforia_ReconstructionAbst3509595417.h"
#include "Vuforia_UnityExtensions_Vuforia_PropAbstractBehavi1047177596.h"
#include "Vuforia_UnityExtensions_Vuforia_StateManager3369465942.h"
#include "Vuforia_UnityExtensions_Vuforia_StateManagerImpl3885489748.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder1347637805.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_InitStat4409649.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Updat1473252352.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Filte3082493643.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Targe1958726506.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinderImpl1380851697.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinderImpl_T3807887646.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinderImpl_I2369108641.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableSource2832298792.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableSourceImp2574642394.h"
#include "Vuforia_UnityExtensions_Vuforia_TextureRenderer3312477626.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackerManager308318605.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackerManagerImpl381223961.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButton3703236737.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButton_Sens1678924861.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButtonImpl2449737797.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamImpl2771725761.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile3757625748.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof1724666488.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof3644865120.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetAbstrac3327552701.h"
#include "Vuforia_UnityExtensions_Vuforia_MarkerAbstractBeha1456101953.h"
#include "Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBeh3489038957.h"
#include "Vuforia_UnityExtensions_Vuforia_MultiTargetAbstrac3616801211.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity657456673.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitE2149396216.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_Vufor3491240575.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_Stora3897282321.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeh3319870759.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeh3132552034.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaMacros1884408435.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtil3083157244.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtil1916387570.h"
#include "Vuforia_UnityExtensions_Vuforia_SurfaceUtilities4096327849.h"
#include "Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBe2386081773.h"
#include "Vuforia_UnityExtensions_Vuforia_SimpleTargetData3993525265.h"
#include "Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeh4084926705.h"
#include "Vuforia_UnityExtensions_Vuforia_UserDefinedTargetB3589690572.h"
#include "Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbst395384314.h"
#include "Vuforia_UnityExtensions_Vuforia_VideoBackgroundMan3765780423.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstr2478279366.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamAbstractBeha4104806356.h"
#include "Vuforia_UnityExtensions_Vuforia_WordAbstractBehavi2878458725.h"
#include "Vuforia_UnityExtensions_Vuforia_WordFilterMode695600879.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (MeshData_t1863273727)+ sizeof (Il2CppObject), sizeof(MeshData_t1863273727_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2100[8] = 
{
	MeshData_t1863273727::get_offset_of_positionsArray_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1863273727::get_offset_of_normalsArray_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1863273727::get_offset_of_texCoordsArray_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1863273727::get_offset_of_triangleIdxArray_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1863273727::get_offset_of_numVertexValues_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1863273727::get_offset_of_hasNormals_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1863273727::get_offset_of_hasTexCoords_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1863273727::get_offset_of_numTriangleIndices_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (SmartTerrainRevisionData_t1453372369)+ sizeof (Il2CppObject), sizeof(SmartTerrainRevisionData_t1453372369_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2101[2] = 
{
	SmartTerrainRevisionData_t1453372369::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SmartTerrainRevisionData_t1453372369::get_offset_of_revision_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (SurfaceData_t2968608989)+ sizeof (Il2CppObject), sizeof(SurfaceData_t2968608989_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2102[9] = 
{
	SurfaceData_t2968608989::get_offset_of_meshBoundaryArray_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t2968608989::get_offset_of_meshData_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t2968608989::get_offset_of_navMeshData_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t2968608989::get_offset_of_boundingBox_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t2968608989::get_offset_of_localPose_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t2968608989::get_offset_of_id_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t2968608989::get_offset_of_parentID_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t2968608989::get_offset_of_numBoundaryIndices_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t2968608989::get_offset_of_revision_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (PropData_t4033919773)+ sizeof (Il2CppObject), sizeof(PropData_t4033919773_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2103[8] = 
{
	PropData_t4033919773::get_offset_of_meshData_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t4033919773::get_offset_of_id_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t4033919773::get_offset_of_parentID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t4033919773::get_offset_of_boundingBox_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t4033919773::get_offset_of_localPosition_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t4033919773::get_offset_of_localPose_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t4033919773::get_offset_of_revision_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t4033919773::get_offset_of_unused_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (FrameState_t3635099966)+ sizeof (Il2CppObject), sizeof(FrameState_t3635099966_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2104[18] = 
{
	FrameState_t3635099966::get_offset_of_trackableDataArray_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_vbDataArray_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_wordResultArray_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_newWordDataArray_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_propTrackableDataArray_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_smartTerrainRevisionsArray_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_updatedSurfacesArray_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_updatedPropsArray_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_numTrackableResults_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_numVirtualButtonResults_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_frameIndex_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_numWordResults_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_numNewWords_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_numPropTrackableResults_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_numSmartTerrainRevisions_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_numUpdatedSurfaces_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_numUpdatedProps_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_deviceTrackableId_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (AutoRotationState_t3090691518)+ sizeof (Il2CppObject), sizeof(AutoRotationState_t3090691518_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2105[5] = 
{
	AutoRotationState_t3090691518::get_offset_of_setOnPause_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AutoRotationState_t3090691518::get_offset_of_autorotateToPortrait_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AutoRotationState_t3090691518::get_offset_of_autorotateToPortraitUpsideDown_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AutoRotationState_t3090691518::get_offset_of_autorotateToLandscapeLeft_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AutoRotationState_t3090691518::get_offset_of_autorotateToLandscapeRight_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (U3CU3Ec__DisplayClass3_t2337328204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2106[1] = 
{
	U3CU3Ec__DisplayClass3_t2337328204::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (VuforiaRenderer_t2933102835), -1, sizeof(VuforiaRenderer_t2933102835_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2107[1] = 
{
	VuforiaRenderer_t2933102835_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (FpsHint_t1598668988)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2108[5] = 
{
	FpsHint_t1598668988::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (VideoBackgroundReflection_t4106934884)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2109[4] = 
{
	VideoBackgroundReflection_t4106934884::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (VideoBGCfgData_t4137084396)+ sizeof (Il2CppObject), sizeof(VideoBGCfgData_t4137084396_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2110[4] = 
{
	VideoBGCfgData_t4137084396::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t4137084396::get_offset_of_size_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t4137084396::get_offset_of_enabled_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t4137084396::get_offset_of_reflection_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (Vec2I_t829768013)+ sizeof (Il2CppObject), sizeof(Vec2I_t829768013_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2111[2] = 
{
	Vec2I_t829768013::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vec2I_t829768013::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (VideoTextureInfo_t2617831468)+ sizeof (Il2CppObject), sizeof(VideoTextureInfo_t2617831468_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2112[2] = 
{
	VideoTextureInfo_t2617831468::get_offset_of_textureSize_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoTextureInfo_t2617831468::get_offset_of_imageSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (RendererAPI_t804170727)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2113[3] = 
{
	RendererAPI_t804170727::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (VuforiaRendererImpl_t3510735303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[5] = 
{
	VuforiaRendererImpl_t3510735303::get_offset_of_mVideoBGConfig_1(),
	VuforiaRendererImpl_t3510735303::get_offset_of_mVideoBGConfigSet_2(),
	VuforiaRendererImpl_t3510735303::get_offset_of_mVideoBackgroundTexture_3(),
	VuforiaRendererImpl_t3510735303::get_offset_of_mBackgroundTextureHasChanged_4(),
	VuforiaRendererImpl_t3510735303::get_offset_of_mLastSetReflection_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (RenderEvent_t4076072164)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2115[7] = 
{
	RenderEvent_t4076072164::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (VuforiaUnityImpl_t149264205), -1, sizeof(VuforiaUnityImpl_t149264205_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2116[1] = 
{
	VuforiaUnityImpl_t149264205_StaticFields::get_offset_of_mRendererDirty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (SmartTerrainTrackableImpl_t32254201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2118[5] = 
{
	SmartTerrainTrackableImpl_t32254201::get_offset_of_mChildren_2(),
	SmartTerrainTrackableImpl_t32254201::get_offset_of_mMesh_3(),
	SmartTerrainTrackableImpl_t32254201::get_offset_of_mMeshRevision_4(),
	SmartTerrainTrackableImpl_t32254201::get_offset_of_mLocalPose_5(),
	SmartTerrainTrackableImpl_t32254201::get_offset_of_U3CParentU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (SurfaceImpl_t3646117491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2120[5] = 
{
	SurfaceImpl_t3646117491::get_offset_of_mNavMesh_7(),
	SurfaceImpl_t3646117491::get_offset_of_mMeshBoundaries_8(),
	SurfaceImpl_t3646117491::get_offset_of_mBoundingBox_9(),
	SurfaceImpl_t3646117491::get_offset_of_mSurfaceArea_10(),
	SurfaceImpl_t3646117491::get_offset_of_mAreaNeedsUpdate_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (SmartTerrainBuilderImpl_t447373045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2121[2] = 
{
	SmartTerrainBuilderImpl_t447373045::get_offset_of_mReconstructionBehaviours_0(),
	SmartTerrainBuilderImpl_t447373045::get_offset_of_mIsInitialized_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (PropImpl_t1187075139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2123[1] = 
{
	PropImpl_t1187075139::get_offset_of_mOrientedBoundingBox3D_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (SmartTerrainTracker_t1462833936), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (SmartTerrainTrackerImpl_t2474837022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[2] = 
{
	SmartTerrainTrackerImpl_t2474837022::get_offset_of_mScaleToMillimeter_1(),
	SmartTerrainTrackerImpl_t2474837022::get_offset_of_mSmartTerrainBuilder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (TextTracker_t89845299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (TextTrackerImpl_t211382711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2127[1] = 
{
	TextTrackerImpl_t211382711::get_offset_of_mWordList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (UpDirection_t944577254)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2128[5] = 
{
	UpDirection_t944577254::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (TypeMapping_t254417876), -1, sizeof(TypeMapping_t254417876_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2129[1] = 
{
	TypeMapping_t254417876_StaticFields::get_offset_of_sTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (WebCamTexAdaptorImpl_t1817875757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2130[2] = 
{
	WebCamTexAdaptorImpl_t1817875757::get_offset_of_mWebCamTexture_0(),
	WebCamTexAdaptorImpl_t1817875757::get_offset_of_mCheckCameraPermissions_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (WordImpl_t1843145168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[5] = 
{
	WordImpl_t1843145168::get_offset_of_mText_2(),
	WordImpl_t1843145168::get_offset_of_mSize_3(),
	WordImpl_t1843145168::get_offset_of_mLetterMask_4(),
	WordImpl_t1843145168::get_offset_of_mLetterImageHeader_5(),
	WordImpl_t1843145168::get_offset_of_mLetterBoundingBoxes_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (WordPrefabCreationMode_t3171836134)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2133[3] = 
{
	WordPrefabCreationMode_t3171836134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (WordManager_t1585193471), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (WordManagerImpl_t4282786523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[12] = 
{
	0,
	WordManagerImpl_t4282786523::get_offset_of_mTrackedWords_1(),
	WordManagerImpl_t4282786523::get_offset_of_mNewWords_2(),
	WordManagerImpl_t4282786523::get_offset_of_mLostWords_3(),
	WordManagerImpl_t4282786523::get_offset_of_mActiveWordBehaviours_4(),
	WordManagerImpl_t4282786523::get_offset_of_mWordBehavioursMarkedForDeletion_5(),
	WordManagerImpl_t4282786523::get_offset_of_mWaitingQueue_6(),
	WordManagerImpl_t4282786523::get_offset_of_mWordBehaviours_7(),
	WordManagerImpl_t4282786523::get_offset_of_mAutomaticTemplate_8(),
	WordManagerImpl_t4282786523::get_offset_of_mMaxInstances_9(),
	WordManagerImpl_t4282786523::get_offset_of_mWordPrefabCreationMode_10(),
	WordManagerImpl_t4282786523::get_offset_of_mVuforiaBehaviour_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (WordResult_t1915507197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (WordResultImpl_t911273601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2137[5] = 
{
	WordResultImpl_t911273601::get_offset_of_mObb_0(),
	WordResultImpl_t911273601::get_offset_of_mPosition_1(),
	WordResultImpl_t911273601::get_offset_of_mOrientation_2(),
	WordResultImpl_t911273601::get_offset_of_mWord_3(),
	WordResultImpl_t911273601::get_offset_of_mStatus_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (WordList_t1278495262), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (WordListImpl_t2150426444), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (VuforiaNativeIosWrapper_t1210651633), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (VuforiaNullWrapper_t3644069544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (VuforiaNativeWrapper_t2645113514), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (VuforiaWrapper_t3750170617), -1, sizeof(VuforiaWrapper_t3750170617_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2144[2] = 
{
	VuforiaWrapper_t3750170617_StaticFields::get_offset_of_sWrapper_0(),
	VuforiaWrapper_t3750170617_StaticFields::get_offset_of_sCamIndependentWrapper_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (KeepAliveAbstractBehaviour_t1486411203), -1, sizeof(KeepAliveAbstractBehaviour_t1486411203_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2148[8] = 
{
	KeepAliveAbstractBehaviour_t1486411203::get_offset_of_mKeepARCameraAlive_2(),
	KeepAliveAbstractBehaviour_t1486411203::get_offset_of_mKeepTrackableBehavioursAlive_3(),
	KeepAliveAbstractBehaviour_t1486411203::get_offset_of_mKeepTextRecoBehaviourAlive_4(),
	KeepAliveAbstractBehaviour_t1486411203::get_offset_of_mKeepUDTBuildingBehaviourAlive_5(),
	KeepAliveAbstractBehaviour_t1486411203::get_offset_of_mKeepCloudRecoBehaviourAlive_6(),
	KeepAliveAbstractBehaviour_t1486411203::get_offset_of_mKeepSmartTerrainAlive_7(),
	KeepAliveAbstractBehaviour_t1486411203_StaticFields::get_offset_of_sKeepAliveBehaviour_8(),
	KeepAliveAbstractBehaviour_t1486411203::get_offset_of_mHandlers_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (ReconstructionAbstractBehaviour_t3509595417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2149[22] = 
{
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mHasInitialized_2(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mSmartTerrainEventHandlers_3(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnInitialized_4(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnPropCreated_5(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnPropUpdated_6(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnPropDeleted_7(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnSurfaceCreated_8(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnSurfaceUpdated_9(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnSurfaceDeleted_10(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mInitializedInEditor_11(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mMaximumExtentEnabled_12(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mMaximumExtent_13(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mAutomaticStart_14(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mNavMeshUpdates_15(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mNavMeshPadding_16(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mReconstruction_17(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mSurfaces_18(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mActiveSurfaceBehaviours_19(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mProps_20(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mActivePropBehaviours_21(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mPreviouslySetWorldCenterSurfaceTemplate_22(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mIgnoreNextUpdate_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (PropAbstractBehaviour_t1047177596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2150[2] = 
{
	PropAbstractBehaviour_t1047177596::get_offset_of_mProp_14(),
	PropAbstractBehaviour_t1047177596::get_offset_of_mBoxColliderToUpdate_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (StateManager_t3369465942), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (StateManagerImpl_t3885489748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[7] = 
{
	StateManagerImpl_t3885489748::get_offset_of_mTrackableBehaviours_0(),
	StateManagerImpl_t3885489748::get_offset_of_mAutomaticallyCreatedBehaviours_1(),
	StateManagerImpl_t3885489748::get_offset_of_mBehavioursMarkedForDeletion_2(),
	StateManagerImpl_t3885489748::get_offset_of_mActiveTrackableBehaviours_3(),
	StateManagerImpl_t3885489748::get_offset_of_mWordManager_4(),
	StateManagerImpl_t3885489748::get_offset_of_mDeviceTrackingManager_5(),
	StateManagerImpl_t3885489748::get_offset_of_mCameraPositioningHelper_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (TargetFinder_t1347637805), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (InitState_t4409649)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2154[6] = 
{
	InitState_t4409649::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (UpdateState_t1473252352)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2155[12] = 
{
	UpdateState_t1473252352::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (FilterMode_t3082493643)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2156[3] = 
{
	FilterMode_t3082493643::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (TargetSearchResult_t1958726506)+ sizeof (Il2CppObject), sizeof(TargetSearchResult_t1958726506_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2157[6] = 
{
	TargetSearchResult_t1958726506::get_offset_of_TargetName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_UniqueTargetId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_TargetSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_MetaData_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_TrackingRating_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_TargetSearchResultPtr_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (TargetFinderImpl_t1380851697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2158[4] = 
{
	TargetFinderImpl_t1380851697::get_offset_of_mTargetFinderStatePtr_0(),
	TargetFinderImpl_t1380851697::get_offset_of_mTargetFinderState_1(),
	TargetFinderImpl_t1380851697::get_offset_of_mNewResults_2(),
	TargetFinderImpl_t1380851697::get_offset_of_mImageTargets_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (TargetFinderState_t3807887646)+ sizeof (Il2CppObject), sizeof(TargetFinderState_t3807887646_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2159[4] = 
{
	TargetFinderState_t3807887646::get_offset_of_IsRequesting_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t3807887646::get_offset_of_UpdateState_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t3807887646::get_offset_of_ResultCount_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t3807887646::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (InternalTargetSearchResult_t2369108641)+ sizeof (Il2CppObject), sizeof(InternalTargetSearchResult_t2369108641_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2160[6] = 
{
	InternalTargetSearchResult_t2369108641::get_offset_of_TargetNamePtr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2369108641::get_offset_of_UniqueTargetIdPtr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2369108641::get_offset_of_MetaDataPtr_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2369108641::get_offset_of_TargetSearchResultPtr_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2369108641::get_offset_of_TargetSize_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2369108641::get_offset_of_TrackingRating_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (TrackableSource_t2832298792), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (TrackableSourceImpl_t2574642394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2162[1] = 
{
	TrackableSourceImpl_t2574642394::get_offset_of_U3CTrackableSourcePtrU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (TextureRenderer_t3312477626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2163[3] = 
{
	TextureRenderer_t3312477626::get_offset_of_mTextureBufferCamera_0(),
	TextureRenderer_t3312477626::get_offset_of_mTextureWidth_1(),
	TextureRenderer_t3312477626::get_offset_of_mTextureHeight_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (TrackerManager_t308318605), -1, sizeof(TrackerManager_t308318605_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2164[1] = 
{
	TrackerManager_t308318605_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (TrackerManagerImpl_t381223961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2165[6] = 
{
	TrackerManagerImpl_t381223961::get_offset_of_mObjectTracker_1(),
	TrackerManagerImpl_t381223961::get_offset_of_mMarkerTracker_2(),
	TrackerManagerImpl_t381223961::get_offset_of_mTextTracker_3(),
	TrackerManagerImpl_t381223961::get_offset_of_mSmartTerrainTracker_4(),
	TrackerManagerImpl_t381223961::get_offset_of_mDeviceTracker_5(),
	TrackerManagerImpl_t381223961::get_offset_of_mStateManager_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (VirtualButton_t3703236737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2166[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (Sensitivity_t1678924861)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2167[4] = 
{
	Sensitivity_t1678924861::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (VirtualButtonImpl_t2449737797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2168[6] = 
{
	VirtualButtonImpl_t2449737797::get_offset_of_mName_1(),
	VirtualButtonImpl_t2449737797::get_offset_of_mID_2(),
	VirtualButtonImpl_t2449737797::get_offset_of_mArea_3(),
	VirtualButtonImpl_t2449737797::get_offset_of_mIsEnabled_4(),
	VirtualButtonImpl_t2449737797::get_offset_of_mParentImageTarget_5(),
	VirtualButtonImpl_t2449737797::get_offset_of_mParentDataSet_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (WebCamImpl_t2771725761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[15] = 
{
	WebCamImpl_t2771725761::get_offset_of_mARCameras_0(),
	WebCamImpl_t2771725761::get_offset_of_mOriginalCameraCullMask_1(),
	WebCamImpl_t2771725761::get_offset_of_mWebCamTexture_2(),
	WebCamImpl_t2771725761::get_offset_of_mVideoModeData_3(),
	WebCamImpl_t2771725761::get_offset_of_mVideoTextureInfo_4(),
	WebCamImpl_t2771725761::get_offset_of_mTextureRenderer_5(),
	WebCamImpl_t2771725761::get_offset_of_mBufferReadTexture_6(),
	WebCamImpl_t2771725761::get_offset_of_mReadPixelsRect_7(),
	WebCamImpl_t2771725761::get_offset_of_mWebCamProfile_8(),
	WebCamImpl_t2771725761::get_offset_of_mFlipHorizontally_9(),
	WebCamImpl_t2771725761::get_offset_of_mIsDirty_10(),
	WebCamImpl_t2771725761::get_offset_of_mLastFrameIdx_11(),
	WebCamImpl_t2771725761::get_offset_of_mRenderTextureLayer_12(),
	WebCamImpl_t2771725761::get_offset_of_mWebcamPlaying_13(),
	WebCamImpl_t2771725761::get_offset_of_U3CIsTextureSizeAvailableU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (WebCamProfile_t3757625748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[1] = 
{
	WebCamProfile_t3757625748::get_offset_of_mProfileCollection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (ProfileData_t1724666488)+ sizeof (Il2CppObject), sizeof(ProfileData_t1724666488_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2171[3] = 
{
	ProfileData_t1724666488::get_offset_of_RequestedTextureSize_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileData_t1724666488::get_offset_of_ResampledTextureSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileData_t1724666488::get_offset_of_RequestedFPS_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (ProfileCollection_t3644865120)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2172[2] = 
{
	ProfileCollection_t3644865120::get_offset_of_DefaultProfile_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileCollection_t3644865120::get_offset_of_Profiles_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (ImageTargetAbstractBehaviour_t3327552701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2176[4] = 
{
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mAspectRatio_21(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mImageTargetType_22(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mImageTarget_23(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mVirtualButtonBehaviours_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (MarkerAbstractBehaviour_t1456101953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2177[2] = 
{
	MarkerAbstractBehaviour_t1456101953::get_offset_of_mMarkerID_10(),
	MarkerAbstractBehaviour_t1456101953::get_offset_of_mMarker_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (MaskOutAbstractBehaviour_t3489038957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2178[1] = 
{
	MaskOutAbstractBehaviour_t3489038957::get_offset_of_maskMaterial_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (MultiTargetAbstractBehaviour_t3616801211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2179[1] = 
{
	MultiTargetAbstractBehaviour_t3616801211::get_offset_of_mMultiTarget_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (VuforiaUnity_t657456673), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (InitError_t2149396216)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2181[12] = 
{
	InitError_t2149396216::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (VuforiaHint_t3491240575)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2182[4] = 
{
	VuforiaHint_t3491240575::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (StorageType_t3897282321)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2183[4] = 
{
	StorageType_t3897282321::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (VuforiaAbstractBehaviour_t3319870759), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2184[41] = 
{
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_VuforiaLicenseKey_2(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_CameraDeviceModeSetting_3(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_MaxSimultaneousImageTargets_4(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_MaxSimultaneousObjectTargets_5(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_UseDelayedLoadingObjectTargets_6(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_CameraDirection_7(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_MirrorVideoBackground_8(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mWorldCenterMode_9(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mWorldCenter_10(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mTrackerEventHandlers_11(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mVideoBgEventHandlers_12(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mOnVuforiaInitError_13(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mOnVuforiaInitialized_14(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mOnVuforiaStarted_15(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mOnTrackablesUpdated_16(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mRenderOnUpdate_17(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mOnPause_18(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mOnBackgroundTextureChanged_19(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mStartHasBeenInvoked_20(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mHasStarted_21(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mFailedToInitialize_22(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mBackgroundTextureHasChanged_23(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mInitError_24(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mCameraConfiguration_25(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mEyewearBehaviour_26(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mVideoBackgroundMgr_27(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mCheckStopCamera_28(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mClearMaterial_29(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mMetalRendering_30(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mHasStartedOnce_31(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mWasEnabledBeforePause_32(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mObjectTrackerWasActiveBeforePause_33(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mObjectTrackerWasActiveBeforeDisabling_34(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mMarkerTrackerWasActiveBeforePause_35(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mMarkerTrackerWasActiveBeforeDisabling_36(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mLastUpdatedFrame_37(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mTrackersRequestedToDeinit_38(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mMissedToApplyLeftProjectionMatrix_39(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mMissedToApplyRightProjectionMatrix_40(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mLeftProjectMatrixToApply_41(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mRightProjectMatrixToApply_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (WorldCenterMode_t3132552034)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2185[5] = 
{
	WorldCenterMode_t3132552034::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (VuforiaMacros_t1884408435)+ sizeof (Il2CppObject), sizeof(VuforiaMacros_t1884408435_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2186[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (VuforiaRuntimeUtilities_t3083157244), -1, sizeof(VuforiaRuntimeUtilities_t3083157244_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2187[2] = 
{
	VuforiaRuntimeUtilities_t3083157244_StaticFields::get_offset_of_sWebCamUsed_0(),
	VuforiaRuntimeUtilities_t3083157244_StaticFields::get_offset_of_sNativePluginSupport_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (InitializableBool_t1916387570)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2188[4] = 
{
	InitializableBool_t1916387570::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (SurfaceUtilities_t4096327849), -1, sizeof(SurfaceUtilities_t4096327849_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2189[1] = 
{
	SurfaceUtilities_t4096327849_StaticFields::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (TextRecoAbstractBehaviour_t2386081773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2190[12] = 
{
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mHasInitialized_2(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTrackerWasActiveBeforePause_3(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTrackerWasActiveBeforeDisabling_4(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mWordListFile_5(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mCustomWordListFile_6(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mAdditionalCustomWords_7(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mFilterMode_8(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mFilterListFile_9(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mAdditionalFilterWords_10(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mWordPrefabCreationMode_11(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mMaximumWordInstances_12(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTextRecoEventHandlers_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (SimpleTargetData_t3993525265)+ sizeof (Il2CppObject), sizeof(SimpleTargetData_t3993525265_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2191[2] = 
{
	SimpleTargetData_t3993525265::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SimpleTargetData_t3993525265::get_offset_of_unused_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (TurnOffAbstractBehaviour_t4084926705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (UserDefinedTargetBuildingAbstractBehaviour_t3589690572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2193[11] = 
{
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mObjectTracker_2(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mLastFrameQuality_3(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mCurrentlyScanning_4(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mWasScanningBeforeDisable_5(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mCurrentlyBuilding_6(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mWasBuildingBeforeDisable_7(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mOnInitializedCalled_8(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mHandlers_9(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StopTrackerWhileScanning_10(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StartScanningAutomatically_11(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StopScanningWhenFinshedBuilding_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (VideoBackgroundAbstractBehaviour_t395384314), -1, sizeof(VideoBackgroundAbstractBehaviour_t395384314_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2194[9] = 
{
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mClearBuffers_2(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mSkipStateUpdates_3(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mVuforiaAbstractBehaviour_4(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mCamera_5(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mBackgroundBehaviour_6(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mStereoDepth_7(),
	VideoBackgroundAbstractBehaviour_t395384314_StaticFields::get_offset_of_mFrameCounter_8(),
	VideoBackgroundAbstractBehaviour_t395384314_StaticFields::get_offset_of_mRenderCounter_9(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mDisabledMeshRenderers_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (VideoBackgroundManagerAbstractBehaviour_t3765780423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2195[7] = 
{
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mTexture_2(),
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mVideoBgConfigChanged_3(),
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mNativeTexturePtr_4(),
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mClippingMode_5(),
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mMatteShader_6(),
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mVideoBackgroundEnabled_7(),
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mVuforiaBehaviour_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (VirtualButtonAbstractBehaviour_t2478279366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2196[14] = 
{
	0,
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mName_3(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mSensitivity_4(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mHasUpdatedPose_5(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPrevTransform_6(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPrevParent_7(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mSensitivityDirty_8(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPreviouslyEnabled_9(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPressed_10(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mHandlers_11(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mLeftTop_12(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mRightBottom_13(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mUnregisterOnDestroy_14(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mVirtualButton_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (WebCamAbstractBehaviour_t4104806356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2197[6] = 
{
	WebCamAbstractBehaviour_t4104806356::get_offset_of_RenderTextureLayer_2(),
	WebCamAbstractBehaviour_t4104806356::get_offset_of_mDeviceNameSetInEditor_3(),
	WebCamAbstractBehaviour_t4104806356::get_offset_of_mFlipHorizontally_4(),
	WebCamAbstractBehaviour_t4104806356::get_offset_of_mTurnOffWebCam_5(),
	WebCamAbstractBehaviour_t4104806356::get_offset_of_mWebCamImpl_6(),
	WebCamAbstractBehaviour_t4104806356::get_offset_of_mBackgroundCameraInstance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (WordAbstractBehaviour_t2878458725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2198[3] = 
{
	WordAbstractBehaviour_t2878458725::get_offset_of_mMode_10(),
	WordAbstractBehaviour_t2878458725::get_offset_of_mSpecificWord_11(),
	WordAbstractBehaviour_t2878458725::get_offset_of_mWord_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (WordFilterMode_t695600879)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2199[4] = 
{
	WordFilterMode_t695600879::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
