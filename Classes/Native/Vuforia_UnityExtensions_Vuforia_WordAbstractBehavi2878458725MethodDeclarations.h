﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t2878458725;
// Vuforia.Word
struct Word_t3872119486;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "Vuforia_UnityExtensions_Vuforia_WordTemplateMode1097144495.h"

// System.Void Vuforia.WordAbstractBehaviour::InternalUnregisterTrackable()
extern "C"  void WordAbstractBehaviour_InternalUnregisterTrackable_m1616159713 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Word Vuforia.WordAbstractBehaviour::get_Word()
extern "C"  Il2CppObject * WordAbstractBehaviour_get_Word_m2554944169 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_SpecificWord()
extern "C"  String_t* WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m3036158392 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetSpecificWord(System.String)
extern "C"  void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m3261411872 (WordAbstractBehaviour_t2878458725 * __this, String_t* ___word0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordTemplateMode Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_Mode()
extern "C"  int32_t WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m2878670072 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsTemplateMode()
extern "C"  bool WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m2245541380 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsSpecificWordMode()
extern "C"  bool WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m3280833542 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetMode(Vuforia.WordTemplateMode)
extern "C"  void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m845570902 (WordAbstractBehaviour_t2878458725 * __this, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.InitializeWord(Vuforia.Word)
extern "C"  void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m3111632870 (WordAbstractBehaviour_t2878458725 * __this, Il2CppObject * ___word0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::.ctor()
extern "C"  void WordAbstractBehaviour__ctor_m1415816009 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C"  bool WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m1796602413 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C"  void WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m776300256 (WordAbstractBehaviour_t2878458725 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C"  Transform_t3275118058 * WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m1279349318 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C"  GameObject_t1756533147 * WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m2929090518 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
