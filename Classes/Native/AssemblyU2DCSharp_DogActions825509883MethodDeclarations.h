﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DogActions
struct DogActions_t825509883;

#include "codegen/il2cpp-codegen.h"

// System.Void DogActions::.ctor()
extern "C"  void DogActions__ctor_m3166527972 (DogActions_t825509883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DogActions::Start()
extern "C"  void DogActions_Start_m1361422856 (DogActions_t825509883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DogActions::Update()
extern "C"  void DogActions_Update_m2530224603 (DogActions_t825509883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DogActions::EnableMouth()
extern "C"  void DogActions_EnableMouth_m3481844422 (DogActions_t825509883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DogActions::DisableMouth()
extern "C"  void DogActions_DisableMouth_m723564969 (DogActions_t825509883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DogActions::Eatbone()
extern "C"  void DogActions_Eatbone_m2367335792 (DogActions_t825509883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DogActions::attack()
extern "C"  void DogActions_attack_m248197194 (DogActions_t825509883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
