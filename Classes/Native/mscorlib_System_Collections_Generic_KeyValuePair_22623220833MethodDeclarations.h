﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2433599180(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2623220833 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m3201181706_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>::get_Key()
#define KeyValuePair_2_get_Key_m655180134(__this, method) ((  int32_t (*) (KeyValuePair_2_t2623220833 *, const MethodInfo*))KeyValuePair_2_get_Key_m1435832840_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m169728297(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2623220833 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1350990071_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>::get_Value()
#define KeyValuePair_2_get_Value_m746410918(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2623220833 *, const MethodInfo*))KeyValuePair_2_get_Value_m3690000728_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m856793353(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2623220833 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m2726037047_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>::ToString()
#define KeyValuePair_2_ToString_m611113747(__this, method) ((  String_t* (*) (KeyValuePair_2_t2623220833 *, const MethodInfo*))KeyValuePair_2_ToString_m1391611625_gshared)(__this, method)
