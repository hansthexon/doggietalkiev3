﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LitJson.ExporterFunc`1<System.Type>
struct ExporterFunc_1_t2540916173;
// LitJson.ImporterFunc`2<System.String,System.Type>
struct ImporterFunc_2_t3485802444;
// System.Action`2<UnityEngine.Vector2,LitJson.JsonWriter>
struct Action_2_t3176658245;
// System.Action`2<UnityEngine.Vector3,LitJson.JsonWriter>
struct Action_2_t3662845984;
// LitJson.ExporterFunc`1<UnityEngine.Vector4>
struct ExporterFunc_1_t3480820528;
// LitJson.ExporterFunc`1<UnityEngine.Quaternion>
struct ExporterFunc_1_t972219569;
// LitJson.ExporterFunc`1<UnityEngine.Color>
struct ExporterFunc_1_t3257505022;
// LitJson.ExporterFunc`1<UnityEngine.Color32>
struct ExporterFunc_1_t2111630465;
// LitJson.ExporterFunc`1<UnityEngine.Rect>
struct ExporterFunc_1_t623901277;
// LitJson.ExporterFunc`1<UnityEngine.RectOffset>
struct ExporterFunc_1_t329972078;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.UnityTypeBindings
struct  UnityTypeBindings_t1763022095  : public Il2CppObject
{
public:

public:
};

struct UnityTypeBindings_t1763022095_StaticFields
{
public:
	// System.Boolean LitJson.UnityTypeBindings::registerd
	bool ___registerd_0;
	// LitJson.ExporterFunc`1<System.Type> LitJson.UnityTypeBindings::<>f__am$cache1
	ExporterFunc_1_t2540916173 * ___U3CU3Ef__amU24cache1_1;
	// LitJson.ImporterFunc`2<System.String,System.Type> LitJson.UnityTypeBindings::<>f__am$cache2
	ImporterFunc_2_t3485802444 * ___U3CU3Ef__amU24cache2_2;
	// System.Action`2<UnityEngine.Vector2,LitJson.JsonWriter> LitJson.UnityTypeBindings::<>f__am$cache3
	Action_2_t3176658245 * ___U3CU3Ef__amU24cache3_3;
	// System.Action`2<UnityEngine.Vector3,LitJson.JsonWriter> LitJson.UnityTypeBindings::<>f__am$cache4
	Action_2_t3662845984 * ___U3CU3Ef__amU24cache4_4;
	// LitJson.ExporterFunc`1<UnityEngine.Vector4> LitJson.UnityTypeBindings::<>f__am$cache5
	ExporterFunc_1_t3480820528 * ___U3CU3Ef__amU24cache5_5;
	// LitJson.ExporterFunc`1<UnityEngine.Quaternion> LitJson.UnityTypeBindings::<>f__am$cache6
	ExporterFunc_1_t972219569 * ___U3CU3Ef__amU24cache6_6;
	// LitJson.ExporterFunc`1<UnityEngine.Color> LitJson.UnityTypeBindings::<>f__am$cache7
	ExporterFunc_1_t3257505022 * ___U3CU3Ef__amU24cache7_7;
	// LitJson.ExporterFunc`1<UnityEngine.Color32> LitJson.UnityTypeBindings::<>f__am$cache8
	ExporterFunc_1_t2111630465 * ___U3CU3Ef__amU24cache8_8;
	// LitJson.ExporterFunc`1<UnityEngine.Rect> LitJson.UnityTypeBindings::<>f__am$cache9
	ExporterFunc_1_t623901277 * ___U3CU3Ef__amU24cache9_9;
	// LitJson.ExporterFunc`1<UnityEngine.RectOffset> LitJson.UnityTypeBindings::<>f__am$cacheA
	ExporterFunc_1_t329972078 * ___U3CU3Ef__amU24cacheA_10;

public:
	inline static int32_t get_offset_of_registerd_0() { return static_cast<int32_t>(offsetof(UnityTypeBindings_t1763022095_StaticFields, ___registerd_0)); }
	inline bool get_registerd_0() const { return ___registerd_0; }
	inline bool* get_address_of_registerd_0() { return &___registerd_0; }
	inline void set_registerd_0(bool value)
	{
		___registerd_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(UnityTypeBindings_t1763022095_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline ExporterFunc_1_t2540916173 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline ExporterFunc_1_t2540916173 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(ExporterFunc_1_t2540916173 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(UnityTypeBindings_t1763022095_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline ImporterFunc_2_t3485802444 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline ImporterFunc_2_t3485802444 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(ImporterFunc_2_t3485802444 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(UnityTypeBindings_t1763022095_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Action_2_t3176658245 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Action_2_t3176658245 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Action_2_t3176658245 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(UnityTypeBindings_t1763022095_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Action_2_t3662845984 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Action_2_t3662845984 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Action_2_t3662845984 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(UnityTypeBindings_t1763022095_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline ExporterFunc_1_t3480820528 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline ExporterFunc_1_t3480820528 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(ExporterFunc_1_t3480820528 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(UnityTypeBindings_t1763022095_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline ExporterFunc_1_t972219569 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline ExporterFunc_1_t972219569 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(ExporterFunc_1_t972219569 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(UnityTypeBindings_t1763022095_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline ExporterFunc_1_t3257505022 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline ExporterFunc_1_t3257505022 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(ExporterFunc_1_t3257505022 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_8() { return static_cast<int32_t>(offsetof(UnityTypeBindings_t1763022095_StaticFields, ___U3CU3Ef__amU24cache8_8)); }
	inline ExporterFunc_1_t2111630465 * get_U3CU3Ef__amU24cache8_8() const { return ___U3CU3Ef__amU24cache8_8; }
	inline ExporterFunc_1_t2111630465 ** get_address_of_U3CU3Ef__amU24cache8_8() { return &___U3CU3Ef__amU24cache8_8; }
	inline void set_U3CU3Ef__amU24cache8_8(ExporterFunc_1_t2111630465 * value)
	{
		___U3CU3Ef__amU24cache8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_9() { return static_cast<int32_t>(offsetof(UnityTypeBindings_t1763022095_StaticFields, ___U3CU3Ef__amU24cache9_9)); }
	inline ExporterFunc_1_t623901277 * get_U3CU3Ef__amU24cache9_9() const { return ___U3CU3Ef__amU24cache9_9; }
	inline ExporterFunc_1_t623901277 ** get_address_of_U3CU3Ef__amU24cache9_9() { return &___U3CU3Ef__amU24cache9_9; }
	inline void set_U3CU3Ef__amU24cache9_9(ExporterFunc_1_t623901277 * value)
	{
		___U3CU3Ef__amU24cache9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_10() { return static_cast<int32_t>(offsetof(UnityTypeBindings_t1763022095_StaticFields, ___U3CU3Ef__amU24cacheA_10)); }
	inline ExporterFunc_1_t329972078 * get_U3CU3Ef__amU24cacheA_10() const { return ___U3CU3Ef__amU24cacheA_10; }
	inline ExporterFunc_1_t329972078 ** get_address_of_U3CU3Ef__amU24cacheA_10() { return &___U3CU3Ef__amU24cacheA_10; }
	inline void set_U3CU3Ef__amU24cacheA_10(ExporterFunc_1_t329972078 * value)
	{
		___U3CU3Ef__amU24cacheA_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
