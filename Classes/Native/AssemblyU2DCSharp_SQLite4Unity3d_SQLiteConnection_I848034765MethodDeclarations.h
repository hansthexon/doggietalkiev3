﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.SQLiteConnection/IndexInfo
struct IndexInfo_t848034765;
struct IndexInfo_t848034765_marshaled_pinvoke;
struct IndexInfo_t848034765_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct IndexInfo_t848034765;
struct IndexInfo_t848034765_marshaled_pinvoke;

extern "C" void IndexInfo_t848034765_marshal_pinvoke(const IndexInfo_t848034765& unmarshaled, IndexInfo_t848034765_marshaled_pinvoke& marshaled);
extern "C" void IndexInfo_t848034765_marshal_pinvoke_back(const IndexInfo_t848034765_marshaled_pinvoke& marshaled, IndexInfo_t848034765& unmarshaled);
extern "C" void IndexInfo_t848034765_marshal_pinvoke_cleanup(IndexInfo_t848034765_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct IndexInfo_t848034765;
struct IndexInfo_t848034765_marshaled_com;

extern "C" void IndexInfo_t848034765_marshal_com(const IndexInfo_t848034765& unmarshaled, IndexInfo_t848034765_marshaled_com& marshaled);
extern "C" void IndexInfo_t848034765_marshal_com_back(const IndexInfo_t848034765_marshaled_com& marshaled, IndexInfo_t848034765& unmarshaled);
extern "C" void IndexInfo_t848034765_marshal_com_cleanup(IndexInfo_t848034765_marshaled_com& marshaled);
