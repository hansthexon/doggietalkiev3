﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.OrderedEnumerable`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct OrderedEnumerable_1_t2198792967;
// System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct IEnumerable_1_t966287033;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct IEnumerator_1_t2444651111;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.OrderedEnumerable`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  void OrderedEnumerable_1__ctor_m3344723181_gshared (OrderedEnumerable_1_t2198792967 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define OrderedEnumerable_1__ctor_m3344723181(__this, ___source0, method) ((  void (*) (OrderedEnumerable_1_t2198792967 *, Il2CppObject*, const MethodInfo*))OrderedEnumerable_1__ctor_m3344723181_gshared)(__this, ___source0, method)
// System.Collections.IEnumerator System.Linq.OrderedEnumerable`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * OrderedEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m1837738625_gshared (OrderedEnumerable_1_t2198792967 * __this, const MethodInfo* method);
#define OrderedEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m1837738625(__this, method) ((  Il2CppObject * (*) (OrderedEnumerable_1_t2198792967 *, const MethodInfo*))OrderedEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m1837738625_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::GetEnumerator()
extern "C"  Il2CppObject* OrderedEnumerable_1_GetEnumerator_m650682298_gshared (OrderedEnumerable_1_t2198792967 * __this, const MethodInfo* method);
#define OrderedEnumerable_1_GetEnumerator_m650682298(__this, method) ((  Il2CppObject* (*) (OrderedEnumerable_1_t2198792967 *, const MethodInfo*))OrderedEnumerable_1_GetEnumerator_m650682298_gshared)(__this, method)
