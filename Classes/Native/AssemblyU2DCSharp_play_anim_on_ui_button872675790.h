﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.Animator
struct Animator_t69676727;
// UnityEngine.Canvas
struct Canvas_t209405766;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// play_anim_on_ui_button
struct  play_anim_on_ui_button_t872675790  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button play_anim_on_ui_button::Text
	Button_t2872111280 * ___Text_2;
	// UnityEngine.AudioClip play_anim_on_ui_button::sound
	AudioClip_t1932558630 * ___sound_3;
	// UnityEngine.Animator play_anim_on_ui_button::ani
	Animator_t69676727 * ___ani_4;
	// UnityEngine.Canvas play_anim_on_ui_button::yourcanvas
	Canvas_t209405766 * ___yourcanvas_5;

public:
	inline static int32_t get_offset_of_Text_2() { return static_cast<int32_t>(offsetof(play_anim_on_ui_button_t872675790, ___Text_2)); }
	inline Button_t2872111280 * get_Text_2() const { return ___Text_2; }
	inline Button_t2872111280 ** get_address_of_Text_2() { return &___Text_2; }
	inline void set_Text_2(Button_t2872111280 * value)
	{
		___Text_2 = value;
		Il2CppCodeGenWriteBarrier(&___Text_2, value);
	}

	inline static int32_t get_offset_of_sound_3() { return static_cast<int32_t>(offsetof(play_anim_on_ui_button_t872675790, ___sound_3)); }
	inline AudioClip_t1932558630 * get_sound_3() const { return ___sound_3; }
	inline AudioClip_t1932558630 ** get_address_of_sound_3() { return &___sound_3; }
	inline void set_sound_3(AudioClip_t1932558630 * value)
	{
		___sound_3 = value;
		Il2CppCodeGenWriteBarrier(&___sound_3, value);
	}

	inline static int32_t get_offset_of_ani_4() { return static_cast<int32_t>(offsetof(play_anim_on_ui_button_t872675790, ___ani_4)); }
	inline Animator_t69676727 * get_ani_4() const { return ___ani_4; }
	inline Animator_t69676727 ** get_address_of_ani_4() { return &___ani_4; }
	inline void set_ani_4(Animator_t69676727 * value)
	{
		___ani_4 = value;
		Il2CppCodeGenWriteBarrier(&___ani_4, value);
	}

	inline static int32_t get_offset_of_yourcanvas_5() { return static_cast<int32_t>(offsetof(play_anim_on_ui_button_t872675790, ___yourcanvas_5)); }
	inline Canvas_t209405766 * get_yourcanvas_5() const { return ___yourcanvas_5; }
	inline Canvas_t209405766 ** get_address_of_yourcanvas_5() { return &___yourcanvas_5; }
	inline void set_yourcanvas_5(Canvas_t209405766 * value)
	{
		___yourcanvas_5 = value;
		Il2CppCodeGenWriteBarrier(&___yourcanvas_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
