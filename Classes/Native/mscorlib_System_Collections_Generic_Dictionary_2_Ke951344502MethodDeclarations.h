﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2923592664MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1721091991(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t951344502 *, Dictionary_2_t2762814027 *, const MethodInfo*))KeyCollection__ctor_m2113853019_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3356419037(__this, ___item0, method) ((  void (*) (KeyCollection_t951344502 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1005923985_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m254995120(__this, method) ((  void (*) (KeyCollection_t951344502 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2085351498_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1990936907(__this, ___item0, method) ((  bool (*) (KeyCollection_t951344502 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3945669279_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2142676476(__this, ___item0, method) ((  bool (*) (KeyCollection_t951344502 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3143167270_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3376477174(__this, method) ((  Il2CppObject* (*) (KeyCollection_t951344502 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1394097920_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m550626806(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t951344502 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1923914272_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2788633433(__this, method) ((  Il2CppObject * (*) (KeyCollection_t951344502 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2219900805_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1054168806(__this, method) ((  bool (*) (KeyCollection_t951344502 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3175780828_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3175480630(__this, method) ((  bool (*) (KeyCollection_t951344502 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2965775648_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m648856374(__this, method) ((  Il2CppObject * (*) (KeyCollection_t951344502 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2849362144_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m3100572696(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t951344502 *, StringU5BU5D_t1642385972*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2449658018_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1193319485(__this, method) ((  Enumerator_t1157350169  (*) (KeyCollection_t951344502 *, const MethodInfo*))KeyCollection_GetEnumerator_m2543932487_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Count()
#define KeyCollection_get_Count_m2083470174(__this, method) ((  int32_t (*) (KeyCollection_t951344502 *, const MethodInfo*))KeyCollection_get_Count_m1053061128_gshared)(__this, method)
