﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// play_anim_on_ui_button
struct play_anim_on_ui_button_t872675790;

#include "codegen/il2cpp-codegen.h"

// System.Void play_anim_on_ui_button::.ctor()
extern "C"  void play_anim_on_ui_button__ctor_m3759501275 (play_anim_on_ui_button_t872675790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void play_anim_on_ui_button::Start123()
extern "C"  void play_anim_on_ui_button_Start123_m3633498011 (play_anim_on_ui_button_t872675790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void play_anim_on_ui_button::Press123()
extern "C"  void play_anim_on_ui_button_Press123_m3447945932 (play_anim_on_ui_button_t872675790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
