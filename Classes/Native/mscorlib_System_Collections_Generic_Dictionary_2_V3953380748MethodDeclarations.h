﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct ValueCollection_t3953380748;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t955353609;
// System.Collections.Generic.IEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>
struct IEnumerator_1_t3718019097;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// Vuforia.VuforiaManagerImpl/TrackableResultData[]
struct TrackableResultDataU5BU5D_t1439643011;

#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1947527974.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2641886373.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m3058588937_gshared (ValueCollection_t3953380748 * __this, Dictionary_2_t955353609 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m3058588937(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3953380748 *, Dictionary_2_t955353609 *, const MethodInfo*))ValueCollection__ctor_m3058588937_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3245193367_gshared (ValueCollection_t3953380748 * __this, TrackableResultData_t1947527974  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3245193367(__this, ___item0, method) ((  void (*) (ValueCollection_t3953380748 *, TrackableResultData_t1947527974 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3245193367_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1369456300_gshared (ValueCollection_t3953380748 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1369456300(__this, method) ((  void (*) (ValueCollection_t3953380748 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1369456300_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1784964545_gshared (ValueCollection_t3953380748 * __this, TrackableResultData_t1947527974  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1784964545(__this, ___item0, method) ((  bool (*) (ValueCollection_t3953380748 *, TrackableResultData_t1947527974 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1784964545_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m372571374_gshared (ValueCollection_t3953380748 * __this, TrackableResultData_t1947527974  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m372571374(__this, ___item0, method) ((  bool (*) (ValueCollection_t3953380748 *, TrackableResultData_t1947527974 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m372571374_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1769785088_gshared (ValueCollection_t3953380748 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1769785088(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3953380748 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1769785088_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m321225704_gshared (ValueCollection_t3953380748 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m321225704(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3953380748 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m321225704_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2231042247_gshared (ValueCollection_t3953380748 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2231042247(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3953380748 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2231042247_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1643757122_gshared (ValueCollection_t3953380748 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1643757122(__this, method) ((  bool (*) (ValueCollection_t3953380748 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1643757122_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m442273292_gshared (ValueCollection_t3953380748 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m442273292(__this, method) ((  bool (*) (ValueCollection_t3953380748 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m442273292_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3020892296_gshared (ValueCollection_t3953380748 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3020892296(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3953380748 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3020892296_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m2600113296_gshared (ValueCollection_t3953380748 * __this, TrackableResultDataU5BU5D_t1439643011* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m2600113296(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3953380748 *, TrackableResultDataU5BU5D_t1439643011*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2600113296_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::GetEnumerator()
extern "C"  Enumerator_t2641886373  ValueCollection_GetEnumerator_m797234613_gshared (ValueCollection_t3953380748 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m797234613(__this, method) ((  Enumerator_t2641886373  (*) (ValueCollection_t3953380748 *, const MethodInfo*))ValueCollection_GetEnumerator_m797234613_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3679740364_gshared (ValueCollection_t3953380748 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3679740364(__this, method) ((  int32_t (*) (ValueCollection_t3953380748 *, const MethodInfo*))ValueCollection_get_Count_m3679740364_gshared)(__this, method)
