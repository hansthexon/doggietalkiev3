﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_WaitForSecondsRealtime2105307154.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1808633952.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram1120735295.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttri1027170048.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAtt665825653.h"
#include "UnityEngine_UnityEngine_Logger3328995178.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3212052468.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative1913052472.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri3673080018.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules1810425448.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1390152093.h"
#include "UnityEngine_UnityEngineInternal_GenericStack3718539591.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4275971970.h"
#include "UnityEngine_UnityEngine_AndroidJavaRunnable3501776228.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"
#include "Apple_U3CModuleU3E3783534214.h"
#include "Apple_UnityEngine_Purchasing_UnityPurchasingCallba2635187846.h"
#include "Apple_UnityEngine_Purchasing_iOSStoreBindings2633471826.h"
#include "Apple_UnityEngine_Purchasing_OSXStoreBindings116576999.h"
#include "Common_U3CModuleU3E3783534214.h"
#include "Common_UnityEngine_Purchasing_MiniJson838727235.h"
#include "UnityEngine_Purchasing_U3CModuleU3E3783534214.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Anal3513180421.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Async423752048.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Asyn2099263868.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Asyn3541402849.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Clou3988464631.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Clou3565118814.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_IDs3808979560.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Conf1298400415.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Init2954032642.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1203687971.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod3600019299.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1942475268.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1573242544.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod2754455291.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purch547992434.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Exte1607114611.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc1322959839.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc2407199463.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc3525211160.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purch728606867.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Stor2597962341.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Transa45391254.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Unit4076614841.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Unit3301441281.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Unity768337693.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Unit1270402009.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Exte4102635892.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Exte2787096497.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Exte3318267523.h"
#include "UnityEngine_UI_U3CModuleU3E3783534214.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandle942672932.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg1967201810.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3959312622.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3365010046.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg2524067914.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1600[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1601[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1602[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1603[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (WaitForSecondsRealtime_t2105307154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1604[1] = 
{
	WaitForSecondsRealtime_t2105307154::get_offset_of_waitTime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (AnimationPlayableUtilities_t1808633952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (FrameData_t1120735295)+ sizeof (Il2CppObject), sizeof(FrameData_t1120735295_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1606[4] = 
{
	FrameData_t1120735295::get_offset_of_m_UpdateId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_Time_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_LastTime_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_TimeScale_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (DefaultValueAttribute_t1027170048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1607[1] = 
{
	DefaultValueAttribute_t1027170048::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (ExcludeFromDocsAttribute_t665825653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (Logger_t3328995178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1611[3] = 
{
	Logger_t3328995178::get_offset_of_U3ClogHandlerU3Ek__BackingField_0(),
	Logger_t3328995178::get_offset_of_U3ClogEnabledU3Ek__BackingField_1(),
	Logger_t3328995178::get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (UsedByNativeCodeAttribute_t3212052468), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (RequiredByNativeCodeAttribute_t1913052472), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (FormerlySerializedAsAttribute_t3673080018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1614[1] = 
{
	FormerlySerializedAsAttribute_t3673080018::get_offset_of_m_oldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (TypeInferenceRules_t1810425448)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1615[5] = 
{
	TypeInferenceRules_t1810425448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (TypeInferenceRuleAttribute_t1390152093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1616[1] = 
{
	TypeInferenceRuleAttribute_t1390152093::get_offset_of__rule_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (GenericStack_t3718539591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (NetFxCoreExtensions_t4275971970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (AndroidJavaRunnable_t3501776228), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (UnityAction_t4025899511), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (U3CModuleU3E_t3783534219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (UnityPurchasingCallback_t2635187846), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (iOSStoreBindings_t2633471826), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (OSXStoreBindings_t116576999), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (MiniJson_t838727235), -1, sizeof(MiniJson_t838727235_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1632[2] = 
{
	MiniJson_t838727235_StaticFields::get_offset_of_s_LastErrorIndex_0(),
	MiniJson_t838727235_StaticFields::get_offset_of_s_LastDecode_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (AnalyticsReporter_t3513180421), -1, sizeof(AnalyticsReporter_t3513180421_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1634[2] = 
{
	AnalyticsReporter_t3513180421::get_offset_of_m_Analytics_0(),
	AnalyticsReporter_t3513180421_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (AsyncUtil_t423752048), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (U3CDoInvokeU3Ec__Iterator0_t2099263868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1636[6] = 
{
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_delayInSeconds_0(),
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_a_1(),
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_U24PC_2(),
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_U24current_3(),
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_U3CU24U3EdelayInSeconds_4(),
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_U3CU24U3Ea_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (U3CProcessU3Ec__Iterator1_t3541402849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1637[8] = 
{
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_request_0(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_errorHandler_1(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_responseHandler_2(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_U24PC_3(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_U24current_4(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_U3CU24U3Erequest_5(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_U3CU24U3EerrorHandler_6(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_U3CU24U3EresponseHandler_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (CloudCatalogManager_t3988464631), -1, sizeof(CloudCatalogManager_t3988464631_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1638[7] = 
{
	CloudCatalogManager_t3988464631::get_offset_of_m_AsyncUtil_0(),
	CloudCatalogManager_t3988464631::get_offset_of_m_CacheFileName_1(),
	CloudCatalogManager_t3988464631::get_offset_of_m_Logger_2(),
	CloudCatalogManager_t3988464631::get_offset_of_m_CatalogURL_3(),
	CloudCatalogManager_t3988464631::get_offset_of_m_StoreName_4(),
	CloudCatalogManager_t3988464631_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	CloudCatalogManager_t3988464631_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (U3CFetchProductsU3Ec__AnonStorey2_t3565118814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1639[3] = 
{
	U3CFetchProductsU3Ec__AnonStorey2_t3565118814::get_offset_of_callback_0(),
	U3CFetchProductsU3Ec__AnonStorey2_t3565118814::get_offset_of_delayInSeconds_1(),
	U3CFetchProductsU3Ec__AnonStorey2_t3565118814::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (IDs_t3808979560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1640[1] = 
{
	IDs_t3808979560::get_offset_of_m_Dic_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { sizeof (ConfigurationBuilder_t1298400415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1641[3] = 
{
	ConfigurationBuilder_t1298400415::get_offset_of_m_Factory_0(),
	ConfigurationBuilder_t1298400415::get_offset_of_m_Products_1(),
	ConfigurationBuilder_t1298400415::get_offset_of_U3CuseCloudCatalogU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (InitializationFailureReason_t2954032642)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1645[4] = 
{
	InitializationFailureReason_t2954032642::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { sizeof (Product_t1203687971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1649[5] = 
{
	Product_t1203687971::get_offset_of_U3CdefinitionU3Ek__BackingField_0(),
	Product_t1203687971::get_offset_of_U3CmetadataU3Ek__BackingField_1(),
	Product_t1203687971::get_offset_of_U3CavailableToPurchaseU3Ek__BackingField_2(),
	Product_t1203687971::get_offset_of_U3CtransactionIDU3Ek__BackingField_3(),
	Product_t1203687971::get_offset_of_U3CreceiptU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (ProductCollection_t3600019299), -1, sizeof(ProductCollection_t3600019299_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1650[6] = 
{
	ProductCollection_t3600019299::get_offset_of_m_IdToProduct_0(),
	ProductCollection_t3600019299::get_offset_of_m_StoreSpecificIdToProduct_1(),
	ProductCollection_t3600019299::get_offset_of_m_Products_2(),
	ProductCollection_t3600019299::get_offset_of_m_ProductSet_3(),
	ProductCollection_t3600019299_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	ProductCollection_t3600019299_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { sizeof (ProductDefinition_t1942475268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1651[3] = 
{
	ProductDefinition_t1942475268::get_offset_of_U3CidU3Ek__BackingField_0(),
	ProductDefinition_t1942475268::get_offset_of_U3CstoreSpecificIdU3Ek__BackingField_1(),
	ProductDefinition_t1942475268::get_offset_of_U3CtypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { sizeof (ProductMetadata_t1573242544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1652[5] = 
{
	ProductMetadata_t1573242544::get_offset_of_U3ClocalizedPriceStringU3Ek__BackingField_0(),
	ProductMetadata_t1573242544::get_offset_of_U3ClocalizedTitleU3Ek__BackingField_1(),
	ProductMetadata_t1573242544::get_offset_of_U3ClocalizedDescriptionU3Ek__BackingField_2(),
	ProductMetadata_t1573242544::get_offset_of_U3CisoCurrencyCodeU3Ek__BackingField_3(),
	ProductMetadata_t1573242544::get_offset_of_U3ClocalizedPriceU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { sizeof (ProductType_t2754455291)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1653[4] = 
{
	ProductType_t2754455291::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { sizeof (PurchaseEventArgs_t547992434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1654[1] = 
{
	PurchaseEventArgs_t547992434::get_offset_of_U3CpurchasedProductU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (PurchaseFailureDescription_t1607114611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1655[3] = 
{
	PurchaseFailureDescription_t1607114611::get_offset_of_U3CproductIdU3Ek__BackingField_0(),
	PurchaseFailureDescription_t1607114611::get_offset_of_U3CreasonU3Ek__BackingField_1(),
	PurchaseFailureDescription_t1607114611::get_offset_of_U3CmessageU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (PurchaseFailureReason_t1322959839)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1656[8] = 
{
	PurchaseFailureReason_t1322959839::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (PurchaseProcessingResult_t2407199463)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1657[3] = 
{
	PurchaseProcessingResult_t2407199463::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (PurchasingFactory_t3525211160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1658[4] = 
{
	PurchasingFactory_t3525211160::get_offset_of_m_ConfigMap_0(),
	PurchasingFactory_t3525211160::get_offset_of_m_ExtensionMap_1(),
	PurchasingFactory_t3525211160::get_offset_of_m_Store_2(),
	PurchasingFactory_t3525211160::get_offset_of_U3CstoreNameU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { sizeof (PurchasingManager_t728606867), -1, sizeof(PurchasingManager_t728606867_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1659[11] = 
{
	PurchasingManager_t728606867::get_offset_of_m_Store_0(),
	PurchasingManager_t728606867::get_offset_of_m_Listener_1(),
	PurchasingManager_t728606867::get_offset_of_m_Logger_2(),
	PurchasingManager_t728606867::get_offset_of_m_TransactionLog_3(),
	PurchasingManager_t728606867::get_offset_of_m_StoreName_4(),
	PurchasingManager_t728606867::get_offset_of_m_AdditionalProductsCallback_5(),
	PurchasingManager_t728606867::get_offset_of_m_AdditionalProductsFailCallback_6(),
	PurchasingManager_t728606867::get_offset_of_initialized_7(),
	PurchasingManager_t728606867::get_offset_of_U3CuseTransactionLogU3Ek__BackingField_8(),
	PurchasingManager_t728606867::get_offset_of_U3CproductsU3Ek__BackingField_9(),
	PurchasingManager_t728606867_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { sizeof (StoreListenerProxy_t2597962341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1660[3] = 
{
	StoreListenerProxy_t2597962341::get_offset_of_m_Analytics_0(),
	StoreListenerProxy_t2597962341::get_offset_of_m_ForwardTo_1(),
	StoreListenerProxy_t2597962341::get_offset_of_m_Extensions_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (TransactionLog_t45391254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1661[2] = 
{
	TransactionLog_t45391254::get_offset_of_logger_0(),
	TransactionLog_t45391254::get_offset_of_persistentDataPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (UnityAnalytics_t4076614841), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (UnityPurchasing_t3301441281), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (U3CInitializeU3Ec__AnonStorey3_t768337693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1664[2] = 
{
	U3CInitializeU3Ec__AnonStorey3_t768337693::get_offset_of_manager_0(),
	U3CInitializeU3Ec__AnonStorey3_t768337693::get_offset_of_proxy_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (U3CFetchAndMergeProductsU3Ec__AnonStorey4_t1270402009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1665[2] = 
{
	U3CFetchAndMergeProductsU3Ec__AnonStorey4_t1270402009::get_offset_of_applicationProducts_0(),
	U3CFetchAndMergeProductsU3Ec__AnonStorey4_t1270402009::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (AbstractPurchasingModule_t4102635892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1666[1] = 
{
	AbstractPurchasingModule_t4102635892::get_offset_of_m_Binder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (AbstractStore_t2787096497), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (ProductDescription_t3318267523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1674[5] = 
{
	ProductDescription_t3318267523::get_offset_of_type_0(),
	ProductDescription_t3318267523::get_offset_of_U3CstoreSpecificIdU3Ek__BackingField_1(),
	ProductDescription_t3318267523::get_offset_of_U3CmetadataU3Ek__BackingField_2(),
	ProductDescription_t3318267523::get_offset_of_U3CreceiptU3Ek__BackingField_3(),
	ProductDescription_t3318267523::get_offset_of_U3CtransactionIdU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (EventHandle_t942672932)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1676[3] = 
{
	EventHandle_t942672932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { sizeof (EventSystem_t3466835263), -1, sizeof(EventSystem_t3466835263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1695[10] = 
{
	EventSystem_t3466835263::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t3466835263::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t3466835263::get_offset_of_m_FirstSelected_4(),
	EventSystem_t3466835263::get_offset_of_m_sendNavigationEvents_5(),
	EventSystem_t3466835263::get_offset_of_m_DragThreshold_6(),
	EventSystem_t3466835263::get_offset_of_m_CurrentSelected_7(),
	EventSystem_t3466835263::get_offset_of_m_SelectionGuard_8(),
	EventSystem_t3466835263::get_offset_of_m_DummyData_9(),
	EventSystem_t3466835263_StaticFields::get_offset_of_s_RaycastComparer_10(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CcurrentU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { sizeof (EventTrigger_t1967201810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1696[2] = 
{
	EventTrigger_t1967201810::get_offset_of_m_Delegates_2(),
	EventTrigger_t1967201810::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { sizeof (TriggerEvent_t3959312622), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (Entry_t3365010046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1698[2] = 
{
	Entry_t3365010046::get_offset_of_eventID_0(),
	Entry_t3365010046::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (EventTriggerType_t2524067914)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1699[18] = 
{
	EventTriggerType_t2524067914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
