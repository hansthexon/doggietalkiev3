﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Type
struct Type_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.String
struct String_t;
// LitJson.PropertyMetadata
struct PropertyMetadata_t3693826136;
struct PropertyMetadata_t3693826136_marshaled_pinvoke;
struct PropertyMetadata_t3693826136_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata3693826136.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "AssemblyU2DCSharp_LitJson_JsonIgnoreWhen1352154806.h"
#include "mscorlib_System_String2029220233.h"

// System.Type LitJson.PropertyMetadata::get_Type()
extern "C"  Type_t * PropertyMetadata_get_Type_m182026075 (PropertyMetadata_t3693826136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.PropertyMetadata::set_Type(System.Type)
extern "C"  void PropertyMetadata_set_Type_m27480386 (PropertyMetadata_t3693826136 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo LitJson.PropertyMetadata::get_Info()
extern "C"  MemberInfo_t * PropertyMetadata_get_Info_m504938762 (PropertyMetadata_t3693826136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.PropertyMetadata::set_Info(System.Reflection.MemberInfo)
extern "C"  void PropertyMetadata_set_Info_m2046380273 (PropertyMetadata_t3693826136 * __this, MemberInfo_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.JsonIgnoreWhen LitJson.PropertyMetadata::get_Ignore()
extern "C"  int32_t PropertyMetadata_get_Ignore_m758728387 (PropertyMetadata_t3693826136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.PropertyMetadata::set_Ignore(LitJson.JsonIgnoreWhen)
extern "C"  void PropertyMetadata_set_Ignore_m384359490 (PropertyMetadata_t3693826136 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LitJson.PropertyMetadata::get_Alias()
extern "C"  String_t* PropertyMetadata_get_Alias_m4074148678 (PropertyMetadata_t3693826136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.PropertyMetadata::set_Alias(System.String)
extern "C"  void PropertyMetadata_set_Alias_m746868831 (PropertyMetadata_t3693826136 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.PropertyMetadata::get_IsField()
extern "C"  bool PropertyMetadata_get_IsField_m4205458919 (PropertyMetadata_t3693826136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.PropertyMetadata::set_IsField(System.Boolean)
extern "C"  void PropertyMetadata_set_IsField_m2222861140 (PropertyMetadata_t3693826136 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.PropertyMetadata::get_Include()
extern "C"  bool PropertyMetadata_get_Include_m4174965043 (PropertyMetadata_t3693826136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.PropertyMetadata::set_Include(System.Boolean)
extern "C"  void PropertyMetadata_set_Include_m3817555192 (PropertyMetadata_t3693826136 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct PropertyMetadata_t3693826136;
struct PropertyMetadata_t3693826136_marshaled_pinvoke;

extern "C" void PropertyMetadata_t3693826136_marshal_pinvoke(const PropertyMetadata_t3693826136& unmarshaled, PropertyMetadata_t3693826136_marshaled_pinvoke& marshaled);
extern "C" void PropertyMetadata_t3693826136_marshal_pinvoke_back(const PropertyMetadata_t3693826136_marshaled_pinvoke& marshaled, PropertyMetadata_t3693826136& unmarshaled);
extern "C" void PropertyMetadata_t3693826136_marshal_pinvoke_cleanup(PropertyMetadata_t3693826136_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct PropertyMetadata_t3693826136;
struct PropertyMetadata_t3693826136_marshaled_com;

extern "C" void PropertyMetadata_t3693826136_marshal_com(const PropertyMetadata_t3693826136& unmarshaled, PropertyMetadata_t3693826136_marshaled_com& marshaled);
extern "C" void PropertyMetadata_t3693826136_marshal_com_back(const PropertyMetadata_t3693826136_marshaled_com& marshaled, PropertyMetadata_t3693826136& unmarshaled);
extern "C" void PropertyMetadata_t3693826136_marshal_com_cleanup(PropertyMetadata_t3693826136_marshaled_com& marshaled);
