﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// MediaPlayerCtrl
struct MediaPlayerCtrl_t1284484152;
// DataService
struct DataService_t2602786891;
// Code
struct Code_t686167511;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolitePanelScript
struct  PolitePanelScript_t678233578  : public MonoBehaviour_t1158329972
{
public:
	// System.String PolitePanelScript::PlanetScore
	String_t* ___PlanetScore_2;
	// UnityEngine.UI.Text PolitePanelScript::ScoreText
	Text_t356221433 * ___ScoreText_3;
	// UnityEngine.GameObject PolitePanelScript::mainScreen
	GameObject_t1756533147 * ___mainScreen_4;
	// System.Boolean PolitePanelScript::replay
	bool ___replay_5;
	// System.String PolitePanelScript::vivdeoname
	String_t* ___vivdeoname_6;
	// UnityEngine.GameObject PolitePanelScript::ReplayPanel
	GameObject_t1756533147 * ___ReplayPanel_7;
	// UnityEngine.GameObject PolitePanelScript::PopupsPanel
	GameObject_t1756533147 * ___PopupsPanel_8;
	// UnityEngine.GameObject PolitePanelScript::PolitePanel
	GameObject_t1756533147 * ___PolitePanel_9;
	// UnityEngine.GameObject PolitePanelScript::PoliteSky
	GameObject_t1756533147 * ___PoliteSky_10;
	// UnityEngine.GameObject PolitePanelScript::ScorePanel
	GameObject_t1756533147 * ___ScorePanel_11;
	// UnityEngine.GameObject[] PolitePanelScript::PanelstBeClosed
	GameObjectU5BU5D_t3057952154* ___PanelstBeClosed_12;
	// UnityEngine.GameObject PolitePanelScript::videoPrefab
	GameObject_t1756533147 * ___videoPrefab_13;
	// UnityEngine.GameObject PolitePanelScript::videoManager
	GameObject_t1756533147 * ___videoManager_14;
	// MediaPlayerCtrl PolitePanelScript::player
	MediaPlayerCtrl_t1284484152 * ___player_15;
	// DataService PolitePanelScript::ds
	DataService_t2602786891 * ___ds_16;
	// Code PolitePanelScript::codeobject
	Code_t686167511 * ___codeobject_17;
	// UnityEngine.GameObject[] PolitePanelScript::PoliteButtons
	GameObjectU5BU5D_t3057952154* ___PoliteButtons_18;
	// UnityEngine.GameObject[] PolitePanelScript::fuckthemall
	GameObjectU5BU5D_t3057952154* ___fuckthemall_19;
	// UnityEngine.GameObject PolitePanelScript::StarBar
	GameObject_t1756533147 * ___StarBar_20;
	// UnityEngine.GameObject PolitePanelScript::PlanetBar
	GameObject_t1756533147 * ___PlanetBar_21;

public:
	inline static int32_t get_offset_of_PlanetScore_2() { return static_cast<int32_t>(offsetof(PolitePanelScript_t678233578, ___PlanetScore_2)); }
	inline String_t* get_PlanetScore_2() const { return ___PlanetScore_2; }
	inline String_t** get_address_of_PlanetScore_2() { return &___PlanetScore_2; }
	inline void set_PlanetScore_2(String_t* value)
	{
		___PlanetScore_2 = value;
		Il2CppCodeGenWriteBarrier(&___PlanetScore_2, value);
	}

	inline static int32_t get_offset_of_ScoreText_3() { return static_cast<int32_t>(offsetof(PolitePanelScript_t678233578, ___ScoreText_3)); }
	inline Text_t356221433 * get_ScoreText_3() const { return ___ScoreText_3; }
	inline Text_t356221433 ** get_address_of_ScoreText_3() { return &___ScoreText_3; }
	inline void set_ScoreText_3(Text_t356221433 * value)
	{
		___ScoreText_3 = value;
		Il2CppCodeGenWriteBarrier(&___ScoreText_3, value);
	}

	inline static int32_t get_offset_of_mainScreen_4() { return static_cast<int32_t>(offsetof(PolitePanelScript_t678233578, ___mainScreen_4)); }
	inline GameObject_t1756533147 * get_mainScreen_4() const { return ___mainScreen_4; }
	inline GameObject_t1756533147 ** get_address_of_mainScreen_4() { return &___mainScreen_4; }
	inline void set_mainScreen_4(GameObject_t1756533147 * value)
	{
		___mainScreen_4 = value;
		Il2CppCodeGenWriteBarrier(&___mainScreen_4, value);
	}

	inline static int32_t get_offset_of_replay_5() { return static_cast<int32_t>(offsetof(PolitePanelScript_t678233578, ___replay_5)); }
	inline bool get_replay_5() const { return ___replay_5; }
	inline bool* get_address_of_replay_5() { return &___replay_5; }
	inline void set_replay_5(bool value)
	{
		___replay_5 = value;
	}

	inline static int32_t get_offset_of_vivdeoname_6() { return static_cast<int32_t>(offsetof(PolitePanelScript_t678233578, ___vivdeoname_6)); }
	inline String_t* get_vivdeoname_6() const { return ___vivdeoname_6; }
	inline String_t** get_address_of_vivdeoname_6() { return &___vivdeoname_6; }
	inline void set_vivdeoname_6(String_t* value)
	{
		___vivdeoname_6 = value;
		Il2CppCodeGenWriteBarrier(&___vivdeoname_6, value);
	}

	inline static int32_t get_offset_of_ReplayPanel_7() { return static_cast<int32_t>(offsetof(PolitePanelScript_t678233578, ___ReplayPanel_7)); }
	inline GameObject_t1756533147 * get_ReplayPanel_7() const { return ___ReplayPanel_7; }
	inline GameObject_t1756533147 ** get_address_of_ReplayPanel_7() { return &___ReplayPanel_7; }
	inline void set_ReplayPanel_7(GameObject_t1756533147 * value)
	{
		___ReplayPanel_7 = value;
		Il2CppCodeGenWriteBarrier(&___ReplayPanel_7, value);
	}

	inline static int32_t get_offset_of_PopupsPanel_8() { return static_cast<int32_t>(offsetof(PolitePanelScript_t678233578, ___PopupsPanel_8)); }
	inline GameObject_t1756533147 * get_PopupsPanel_8() const { return ___PopupsPanel_8; }
	inline GameObject_t1756533147 ** get_address_of_PopupsPanel_8() { return &___PopupsPanel_8; }
	inline void set_PopupsPanel_8(GameObject_t1756533147 * value)
	{
		___PopupsPanel_8 = value;
		Il2CppCodeGenWriteBarrier(&___PopupsPanel_8, value);
	}

	inline static int32_t get_offset_of_PolitePanel_9() { return static_cast<int32_t>(offsetof(PolitePanelScript_t678233578, ___PolitePanel_9)); }
	inline GameObject_t1756533147 * get_PolitePanel_9() const { return ___PolitePanel_9; }
	inline GameObject_t1756533147 ** get_address_of_PolitePanel_9() { return &___PolitePanel_9; }
	inline void set_PolitePanel_9(GameObject_t1756533147 * value)
	{
		___PolitePanel_9 = value;
		Il2CppCodeGenWriteBarrier(&___PolitePanel_9, value);
	}

	inline static int32_t get_offset_of_PoliteSky_10() { return static_cast<int32_t>(offsetof(PolitePanelScript_t678233578, ___PoliteSky_10)); }
	inline GameObject_t1756533147 * get_PoliteSky_10() const { return ___PoliteSky_10; }
	inline GameObject_t1756533147 ** get_address_of_PoliteSky_10() { return &___PoliteSky_10; }
	inline void set_PoliteSky_10(GameObject_t1756533147 * value)
	{
		___PoliteSky_10 = value;
		Il2CppCodeGenWriteBarrier(&___PoliteSky_10, value);
	}

	inline static int32_t get_offset_of_ScorePanel_11() { return static_cast<int32_t>(offsetof(PolitePanelScript_t678233578, ___ScorePanel_11)); }
	inline GameObject_t1756533147 * get_ScorePanel_11() const { return ___ScorePanel_11; }
	inline GameObject_t1756533147 ** get_address_of_ScorePanel_11() { return &___ScorePanel_11; }
	inline void set_ScorePanel_11(GameObject_t1756533147 * value)
	{
		___ScorePanel_11 = value;
		Il2CppCodeGenWriteBarrier(&___ScorePanel_11, value);
	}

	inline static int32_t get_offset_of_PanelstBeClosed_12() { return static_cast<int32_t>(offsetof(PolitePanelScript_t678233578, ___PanelstBeClosed_12)); }
	inline GameObjectU5BU5D_t3057952154* get_PanelstBeClosed_12() const { return ___PanelstBeClosed_12; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_PanelstBeClosed_12() { return &___PanelstBeClosed_12; }
	inline void set_PanelstBeClosed_12(GameObjectU5BU5D_t3057952154* value)
	{
		___PanelstBeClosed_12 = value;
		Il2CppCodeGenWriteBarrier(&___PanelstBeClosed_12, value);
	}

	inline static int32_t get_offset_of_videoPrefab_13() { return static_cast<int32_t>(offsetof(PolitePanelScript_t678233578, ___videoPrefab_13)); }
	inline GameObject_t1756533147 * get_videoPrefab_13() const { return ___videoPrefab_13; }
	inline GameObject_t1756533147 ** get_address_of_videoPrefab_13() { return &___videoPrefab_13; }
	inline void set_videoPrefab_13(GameObject_t1756533147 * value)
	{
		___videoPrefab_13 = value;
		Il2CppCodeGenWriteBarrier(&___videoPrefab_13, value);
	}

	inline static int32_t get_offset_of_videoManager_14() { return static_cast<int32_t>(offsetof(PolitePanelScript_t678233578, ___videoManager_14)); }
	inline GameObject_t1756533147 * get_videoManager_14() const { return ___videoManager_14; }
	inline GameObject_t1756533147 ** get_address_of_videoManager_14() { return &___videoManager_14; }
	inline void set_videoManager_14(GameObject_t1756533147 * value)
	{
		___videoManager_14 = value;
		Il2CppCodeGenWriteBarrier(&___videoManager_14, value);
	}

	inline static int32_t get_offset_of_player_15() { return static_cast<int32_t>(offsetof(PolitePanelScript_t678233578, ___player_15)); }
	inline MediaPlayerCtrl_t1284484152 * get_player_15() const { return ___player_15; }
	inline MediaPlayerCtrl_t1284484152 ** get_address_of_player_15() { return &___player_15; }
	inline void set_player_15(MediaPlayerCtrl_t1284484152 * value)
	{
		___player_15 = value;
		Il2CppCodeGenWriteBarrier(&___player_15, value);
	}

	inline static int32_t get_offset_of_ds_16() { return static_cast<int32_t>(offsetof(PolitePanelScript_t678233578, ___ds_16)); }
	inline DataService_t2602786891 * get_ds_16() const { return ___ds_16; }
	inline DataService_t2602786891 ** get_address_of_ds_16() { return &___ds_16; }
	inline void set_ds_16(DataService_t2602786891 * value)
	{
		___ds_16 = value;
		Il2CppCodeGenWriteBarrier(&___ds_16, value);
	}

	inline static int32_t get_offset_of_codeobject_17() { return static_cast<int32_t>(offsetof(PolitePanelScript_t678233578, ___codeobject_17)); }
	inline Code_t686167511 * get_codeobject_17() const { return ___codeobject_17; }
	inline Code_t686167511 ** get_address_of_codeobject_17() { return &___codeobject_17; }
	inline void set_codeobject_17(Code_t686167511 * value)
	{
		___codeobject_17 = value;
		Il2CppCodeGenWriteBarrier(&___codeobject_17, value);
	}

	inline static int32_t get_offset_of_PoliteButtons_18() { return static_cast<int32_t>(offsetof(PolitePanelScript_t678233578, ___PoliteButtons_18)); }
	inline GameObjectU5BU5D_t3057952154* get_PoliteButtons_18() const { return ___PoliteButtons_18; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_PoliteButtons_18() { return &___PoliteButtons_18; }
	inline void set_PoliteButtons_18(GameObjectU5BU5D_t3057952154* value)
	{
		___PoliteButtons_18 = value;
		Il2CppCodeGenWriteBarrier(&___PoliteButtons_18, value);
	}

	inline static int32_t get_offset_of_fuckthemall_19() { return static_cast<int32_t>(offsetof(PolitePanelScript_t678233578, ___fuckthemall_19)); }
	inline GameObjectU5BU5D_t3057952154* get_fuckthemall_19() const { return ___fuckthemall_19; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_fuckthemall_19() { return &___fuckthemall_19; }
	inline void set_fuckthemall_19(GameObjectU5BU5D_t3057952154* value)
	{
		___fuckthemall_19 = value;
		Il2CppCodeGenWriteBarrier(&___fuckthemall_19, value);
	}

	inline static int32_t get_offset_of_StarBar_20() { return static_cast<int32_t>(offsetof(PolitePanelScript_t678233578, ___StarBar_20)); }
	inline GameObject_t1756533147 * get_StarBar_20() const { return ___StarBar_20; }
	inline GameObject_t1756533147 ** get_address_of_StarBar_20() { return &___StarBar_20; }
	inline void set_StarBar_20(GameObject_t1756533147 * value)
	{
		___StarBar_20 = value;
		Il2CppCodeGenWriteBarrier(&___StarBar_20, value);
	}

	inline static int32_t get_offset_of_PlanetBar_21() { return static_cast<int32_t>(offsetof(PolitePanelScript_t678233578, ___PlanetBar_21)); }
	inline GameObject_t1756533147 * get_PlanetBar_21() const { return ___PlanetBar_21; }
	inline GameObject_t1756533147 ** get_address_of_PlanetBar_21() { return &___PlanetBar_21; }
	inline void set_PlanetBar_21(GameObject_t1756533147 * value)
	{
		___PlanetBar_21 = value;
		Il2CppCodeGenWriteBarrier(&___PlanetBar_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
