﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Linq.Expressions.Expression
struct Expression_t114864668;

#include "System_Core_System_Linq_Expressions_Expression114864668.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.UnaryExpression
struct  UnaryExpression_t4253534555  : public Expression_t114864668
{
public:
	// System.Linq.Expressions.Expression System.Linq.Expressions.UnaryExpression::operand
	Expression_t114864668 * ___operand_2;

public:
	inline static int32_t get_offset_of_operand_2() { return static_cast<int32_t>(offsetof(UnaryExpression_t4253534555, ___operand_2)); }
	inline Expression_t114864668 * get_operand_2() const { return ___operand_2; }
	inline Expression_t114864668 ** get_address_of_operand_2() { return &___operand_2; }
	inline void set_operand_2(Expression_t114864668 * value)
	{
		___operand_2 = value;
		Il2CppCodeGenWriteBarrier(&___operand_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
