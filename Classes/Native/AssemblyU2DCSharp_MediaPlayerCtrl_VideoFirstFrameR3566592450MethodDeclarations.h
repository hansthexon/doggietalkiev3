﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MediaPlayerCtrl/VideoFirstFrameReady
struct VideoFirstFrameReady_t3566592450;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void MediaPlayerCtrl/VideoFirstFrameReady::.ctor(System.Object,System.IntPtr)
extern "C"  void VideoFirstFrameReady__ctor_m1195946215 (VideoFirstFrameReady_t3566592450 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl/VideoFirstFrameReady::Invoke()
extern "C"  void VideoFirstFrameReady_Invoke_m552321867 (VideoFirstFrameReady_t3566592450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MediaPlayerCtrl/VideoFirstFrameReady::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * VideoFirstFrameReady_BeginInvoke_m955106092 (VideoFirstFrameReady_t3566592450 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl/VideoFirstFrameReady::EndInvoke(System.IAsyncResult)
extern "C"  void VideoFirstFrameReady_EndInvoke_m1574910253 (VideoFirstFrameReady_t3566592450 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
