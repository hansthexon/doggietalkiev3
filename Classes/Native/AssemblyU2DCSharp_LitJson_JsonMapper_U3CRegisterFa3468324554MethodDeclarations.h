﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.JsonMapper/<RegisterFactory>c__AnonStorey1A`1<System.Object>
struct U3CRegisterFactoryU3Ec__AnonStorey1A_1_t3468324554;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LitJson.JsonMapper/<RegisterFactory>c__AnonStorey1A`1<System.Object>::.ctor()
extern "C"  void U3CRegisterFactoryU3Ec__AnonStorey1A_1__ctor_m266441525_gshared (U3CRegisterFactoryU3Ec__AnonStorey1A_1_t3468324554 * __this, const MethodInfo* method);
#define U3CRegisterFactoryU3Ec__AnonStorey1A_1__ctor_m266441525(__this, method) ((  void (*) (U3CRegisterFactoryU3Ec__AnonStorey1A_1_t3468324554 *, const MethodInfo*))U3CRegisterFactoryU3Ec__AnonStorey1A_1__ctor_m266441525_gshared)(__this, method)
// System.Object LitJson.JsonMapper/<RegisterFactory>c__AnonStorey1A`1<System.Object>::<>m__34()
extern "C"  Il2CppObject * U3CRegisterFactoryU3Ec__AnonStorey1A_1_U3CU3Em__34_m3302115200_gshared (U3CRegisterFactoryU3Ec__AnonStorey1A_1_t3468324554 * __this, const MethodInfo* method);
#define U3CRegisterFactoryU3Ec__AnonStorey1A_1_U3CU3Em__34_m3302115200(__this, method) ((  Il2CppObject * (*) (U3CRegisterFactoryU3Ec__AnonStorey1A_1_t3468324554 *, const MethodInfo*))U3CRegisterFactoryU3Ec__AnonStorey1A_1_U3CU3Em__34_m3302115200_gshared)(__this, method)
