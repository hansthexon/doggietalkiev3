﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "Vuforia_UnityExtensions_U3CPrivateImplementationDe2759170530.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{C30DAF88-821F-4C93-A2B7-DEA1CE3CF3BB}
struct  U3CPrivateImplementationDetailsU3EU7BC30DAF88U2D821FU2D4C93U2DA2B7U2DDEA1CE3CF3BBU7D_t1649997976  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3EU7BC30DAF88U2D821FU2D4C93U2DA2B7U2DDEA1CE3CF3BBU7D_t1649997976_StaticFields
{
public:
	// <PrivateImplementationDetails>{C30DAF88-821F-4C93-A2B7-DEA1CE3CF3BB}/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>{C30DAF88-821F-4C93-A2B7-DEA1CE3CF3BB}::$$method0x6000b9c-1
	__StaticArrayInitTypeSizeU3D24_t2759170530  ___U24U24method0x6000b9cU2D1_0;

public:
	inline static int32_t get_offset_of_U24U24method0x6000b9cU2D1_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7BC30DAF88U2D821FU2D4C93U2DA2B7U2DDEA1CE3CF3BBU7D_t1649997976_StaticFields, ___U24U24method0x6000b9cU2D1_0)); }
	inline __StaticArrayInitTypeSizeU3D24_t2759170530  get_U24U24method0x6000b9cU2D1_0() const { return ___U24U24method0x6000b9cU2D1_0; }
	inline __StaticArrayInitTypeSizeU3D24_t2759170530 * get_address_of_U24U24method0x6000b9cU2D1_0() { return &___U24U24method0x6000b9cU2D1_0; }
	inline void set_U24U24method0x6000b9cU2D1_0(__StaticArrayInitTypeSizeU3D24_t2759170530  value)
	{
		___U24U24method0x6000b9cU2D1_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
