﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.SQLiteConnectionString
struct SQLiteConnectionString_t3251561909;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SQLite4Unity3d.SQLiteConnectionString::.ctor(System.String,System.Boolean)
extern "C"  void SQLiteConnectionString__ctor_m3883578551 (SQLiteConnectionString_t3251561909 * __this, String_t* ___databasePath0, bool ___storeDateTimeAsTicks1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.SQLiteConnectionString::get_ConnectionString()
extern "C"  String_t* SQLiteConnectionString_get_ConnectionString_m738761717 (SQLiteConnectionString_t3251561909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnectionString::set_ConnectionString(System.String)
extern "C"  void SQLiteConnectionString_set_ConnectionString_m570396280 (SQLiteConnectionString_t3251561909 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.SQLiteConnectionString::get_DatabasePath()
extern "C"  String_t* SQLiteConnectionString_get_DatabasePath_m1051739642 (SQLiteConnectionString_t3251561909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnectionString::set_DatabasePath(System.String)
extern "C"  void SQLiteConnectionString_set_DatabasePath_m2065728477 (SQLiteConnectionString_t3251561909 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.SQLiteConnectionString::get_StoreDateTimeAsTicks()
extern "C"  bool SQLiteConnectionString_get_StoreDateTimeAsTicks_m2872969873 (SQLiteConnectionString_t3251561909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnectionString::set_StoreDateTimeAsTicks(System.Boolean)
extern "C"  void SQLiteConnectionString_set_StoreDateTimeAsTicks_m2761205144 (SQLiteConnectionString_t3251561909 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
