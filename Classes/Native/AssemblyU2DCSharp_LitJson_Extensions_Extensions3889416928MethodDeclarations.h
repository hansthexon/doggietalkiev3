﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.JsonWriter
struct JsonWriter_t1927598499;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LitJson_JsonWriter1927598499.h"
#include "mscorlib_System_String2029220233.h"

// System.Void LitJson.Extensions.Extensions::WriteProperty(LitJson.JsonWriter,System.String,System.Int64)
extern "C"  void Extensions_WriteProperty_m925579472 (Il2CppObject * __this /* static, unused */, JsonWriter_t1927598499 * ___w0, String_t* ___name1, int64_t ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.Extensions.Extensions::WriteProperty(LitJson.JsonWriter,System.String,System.String)
extern "C"  void Extensions_WriteProperty_m901643238 (Il2CppObject * __this /* static, unused */, JsonWriter_t1927598499 * ___w0, String_t* ___name1, String_t* ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.Extensions.Extensions::WriteProperty(LitJson.JsonWriter,System.String,System.Boolean)
extern "C"  void Extensions_WriteProperty_m204116181 (Il2CppObject * __this /* static, unused */, JsonWriter_t1927598499 * ___w0, String_t* ___name1, bool ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.Extensions.Extensions::WriteProperty(LitJson.JsonWriter,System.String,System.Double)
extern "C"  void Extensions_WriteProperty_m1072965522 (Il2CppObject * __this /* static, unused */, JsonWriter_t1927598499 * ___w0, String_t* ___name1, double ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
