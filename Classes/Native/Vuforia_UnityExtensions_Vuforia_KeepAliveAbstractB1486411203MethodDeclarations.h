﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.KeepAliveAbstractBehaviour
struct KeepAliveAbstractBehaviour_t1486411203;
// Vuforia.ILoadLevelEventHandler
struct ILoadLevelEventHandler_t75496635;

#include "codegen/il2cpp-codegen.h"

// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepARCameraAlive()
extern "C"  bool KeepAliveAbstractBehaviour_get_KeepARCameraAlive_m3170864036 (KeepAliveAbstractBehaviour_t1486411203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepARCameraAlive(System.Boolean)
extern "C"  void KeepAliveAbstractBehaviour_set_KeepARCameraAlive_m1548448823 (KeepAliveAbstractBehaviour_t1486411203 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepTrackableBehavioursAlive()
extern "C"  bool KeepAliveAbstractBehaviour_get_KeepTrackableBehavioursAlive_m1872926173 (KeepAliveAbstractBehaviour_t1486411203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepTrackableBehavioursAlive(System.Boolean)
extern "C"  void KeepAliveAbstractBehaviour_set_KeepTrackableBehavioursAlive_m611798104 (KeepAliveAbstractBehaviour_t1486411203 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepTextRecoBehaviourAlive()
extern "C"  bool KeepAliveAbstractBehaviour_get_KeepTextRecoBehaviourAlive_m287271185 (KeepAliveAbstractBehaviour_t1486411203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepTextRecoBehaviourAlive(System.Boolean)
extern "C"  void KeepAliveAbstractBehaviour_set_KeepTextRecoBehaviourAlive_m574388120 (KeepAliveAbstractBehaviour_t1486411203 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepUDTBuildingBehaviourAlive()
extern "C"  bool KeepAliveAbstractBehaviour_get_KeepUDTBuildingBehaviourAlive_m3869308214 (KeepAliveAbstractBehaviour_t1486411203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepUDTBuildingBehaviourAlive(System.Boolean)
extern "C"  void KeepAliveAbstractBehaviour_set_KeepUDTBuildingBehaviourAlive_m2801882483 (KeepAliveAbstractBehaviour_t1486411203 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepSmartTerrainAlive()
extern "C"  bool KeepAliveAbstractBehaviour_get_KeepSmartTerrainAlive_m144254070 (KeepAliveAbstractBehaviour_t1486411203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepSmartTerrainAlive(System.Boolean)
extern "C"  void KeepAliveAbstractBehaviour_set_KeepSmartTerrainAlive_m1231549727 (KeepAliveAbstractBehaviour_t1486411203 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepCloudRecoBehaviourAlive()
extern "C"  bool KeepAliveAbstractBehaviour_get_KeepCloudRecoBehaviourAlive_m1949585149 (KeepAliveAbstractBehaviour_t1486411203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepCloudRecoBehaviourAlive(System.Boolean)
extern "C"  void KeepAliveAbstractBehaviour_set_KeepCloudRecoBehaviourAlive_m4092928582 (KeepAliveAbstractBehaviour_t1486411203 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.KeepAliveAbstractBehaviour Vuforia.KeepAliveAbstractBehaviour::get_Instance()
extern "C"  KeepAliveAbstractBehaviour_t1486411203 * KeepAliveAbstractBehaviour_get_Instance_m1024669491 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::RegisterEventHandler(Vuforia.ILoadLevelEventHandler)
extern "C"  void KeepAliveAbstractBehaviour_RegisterEventHandler_m3708155157 (KeepAliveAbstractBehaviour_t1486411203 * __this, Il2CppObject * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::UnregisterEventHandler(Vuforia.ILoadLevelEventHandler)
extern "C"  bool KeepAliveAbstractBehaviour_UnregisterEventHandler_m4061263518 (KeepAliveAbstractBehaviour_t1486411203 * __this, Il2CppObject * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::OnLevelWasLoaded()
extern "C"  void KeepAliveAbstractBehaviour_OnLevelWasLoaded_m1333989302 (KeepAliveAbstractBehaviour_t1486411203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::.ctor()
extern "C"  void KeepAliveAbstractBehaviour__ctor_m3694701863 (KeepAliveAbstractBehaviour_t1486411203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
