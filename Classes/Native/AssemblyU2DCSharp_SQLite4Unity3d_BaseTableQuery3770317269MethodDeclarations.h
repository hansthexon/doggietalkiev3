﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.BaseTableQuery
struct BaseTableQuery_t3770317269;

#include "codegen/il2cpp-codegen.h"

// System.Void SQLite4Unity3d.BaseTableQuery::.ctor()
extern "C"  void BaseTableQuery__ctor_m2606818376 (BaseTableQuery_t3770317269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
